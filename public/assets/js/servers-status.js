class serversStatus {
    constructor(route) {
        this.route = route;
    }
    getServerDetails(server_id, serverSelector) {
        var result;
        $.getJSON(this.route + '/' + server_id, function (data, status) {
        }, this).done(function(data,status){
            if (status == 'success') {
                if (data.code == 200) {
                    $('#'+serverSelector+' td#server_clients').html(data.body.serverClients);
                    $('#'+serverSelector+' td#server_status').html(data.body.serverStatus);
                }
            }
        });
    }
}