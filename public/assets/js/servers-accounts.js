class serversAccounts {
    constructor(route) {
        this.route = route;
    }

    getServerAccounts(server_id, serverSelector, selectedAccounts) {
        var result;
        $.getJSON(this.route + '/' + server_id, function (data, status) {
        }, this).done(function (data, status) {
            if (status == 'success') {
                if (data.code == 200) {
                    $('#' + serverSelector + ' #loading_badge').hide();
                    Object.keys(data.body.clients.obj).forEach(function (key) {
                        var selected = '';
                        if (selectedAccounts[data.body.serverId] == data.body.clients.obj[key].id) {
                            selected = 'selected';
                        }
                        $('#' + serverSelector + ' #accounts_list').append(`
                                <option value="` + data.body.serverId + `,` + data.body.clients.obj[key].id + `" ` + selected + `>` + data.body.clients.obj[key].remark + `</option>
                            `);
                    });

                }
            }
        });
    }
}
