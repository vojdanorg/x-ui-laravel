class getAccountDetails {
    constructor(route) {
        this.route = route;
    }

    accountDetails(account_id, accountSelector) {
        var errorOnFetch = '<span class="badge badge-danger">خطا در دریافت</span>';
        $.getJSON(this.route + '/' + account_id, function (data, status) {
        }, this).done(function (data, status) {

            function qrCodeGenerator(accountSelector) {
                if (data.body.is_multi_user == false) {
                    var iranBTN = '<button class="btn btn-warning btn-sm" style="color:black !important;" data-toggle="modal" data-target="#qrCodeModal" data-account-selector="' + accountSelector + '" data-is-iran="1">همراه اول و مودم</button>';
                } else {
                    var iranBTN = '<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#qrCodeModal" data-account-selector="' + accountSelector + '" data-is-iran="1">ایران</button>';
                }
                if (data.body.is_multi_user == false) {
                    var outsiderBTN = '<button class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#qrCodeModal" data-account-selector="' + accountSelector + '" data-is-iran="0" >ایرانسل و رایتل</button>';
                    return "<br>" + iranBTN + "<br><br>" + outsiderBTN;
                } else {
                    return "<br>" + iranBTN;
                }
            }

            if (status == 'success') {
                if (data.code == 200) {
                    $('#' + accountSelector + ' td#account_traffic').html(data.body.traffic);
                    $('#' + accountSelector + ' td#account_port').html(data.body.port);
                    $('#' + accountSelector + ' td#account_expires_at').html(data.body.expires_at);
                    $('#' + accountSelector + ' td#account_status').html(data.body.status);
                    $('#' + accountSelector + ' td#account_address').html(data.body.address);
                    $('#' + accountSelector + ' td#qr_code').html(qrCodeGenerator(data.body.id));
                    if (!data.body.is_multi_user) {
                        if (data.body.account_data.total_used_traffic < data.body.account_data.total_traffic || data.body.account_data.total_traffic === 0) {
                            if (data.body.account_data.expiry_time > data.body.account_data.current_timestamp || data.body.account_data.expiry_time === 0) {
                                if (!data.body.account_data.status) {
                                    $('#' + accountSelector + ' td#more_functions #account_power_switch').html('<button\n' +
                                        '                                                                        id="account_power_button"\n' +
                                        '                                                                        class="btn btn-sm btn-success darken-3"\n' +
                                        '                                                                        title="فعالسازی">\n' +
                                        '                                                                        <i class="icon-power-off text-white s-18"></i>\n' +
                                        '                                                                    </button>');
                                } else {
                                    $('#' + accountSelector + ' td#more_functions #account_power_switch').html('<button\n' +
                                        '                                                                        id="account_power_button"\n' +
                                        '                                                                        class="btn btn-sm btn-danger darken-3"\n' +
                                        '                                                                        title="غیرفعالسازی">\n' +
                                        '                                                                        <i class="icon-power-off text-white s-18"></i>\n' +
                                        '                                                                    </button>');
                                }
                                $('#' + accountSelector + ' td#more_functions #account_power_switch').show();
                            }
                        }
                    }
                } else {
                    $('#' + accountSelector + ' td#account_traffic').html(errorOnFetch);
                    $('#' + accountSelector + ' td#account_port').html(errorOnFetch);
                    $('#' + accountSelector + ' td#account_expires_at').html(errorOnFetch);
                    $('#' + accountSelector + ' td#account_status').html(errorOnFetch);
                    $('#' + accountSelector + ' td#account_address').html(errorOnFetch);
                    $('#' + accountSelector + ' td#qr_code').html(errorOnFetch);
                }
            } else {
                $('#' + accountSelector + ' td#account_traffic').html(errorOnFetch);
                $('#' + accountSelector + ' td#account_port').html(errorOnFetch);
                $('#' + accountSelector + ' td#account_expires_at').html(errorOnFetch);
                $('#' + accountSelector + ' td#account_status').html(errorOnFetch);
                $('#' + accountSelector + ' td#account_address').html(errorOnFetch);
                $('#' + accountSelector + ' td#qr_code').html(errorOnFetch);
            }
        });
    }
}
