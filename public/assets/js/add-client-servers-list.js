class addClientServersStatus {
    constructor(route) {
        this.route = route;
    }

    getServerDetails(server_id, serverSelector) {
        var result;
        $.getJSON(this.route + '/' + server_id, function (data, status) {
        }, this).done(function (data, status) {
            if (status == 'success') {
                if (data.code == 200) {
                    if (data.body.serverClients != 'تکمیل') {
                        var optionName = ' ظرفیت: ' + data.body.serverClients;
                        $('#loadingBadge').remove();
                        $('#serverList').append('<option data-has_cdn="' + data.body.has_cdn + '" data-allowed_for_multi_user="' + data.body.allowed_for_multi_user + '" data-is_pritunl="' + data.body.is_pritunl + '" value="' + data.body.serverId + '">' + data.body.serverName + ' - ' + optionName + '</option>');
                    }
                }
            }
        });
    }
}
