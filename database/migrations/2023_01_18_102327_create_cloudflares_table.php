<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cloudflare', function (Blueprint $table) {
            $table->id();
            $table->string('name', 1024);
            $table->string('api_token', 2048);
            $table->string('zone_id', 2048);
            $table->string('global_key', 2048);
            $table->json('json_data')->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cloudflares');
    }
};
