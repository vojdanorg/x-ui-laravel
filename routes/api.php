<?php

use App\Http\Controllers\Account\AccountController;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\LimiterApiController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\ServersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->middleware(['api', 'checkApiToken'])->group(function () {

    Route::prefix('reseller')->group(function () {
        Route::post('info', [ApiController::class, 'getUserInfo']);
    });
    Route::prefix('servers')->group(function () {
        Route::post('available', [ApiController::class, 'getAvailableServers']);
    });

    Route::prefix('packages')->group(function () {
        Route::post('available', [ApiController::class, 'getAvailablePackages']);
    });

    Route::prefix('accounts')->group(function () {
        Route::post('buy', [ApiController::class, 'buyAccount']);
        Route::post('stats', [ApiController::class, 'accountStats']);
    });

    Route::prefix('payment')->group(function () {
        Route::post('create-invoice', [ApiController::class, 'createInvoice']);
        Route::post('verify-invoice-payment', [ApiController::class, 'verifyInvoicePayment']);
    });

    Route::prefix('customer')->group(function () {
        Route::post('register', [ApiController::class, 'registerCustomer']);
    });

    Route::post('temp-get-account-info', [ApiController::class, 'tempGetAccountInfo']);

    Route::post('get-account-info', [ApiController::class, 'getAccountInfo']);

    Route::post('rebuild-not-found-accounts', [ApiController::class, 'rebuildAccounts']);
    Route::post('rebuild-custom-not-found-accounts', [ApiController::class, 'rebuildAccountsByTerm']);
    // Route::prefix('')

});

Route::prefix('limiter')->group(function () {
    Route::post('get-list-of-accounts-server', [LimiterApiController::class, 'getListOfAccountOnServer']);
    Route::post('add-list-of-connected-ips-to-account', [LimiterApiController::class, 'addListOfConnectedIPsToAccount']);
});

Route::prefix('panel')->middleware(['web', 'auth'])->group(function () {
    Route::get('get-server-status/{id?}', [ServersController::class, 'getServerStatus'])->name('api.panel.server-stauts');
    Route::get('add-client/get-server-status/{id?}', [ServersController::class, 'addClientGetServerStatus'])->name('api.panel.add-client.server-stauts');
    Route::get('client/details/{id?}', [AccountController::class, 'getAccountDetails'])->name('api.panel.get-account-details');

    Route::prefix('admin')->middleware(['web', 'auth', 'role:admin'])->group(function () {
        Route::get('add-client/get-server-accounts/{id?}', [ServersController::class, 'getServerClients'])->name('api.panel.admin.packages.get-accounts');
    });
});


//Customer Panel Routes
Route::prefix('customer')->middleware(['api', 'CPCheckApiToken'])->group(function () {
    //1st version of api
    Route::prefix('v1')->group(function () {
        Route::post('register', [\App\Http\Controllers\CustomerPanel\AuthController::class, 'register']);
        Route::post('login', [\App\Http\Controllers\CustomerPanel\AuthController::class, 'login']);
    });
});

Route::prefix('app')->group(function (){
    Route::post('login', [\App\Http\Controllers\AppController\AuthController::class, 'login']);
    Route::post('loginApp', [\App\Http\Controllers\AppController\AuthController::class, 'loginApp']);
    Route::post('increaseWallet', [\App\Http\Controllers\AppController\AuthController::class, 'increaseWallet']);
});



//Email required
//Phone optional
//password required
