<?php

use App\Http\Controllers\Account\AccountController;
use App\Http\Controllers\Admin\SettingsController;
use App\Http\Controllers\Agent\AgentController;
use App\Http\Controllers\Agent\AgentPackageController;
use App\Http\Controllers\Package\PackageController;
use App\Http\Controllers\Panel\DashboardController;
use App\Http\Controllers\ServersController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\TransactionsController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\WebServiceController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SiteController::class, 'index']);
// login as agent member
Route::get('/account-details', [AgentPackageController::class, 'accountDetail']);
Route::post('/account-details', [AgentPackageController::class, 'doAccountDetail'])->name('doAccountDetail.info');

Route::get('/change-protocol', [AgentPackageController::class, 'changeProtocol']);
Route::post('/change-protocol', [AgentPackageController::class, 'doChangeProtocol'])->name('doChangeProtocol');
Route::get('/forward-page', [AgentPackageController::class, 'forward_page'])->name('forward_page');
Route::get('/account-information/{hash}', [AgentPackageController::class, 'account_information'])->name('account.information');

Route::middleware(['auth'])->group(function () {
    //Logout Route
    Route::get('/user/logout', function () {
        Session::flush();
        Auth::logout();
        return redirect('login');
    })->name('user.logout');

    Route::get('/dashboard/', [DashboardController::class, 'index'])->name('dashboard');


    //User Profile Routes
    Route::get('/user/profile', [UserController::class, 'profile'])->name('user.profile');
    Route::post('/user/profile', [UserController::class, 'updateProfile']);
    Route::get('/user/change-password', [UserController::class, 'changePassword'])->name('user.change-password');
    Route::post('/user/change-password', [UserController::class, 'doChangePassword']);

    Route::get('/agent/register', [AgentController::class, 'register'])->name('agent.register');
    Route::post('/agent/register', [AgentController::class, 'do_register'])->name('agent.doRegister');
    Route::get('/agent/package', [AgentPackageController::class, 'index'])->name('agent.package');

    Route::get('/servers/status', [ServersController::class, 'status'])->name('servers.status');
    Route::get('/transactions/list', [TransactionsController::class, 'list'])->name('transactions.list');
    Route::get('/transactions/add', [TransactionsController::class, 'add'])->name('transactions.add');
    Route::post('/transactions/add', [TransactionsController::class, 'doAdd']);

    Route::get('notifications/read/{id}', [\App\Http\Controllers\NotificationsController::class, 'readById'])->name('notifications.read-by-id');
    Route::get('notifications/read-all', [\App\Http\Controllers\NotificationsController::class, 'readAll'])->name('notifications.read-all');
    Route::get('notifications/all', [\App\Http\Controllers\NotificationsController::class, 'list'])->name('notifications.all');

    Route::get('verify-payment/{id}', [TransactionsController::class, 'verifyPayment'])->name('verify.payment');

    Route::middleware('stopUsersFromBuyingAccounts')->group(function () {
        Route::get('account/add', [AccountController::class, 'add'])->name('accounts.add');
        Route::post('account/add', [AccountController::class, 'doAdd']);
    });
    Route::get('account/list', [AccountController::class, 'list'])->name('accounts.list');
    Route::get('account/search/{term?}', [AccountController::class, 'search'])->name('accounts.search');
    Route::get('account/change-protocol/{id}/{isMultiUser?}', [AccountController::class, 'changeProtocol'])->name('accounts.change.protocol');
    Route::post('account/change-protocol/{id}/{isMultiUser?}', [AccountController::class, 'doChangeProtocol']);
    Route::get('account/show-edit-result/{id}', [AccountController::class, 'showResultOfEdit'])->name('accounts.show.edit.results');
    Route::get('web-service/settings', [WebServiceController::class, 'settings'])->name('webservice.settings');
    Route::get('account/change-protocol/{id}', [AccountController::class, 'changeProtocol'])->name('account.change-protocol');
    Route::get('account/order-package/{id}', [AccountController::class, 'orderPackage'])->name('account.order-package');
    Route::post('account/order-package/{id}', [AccountController::class, 'doOrderPackage']);
    Route::get('account/switch-power/{id}', [AccountController::class, 'switchPower'])->name('account.switch-power');
    Route::group(['middleware' => ['auth', 'role:admin'], 'prefix' => 'admin'], function () {
        Route::get('/servers/add', [ServersController::class, 'add'])->name('servers.add');
        Route::post('/servers/add', [ServersController::class, 'doAdd']);
        Route::get('/servers/edit/{id}', [ServersController::class, 'edit'])->name('servers.edit');
        Route::post('/servers/edit/{id}', [ServersController::class, 'doEdit']);
        Route::get('/servers/delete/{id}', [ServersController::class, 'delete'])->name('servers.delete');
        Route::get('/servers/sync-accounts/{id}', [ServersController::class, 'syncAccounts'])->name('servers.sync.accounts');
        Route::get('/servers/move-accounts/{id}', [ServersController::class, 'moveAccounts'])->name('servers.move.accounts');
        Route::post('/servers/move-accounts/{id}', [ServersController::class, 'doMoveAccounts']);
        Route::get('/servers/change-configs/{id}', [ServersController::class, 'changeConfigs'])->name('servers.configs.change');
        Route::post('/servers/change-configs/{id}', [ServersController::class, 'doChangeConfigs']);
        Route::post('/servers/extra-domain-or-ip/add/{id}', [ServersController::class, 'addExtraDomainOrIp'])->name('servers.extra-domain-or-ip.add');
        Route::get('/servers/extra-domain-or-ip/delete/{id}/{itemId}', [ServersController::class, 'deleteExtraDomainOrIp'])->name('servers.extra-domain-or-ip.delete');
        Route::get('/servers/bot/monitoring', [ServersController::class, 'botMonitoring'])->name('servers.bot.monitoring');
        Route::post('/servers/bot/monitoring', [ServersController::class, 'saveBotMonitoring']);
        Route::get('/servers/bot/monitoring/more-ips', [ServersController::class, 'moreIps'])->name('servers.bot.monitoring.more-ips');
        Route::post('/servers/bot/monitoring/more-ips', [ServersController::class, 'doAddMoreIps']);
        Route::get('/servers/bot/monitoring/more-ips/delete/{id}', [ServersController::class, 'deleteMoreIps'])->name('servers.bot.monitoring.more-ips.delete');
        Route::prefix('pritunl')->group(function () {
            Route::get('add', [ServersController::class, 'addPritunlServer'])->name('pritunl.servers.add');
            Route::post('add/select-organizations', [ServersController::class, 'addPritunlServerSecondStep'])->name('pritunl.servers.add.second-step');
            Route::post('add', [ServersController::class, 'addPritunlServerFinalStep']);
        });
        Route::get('/settings/general', [SettingsController::class, 'general'])->name('settings.general');
        Route::post('/settings/general', [SettingsController::class, 'saveGeneral']);

        Route::get('/transactions/edit/{id}', [TransactionsController::class, 'edit'])->name('transactions.edit');
        Route::post('/transactions/edit/{id}', [TransactionsController::class, 'doEdit']);
        Route::get('/transactions/add-by-admin', [TransactionsController::class, 'addByAdmin'])->name('transactions.add-by-admin');
        Route::post('/transactions/add-by-admin', [TransactionsController::class, 'doAddByAdmin']);
        Route::get('/change-settled/{id}', [TransactionsController::class, 'changeSettled'])->name('transactions.change-settled');

        Route::get('/user/list', [UserController::class, 'list'])->name('user.list');
        Route::get('/user/detail/{id}', [UserController::class, 'detail'])->name('user.detail');
        Route::get('/user/edit/{id}', [UserController::class, 'edit'])->name('user.edit');
        Route::post('/user/edit/{id}', [UserController::class, 'doEdit']);
        Route::get('/user/delete/{id}', [UserController::class, 'delete'])->name('user.delete');
        Route::get('/user/settle/{id}', [UserController::class, 'autoSettleBasedOnAmount'])->name('user.settle');
        Route::post('/user/settle/{id}', [UserController::class, 'doAutoSettleBasedOnAmount']);
        Route::get('/user/change-status/{id}', [UserController::class, 'changeStatus'])->name('user.change_status');

        Route::get('/admin/account/list', [AccountController::class, 'adminList'])->name('admin.accounts.list');
        Route::get('account/edit/{id}', [AccountController::class, 'adminEdit'])->name('admin.accounts.edit');
        Route::post('account/edit/{id}', [AccountController::class, 'adminDoEdit']);
        Route::get('account/delete/{id}', [AccountController::class, 'adminDoDelete'])->name('admin.accounts.delete');

        Route::get('/package/add/{type?}', [PackageController::class, 'add'])->name('package.add');
        Route::post('/package/add/{type?}', [PackageController::class, 'doAdd']);
        Route::get('/package/list', [PackageController::class, 'list'])->name('package.list');
        Route::get('/package/edit/{type?}/{id}', [PackageController::class, 'edit'])->name('package.edit');
        Route::post('/package/edit/{type?}/{id}', [PackageController::class, 'doEdit']);
        Route::get('/package/delete/{type?}/{id}', [PackageController::class, 'delete'])->name('package.delete');

        Route::get('/alerts/list', [\App\Http\Controllers\Admin\AdminAlertsController::class, 'index'])->name('alerts.list');
        Route::get('/alerts/add', [\App\Http\Controllers\Admin\AdminAlertsController::class, 'add'])->name('alerts.add');
        Route::post('/alerts/add', [\App\Http\Controllers\Admin\AdminAlertsController::class, 'doAdd']);
        Route::get('/alerts/delete/{id}', [\App\Http\Controllers\Admin\AdminAlertsController::class, 'delete'])->name('alerts.delete');

        Route::prefix('third-party')->group(function () {

            Route::get('cloudflare', [\App\Http\Controllers\Admin\Thirdparty\CloudflareController::class, 'index'])->name('third-party.cloudflare');
            Route::get('cloudflare/add', [\App\Http\Controllers\Admin\Thirdparty\CloudflareController::class, 'add'])->name('third-party.cloudflare.add');
            Route::post('cloudflare/add', [\App\Http\Controllers\Admin\Thirdparty\CloudflareController::class, 'doAdd']);
            Route::get('cloudflare/edit/{id}', [\App\Http\Controllers\Admin\Thirdparty\CloudflareController::class, 'edit'])->name('third-party.cloudflare.edit');
            Route::post('cloudflare/edit/{id}', [\App\Http\Controllers\Admin\Thirdparty\CloudflareController::class, 'doEdit']);

        });

        Route::get('storage/link', function () {
            Artisan::call('storage:link');
            return "link process successfully completed";
        });
    });

});

Route::get('t-payment-verify/{id}', [TransactionsController::class, 'telegramVerifyPayment'])->name('telegram.payment.verify');

require __DIR__ . '/auth.php';

Route::prefix('account/pritunl')->group(function () {
    Route::get('download/{id}', [AccountController::class, 'downloadPritunlOVPN'])->name('account.pritunl.download-ovpn');
});
