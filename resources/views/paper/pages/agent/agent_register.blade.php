@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    افزودن حساب مشتری جدید ( مشتری شما بصورت آنلاین میتواند وارد شود )
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            @include('paper.components.admin-alerts')
            <div class="alert-danger" > <label class="m-3">لطفا تمامی اطلاعات رو به مشتری تحویل بدید، این اطلاعات برای ورود به حساب بسیار بسیار مهم و حیاطیست</label> </div>
            <div class="pb-3">
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card">
                            <form action="{{route('agent.doRegister')}}"
                                  method="post" onkeydown="return event.key != 'Enter';" id="create_agent_account">
                                @csrf
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>
                                                    ایمیل حساب
                                                </label>
                                                <input class="form-control" type="text" name="email" />

                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    نام حساب
                                                </label>
                                                <input type="text" class="form-control" name="name" dir="ltr"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    کلمه عبور
                                                </label>
                                                <input type="text" class="form-control" name="password" dir="ltr"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    کد فروشندگی ( این مورد میتونه متفاوت باشه )
                                                </label>
                                                <input type="text" class="form-control" name="agent_code" dir="ltr"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    کد تاییدی صلاحیت ساخت حساب
                                                </label>
                                                <input type="text" class="form-control" name="agent_accept_code" dir="ltr"/>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-success" id="submitButton">ثبت نام مشتری به مبلغ 35000 تومان</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
