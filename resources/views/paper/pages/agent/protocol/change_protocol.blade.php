@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    افزودن حساب مشتری جدید ( مشتری شما بصورت آنلاین میتواند وارد شود )
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="alert-danger" > <label class="m-3">لینک باید حتما Reality باشه، درغیر این صورت مسئولیت مسدود سازی حسابتان با خودتان میباشد</label> </div>
            <div class="pb-3">
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card">
                            <form action="{{route('agent.doChangeProtocol')}}"
                                  method="post" onkeydown="return event.key != 'Enter';" id="create_agent_account">
                                @csrf
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group h-20" style="height: 250px;">
                                                <label>
                                                    لینک حساب
                                                </label>
                                                <input class="form-control" style="height: 250px; max-lines: 10;" type="text" name="link" />

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-danger" id="submitButton">تغییر لینک (فقط همراه اولی ها)</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
