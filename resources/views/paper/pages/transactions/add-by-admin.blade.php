@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full">
    <header class="dark-blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                افزودن تراکنش
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce bg-dark">
        <div class="pb-3 bg-dark">
            <div class="row row-eq-height bg-dark">
                <!-- Daily Sale Report-->
                <div class="col-12 bg-dark">
                    <div class="card">
                        <form action="{{ route('transactions.add-by-admin') }}" method="post">
                            @csrf
                            <div class="card-body bg-dark">
                                @if ($errors->any())
                                <div class="alert alert-danger text-white">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row bg-dark">
                                    <div class="col-6 mx-auto bg-dark">
                                        <div class="form-group bg-dark">
                                            <label>
                                                برای
                                            </label>
                                            <select name="user_id" class="form-control">
                                                @foreach (getAllUsers() as $user )
                                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                نوع تراکنش
                                            </label>
                                            <select name="type" class="form-control">
                                                <option value="income">واریز</option>
                                                <option  value="outgoing">برداشت</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                مبلغ
                                            </label>
                                            <input type="number" class="form-control" name="amount" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                کد تراکنش
                                            </label>
                                            <input type="text" class="form-control" name="transaction_code" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                توضیحات
                                            </label>
                                            <input type="text" class="form-control" name="description" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer bg-dark">
                                <button class="btn btn-success">افزودن</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
