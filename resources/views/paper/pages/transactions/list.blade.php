@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full bg-dark">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                لیست تراکنش ها
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce bg-dark">
        @include('paper.components.admin-alerts')
        <div class="pb-3 bg-dark">
            <div class="row row-eq-height bg-dark">
                <!-- Daily Sale Report-->
                <div class="col-12 bg-dark">
                    <div class="card my-3 no-b bg-dark ">
                        <div class="card-header  bg-dark text-white b-0 p-3 d-flex justify-content-between ">
                            <div>
                                <h4 class="card-title">
                                    تراکنش ها
                                    -
                                    صفحه
                                    {{ $transactions->currentPage() }}
                                </h4>
                                <small class="card-subtitle mb-2 text-white">
                                    مروری بر تراکنش های شما
                                </small>
                            </div>
                            <a data-toggle="collapse" href="#salesCard" aria-expanded="false" aria-controls="salesCard">
                                <i class="icon-menu"></i>
                            </a>
                        </div>
                        <div class="collapse show" id="salesCard">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table  earning-box">
                                        <thead class="dark-grey text-white">
                                            <tr>
                                                <th>نوع تراکنش</th>
                                                <th>مبلغ</th>
                                                <th>شماره تراکنش</th>
                                                <th>توضیحات</th>
                                                <th>تاریخ تراکنش</th>
                                                <th>وضعیت</th>
                                                @if(auth()->user()->hasRole('admin'))
                                                <th>توسط</th>
                                                <th>عملیات</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($transactions as $transaction)
                                            <tr class="text-white">
                                                <td>
                                                    {!! getTransactionType($transaction->type) !!}
                                                </td>
                                                <td>
                                                    {{ number_format($transaction->amount) }}
                                                    تومان
                                                </td>
                                                <td>
                                                    <code style="overflow:scroll-x !important;width:10pxpx;">
                                                        {{ $transaction->transaction_code }}
                                                    </code>
                                                </td>
                                                <td>
                                                    {{ $transaction->description }}
                                                </td>
                                                <td>{{ $transaction->created_at->diffForHumans() }} - {{  $transaction->created_at }}</td>
                                                <td>
                                                    @if($transaction->type == 'outgoing')
                                                    {!! getTransactionSettleStatus($transaction->settled) !!}
                                                    @else
                                                    -
                                                    @endif
                                                </td>
                                                @if(auth()->user()->hasRole('admin'))
                                                <td>
                                                    {{ $transaction->user->name }}
                                                </td>
                                                <td>
                                                    <a href="{{ route('transactions.edit',$transaction->id) }}">
                                                        <button class="btn btn-sm btn-primary">
                                                            ویرایش
                                                        </button>
                                                    </a>
                                                    @if($transaction->type == 'outgoing')
                                                    <a href="{{ route('transactions.change-settled',$transaction->id) }}">
                                                        <button
                                                            class="btn btn-sm @if($transaction->settled == 0) btn-success @else btn-warning text-black @endif">
                                                            @if($transaction->settled == 0)
                                                            تسویه شده
                                                            @else
                                                            تسویه نشده
                                                            @endif
                                                        </button>
                                                    </a>
                                                    @endif
                                                </td>
                                                @endif
                                            </tr>
                                            @empty

                                            @endforelse
                                        </tbody>
                                    </table>
                                    <div class="row">
                                        <div class="col-7 mx-auto mb-5">
                                            {{ $transactions->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
