@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full bg-dark" >
    <header class="dark-blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                افزودن تراکنش
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce bg-dark">
        @include('paper.components.admin-alerts')
        <div class="pb-3 bg-dark">
            <div class="row row-eq-height bg-dark">
                <!-- Daily Sale Report-->
                <div class="col-12 bg-dark">
                    <div class="card bg-dark">
                        <form action="{{ route('transactions.add') }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="alert alert-warning text-white">
                                    مبلغ افزوده شده به کیف پول به هیچ وجه قابل استرداد نخواهد بود
                                </div>
                                <div class="alert alert-info">
                                    به کلیه مبالغ
                                    2.5
                                    درصد
                                    کارمزد درگاه پرداخت اضافه میشود
                                </div>
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-6 mx-auto">
                                        <div class="form-group">
                                            <label>
                                                مبلغ
                                                (تومان)
                                            </label>
                                            <input type="number" min="50000" max="1000000" class="form-control" name="amount" value="50000">
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                درگاه پرداخت
                                            </label>
                                            <input type="radio" name="gateway" value="zarinpal" selected> زرین پال
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-success" type="submit">
                                    پرداخت
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
