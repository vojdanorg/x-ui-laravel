@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                ویرایش تراکنش
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce">
        <div class="pb-3">
            <div class="row row-eq-height">
                <!-- Daily Sale Report-->
                <div class="col-12">
                    <div class="card">
                        <form action="{{ route('transactions.edit',$transaction->id) }}" method="post">
                            @csrf
                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
        
                                <div class="row">
                                    <div class="col-6 mx-auto">
                                        <div class="form-group">
                                            <input type="hidden" name="user_id" value="{{ $transaction->user_id }}">
                                            <label>
                                                برای
                                            </label>
                                            <select disabled class="form-control">
                                                @foreach (getAllUsers() as $user )
                                                    <option @if($user->id == $transaction->user_id) selected @endif value="{{ $user->id }}">{{ $user->name }}</option>
                                                @endforeach                       
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                نوع تراکنش
                                            </label>
                                            <select name="type" class="form-control">
                                                <option @if($transaction->type == 'income') selected @endif value="income">واریز</option>
                                                <option @if($transaction->type == 'outgoing') selected @endif value="outgoing">برداشت</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                مبلغ
                                            </label>
                                            <input type="number" class="form-control" name="amount" value="{{ $transaction->amount }}" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                کد تراکنش
                                            </label>
                                            <input type="text" class="form-control" name="transaction_code" value="{{ $transaction->transaction_code }}" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                توضیحات
                                            </label>
                                            <input type="text" class="form-control" name="description" value="{{ $transaction->description }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-success">ویرایش</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection