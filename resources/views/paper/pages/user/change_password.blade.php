@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
               تغییر رمزعبور
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce">
        @include('paper.components.admin-alerts')
        <div class="pb-3">
            <div class="card">
                <form action="{{ route('user.change-password') }}" method="post">
                    @csrf
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        <div class="row">
                            <div class="col-6 mx-auto">
                                <div class="form-group">
                                    <label>
                                        رمزعبور جدید
                                    </label>
                                    <input type="password" class="form-control" name="password" />
                                </div>
                                <div class="form-group">
                                    <label>
                                        تکرار رمزعبور جدید
                                    </label>
                                    <input type="password" class="form-control" name="password_confirmation" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-success">تغییر رمزعبور</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
