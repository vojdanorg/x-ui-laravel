@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                {{ $pageTitle }}
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce">
        @if($unSettledTransactions->count() > 0)
                <div class="row mt-3">
                    <div class="col-12">
                        <div class="alert alert-danger bold">
                            <h1>
                                    {{ $user->name }}
                                    {{ number_format($unSettledTransactions->sum('amount')) }}
                                تومان تراکنش تسویه نشده دارد
                            </h1>
                        </div>
                    </div>
                </div>
            @endif
        <div class="pb-3">
            <!--Today Tab Start-->
            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
                <div class="row my-3">
                    <div class="col-md-3">
                        <div class="counter-box white r-5 p-3">
                            <div class="p-4">
                                <div class="float-left">
                                    <span class="icon icon-note-list text-light-blue s-48"></span>
                                </div>
                                <div class="counter-title">حساب ها</div>
                                <h5 class="sc-counter mt-3">{{ $user->accounts->count() }}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="counter-box white r-5 p-3">
                            <div class="p-4">
                                <div class="float-left">
                                    <span class="icon icon-mail-envelope-open s-48"></span>
                                </div>
                                <div class="counter-title ">موجودی کیف پول</div>
                                <h5 class="mt-3">
                                    {{ number_format($user->wallet) }}
                                    تومان
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="counter-box white r-5 p-3">
                            <div class="p-4">
                                <div class="float-left">
                                    <span class="icon icon-money text-light-blue s-48"></span>
                                </div>
                                <div class="counter-title">تراکنش ها</div>
                                <h5 class="sc-counter mt-3">{{ $user->transactions->count() }}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="counter-box white r-5 p-3">
                            <div class="p-4">
                                <div class="float-left">
                                    <span class="icon icon-money s-48"></span>
                                </div>
                                <div class="counter-title ">
                                    <small>
                                    تراکنش های تسویه نشده
                                </small>
                                </div>
                                <h5 class="mt-3">
                                    {{ $unSettledTransactions->count() }}
                                </h5>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">تعداد تراکنش در روز</div>

                            <div class="card-body">

                                {!! $incomeChart->renderHtml() !!}

                            </div>

                        </div>
                    </div>
                </div>
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card my-3 no-b ">
                            <div class="card-header white b-0 p-3 d-flex justify-content-between ">
                                <div>
                                    <h4 class="card-title">
                                        حساب ها
                                        -
                                        صفحه
                                    {{ $dbAccounts->currentPage() }}
                                    </h4>
                                    <small class="card-subtitle mb-2 text-muted">
                                        مروری بر آخرین حساب های خریداری شده
                                    </small>
                                </div>

                            </div>
                            <div>
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table table-hover earning-box">
                                            <thead class="bg-light">
                                                <tr>
                                                    <th>نام حساب</th>
                                                    <th>
                                                        ترافیک مصرف شده
                                                        /
                                                        ترافیک کل
                                                    </th>
                                                    <th>پورت</th>
                                                    <th>تاریخ انقضا</th>
                                                    <th>سرور</th>
                                                    <th>وضعیت</th>
                                                    <th>آدرس سرور</th>
                                                    @if(auth()->user()->hasRole('admin'))
                                                    <th>عملیات</th>
                                                    @endif
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($accounts as $account )
                                                <tr id="account_{{ $account->id }}">
                                                    <td>
                                                        {{ $account->account_name }}
                                                    </td>
                                                    <td id="account_traffic" style="direction:ltr;text-align:left;">
                                                        <span class="badge badge-warning">درحال بارگیری</span>
                                                    </td>
                                                    <td id="account_port">
                                                        <span class="badge badge-warning">درحال بارگیری</span>
                                                    </td>
                                                    <td id="account_expires_at">
                                                        <span class="badge badge-warning">درحال بارگیری</span>
                                                    </td>
                                                    <td>
                                                        {{ @$account->servers->server_name }}
                                                    </td>
                                                    <td id="account_status">
                                                        <span class="badge badge-warning">درحال بارگیری</span>
                                                    </td>
                                                    <td id="account_address">
                                                        <span class="badge badge-warning">درحال بارگیری</span>
                                                    </td>
                                                    @if(auth()->user()->hasRole('admin'))
                                                    <td>
                                                        <a
                                                            href="{{ route('admin.accounts.edit',$account->id) }}">
                                                            <button class="btn btn-primary btn-sm"><i
                                                                    class="icon-edit"></i></button>
                                                        </a>

                                                        <a onclick="return confirm('آیا از حذف این اکانت اطمینان کامل دارید؟ مقادیر حذف شده غیرقابل بازگشت است');"
                                                            href="{{ route('admin.accounts.delete',$account->id) }}">
                                                            <button class="btn btn-danger btn-sm"><i
                                                                    class="icon-trash"></i></button>
                                                        </a>
                                                    </td>
                                                    @endif
                                                </tr>
                                                @empty

                                                @endforelse
                                            </tbody>
                                        </table>
                                       <div class="row">
                                            <div class="col-12 mx-auto mb-5">
                                                {{ $dbAccounts->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card my-3 no-b ">
                            <div class="card-header white b-0 p-3 d-flex justify-content-between ">
                                <div>
                                    <h4 class="card-title">
                                        تراکنش ها
                                        -
                                        صفحه
                                        {{ $transactions->currentPage() }}
                                    </h4>
                                    <small class="card-subtitle mb-2 text-muted">
                                        مروری بر تراکنش های شما
                                    </small>
                                </div>
                                <a data-toggle="collapse" href="#salesCard" aria-expanded="false" aria-controls="salesCard">
                                    <i class="icon-menu"></i>
                                </a>
                            </div>
                            <div class="collapse show" id="salesCard">
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table table-hover earning-box">
                                            <thead class="bg-light">
                                                <tr>
                                                    <th>نوع تراکنش</th>
                                                    <th>مبلغ</th>
                                                    <th>شماره تراکنش</th>
                                                    <th>توضیحات</th>
                                                    <th>تاریخ تراکنش</th>
                                                    <th>وضعیت</th>
                                                    @if(auth()->user()->hasRole('admin'))
                                                    <th>توسط</th>
                                                    <th>عملیات</th>
                                                    @endif
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($transactions as $transaction)
                                                <tr>
                                                    <td>
                                                        {!! getTransactionType($transaction->type) !!}
                                                    </td>
                                                    <td>
                                                        {{ number_format($transaction->amount) }}
                                                        تومان
                                                    </td>
                                                    <td>
                                                        <code style="overflow:scroll-x !important;width:10pxpx;">
                                                            {{ $transaction->transaction_code }}
                                                        </code>
                                                    </td>
                                                    <td>
                                                        {{ $transaction->description }}
                                                    </td>
                                                    <td>{{ $transaction->created_at->diffForHumans() }} - {{  $transaction->created_at }}</td>
                                                    <td>
                                                        @if($transaction->type == 'outgoing')
                                                        {!! getTransactionSettleStatus($transaction->settled) !!}
                                                        @else
                                                        -
                                                        @endif
                                                    </td>
                                                    @if(auth()->user()->hasRole('admin'))
                                                    <td>
                                                        {{ $transaction->user->name }}
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('transactions.edit',$transaction->id) }}">
                                                            <button class="btn btn-sm btn-primary">
                                                                ویرایش
                                                            </button>
                                                        </a>
                                                        @if($transaction->type == 'outgoing')
                                                        <a href="{{ route('transactions.change-settled',$transaction->id) }}">
                                                            <button
                                                                class="btn btn-sm @if($transaction->settled == 0) btn-success @else btn-warning text-black @endif">
                                                                @if($transaction->settled == 0)
                                                                تسویه شده
                                                                @else
                                                                تسویه نشده
                                                                @endif
                                                            </button>
                                                        </a>
                                                        @endif
                                                    </td>
                                                    @endif
                                                </tr>
                                                @empty

                                                @endforelse
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-7 mx-auto mb-5">
                                                {{ $transactions->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Today Tab End-->
            <!--Yesterday Tab Start-->
        </div>
    </div>
</div>
@endsection
@php
$accountList = [];
foreach ($accounts as $account){
    $accountList[] = $account->id;
}
@endphp
@push('scripts')
{!! $incomeChart->renderChartJsLibrary() !!}
{!! $incomeChart->renderJs() !!}
<script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.10/dist/clipboard.min.js"></script>
<script>
    new ClipboardJS('#copyClipboard');
</script>
<script src="{{ url('assets/js/get-account-details.js') }}"></script>
<script>
    let account = new getAccountDetails("{{ route('api.panel.get-account-details') }}");
    let accountList = JSON.parse('{{ json_encode($accountList) }}');
    $(document).ready(function() {
        $.each(accountList,function(index,value){
            account.accountDetails(value,'account_'+value);
        });
    });


</script>
@endpush
