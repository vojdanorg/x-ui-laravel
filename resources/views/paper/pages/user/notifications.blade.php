@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{ $pageTitle }}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="pb-3">
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card my-3 no-b ">
                            <div class="card-header white b-0 p-3 d-flex justify-content-between ">
                                <a data-toggle="collapse" href="#salesCard" aria-expanded="false"
                                   aria-controls="salesCard">
                                    <i class="icon-menu"></i>
                                </a>

                            </div>

                            <div class="collapse show" id="salesCard">
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table table-hover earning-box">
                                            <thead class="bg-light">
                                            <tr>
                                                <th>ردیف</th>
                                                <th>پیام</th>
                                                <th>تاریخ</th>
                                                <th>وضعیت</th>
                                                <th>عملیات</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @forelse ($notifications as $item)
                                                <tr>
                                                    <td>
                                                        {{ $loop->iteration }}
                                                    </td>
                                                    <td>
                                                        {{$item->data['data']}}
                                                    </td>
                                                    <td>
                                                        {{$item->created_at->diffForHumans()}}
                                                        <br>
                                                        {{$item->created_at}}
                                                    </td>
                                                    <td>
                                                        @if($item->read_at)
                                                            <span class="badge badge-success">خوانده شده</span>
                                                        @else
                                                            <span class="badge badge-warning" style="color:black">خوانده نشده</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{route('notifications.read-by-id',$item->id)}}">
                                                            <button class="btn btn-sm btn-success" @if($item->read_at) disabled @endif >
                                                                <i class="icon-eye"></i>
                                                            </button>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @empty

                                            @endforelse
                                            </tbody>
                                            <div class="row">
                                                <div class="col-12 mx-auto mb-5">
                                                    {{ $notifications->appends(request()->input())->links() }}
                                                </div>
                                            </div>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
