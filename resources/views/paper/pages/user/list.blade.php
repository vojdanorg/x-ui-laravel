@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="dark-blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{ $pageTitle }}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce bg-dark">
            <div class="pb-3">
                <div class="row row-eq-height bg-dark">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card my-3 no-b ">
                            <div class="card-header bg-dark b-0 p-3 d-flex justify-content-between ">
                                <div>
                                    <h4 class="card-title text-white">
                                        کاربران
                                    </h4>
                                    <small class="card-subtitle mb-2 text-white">
                                        مروری بر کاربران
                                    </small>
                                </div>
                                <a data-toggle="collapse" href="#salesCard" aria-expanded="false"
                                   aria-controls="salesCard">
                                    <i class="icon-menu"></i>
                                </a>

                            </div>

                            <div class="collapse show" id="salesCard">
                                <div class="card-body p-0 bg-dark">
                                    <div class="table-responsive">
                                        <table class="table table-hover earning-box">
                                            <thead class="bg-dark text-white">
                                            <tr>
                                                <th>نام</th>
                                                <th>ایمیل</th>
                                                <th>تعداد حساب ها</th>
                                                <th>کیف پول</th>
                                                <th>وضعیت</th>
                                                <th>تسویه نشده</th>
                                                {{--                                                <th>قیمت خرید هر حساب</th>--}}
                                                <th>عملیات</th>
                                            </tr>
                                            </thead>
                                            <tbody class="text-white">
                                            @forelse ($users as $user)
                                                <tr>
                                                    <td>
                                                        {{ $user->name }}
                                                    </td>
                                                    <td>
                                                        {{ $user->email }}
                                                    </td>
                                                    <td>
                                                        {{ $user->accounts->count() }}
                                                    </td>
                                                    <td>
                                                        {{ number_format($user->wallet)}}
                                                        تومان
                                                    </td>
                                                    <td>
                                                        @if(checkIfUserByIdIsOnline($user->id))
                                                            <span class="badge badge-success">آنلاین</span>
                                                        @else
                                                            <span class="badge badge-danger">آفلاین</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ number_format(getUnSetteledByUserId($user->id))}}
                                                        تومان
                                                    </td>
                                                    {{--                                                <td>--}}
                                                    {{--                                                    @if(is_null($user->price_per_account))--}}
                                                    {{--                                                    {{ number_format(get_static_option('default_account_price')) }}--}}
                                                    {{--                                                    @else--}}
                                                    {{--                                                    {{ number_format($user->price_per_account) }}--}}
                                                    {{--                                                    @endif--}}
                                                    {{--                                                    تومان--}}
                                                    {{--                                                </td>--}}
                                                    <td>
                                                        <a href="{{ route('user.detail',$user->id) }}">
                                                            <button class="btn btn-sm btn-info">
                                                                <i class="icon-eye"></i>
                                                            </button>
                                                        </a>
                                                        <a href="{{ route('user.edit',$user->id) }}">
                                                            <button class="btn btn-sm btn-primary">
                                                                <i class="icon-edit"></i>
                                                            </button>
                                                        </a>
                                                        <a href="{{ route('user.settle',$user->id) }}">
                                                            <button class="btn btn-sm btn-success" title="تسویه حساب">
                                                                <i class="icon-calculator"></i>
                                                            </button>
                                                        </a>
                                                        @php
                                                            $userStatus = checkIfUserIsBlocked($user->id);
                                                        @endphp
                                                        <a href="{{ route('user.change_status',$user->id) }}">
                                                            <button class="btn btn-sm @if($userStatus) btn-success @else btn-danger @endif"
                                                                    title="@if($userStatus) آنبلاک @else بلاک @endif">
                                                                <i class="icon-block"></i>
                                                            </button>
                                                        </a>
                                                        {{--                                                    <a href="{{ route('servers.delete',$user->id) }}">--}}
                                                        {{--                                                        <button class="btn btn-sm btn-danger">--}}
                                                        {{--                                                            <i class="icon-trash"></i>--}}
                                                        {{--                                                        </button>--}}
                                                        {{--                                                    </a>--}}
                                                    </td>
                                                </tr>
                                            @empty

                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
