
@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                {{ $pageTitle }}
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce">
        <div class="pb-3">
            <div class="row row-eq-height">
                <!-- Daily Sale Report-->
                <div class="col-12">
                    <div class="card">
                        <form action="{{ route('user.edit',$user->id) }}" method="post">
                            @csrf
                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
        
                                <div class="row">
                                    <div class="col-6 mx-auto">
                                        <div class="form-group">
                                            <label>
                                                نام
                                            </label>
                                            <input type="text" class="form-control" name="name" value="{{ $user->name }}" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                ایمیل
                                            </label>
                                            <input type="text" class="form-control" name="email" value="{{ $user->email }}" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                    قیمت خرید هر حساب
                                                <small>
                                                    برای قیمت پیشفرض خالی بگذارید
                                                </small>
                                            </label>
                                            
                                            <input type="text" class="form-control" name="price_per_account" value="{{ $user->price_per_account }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-success">ویرایش</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection