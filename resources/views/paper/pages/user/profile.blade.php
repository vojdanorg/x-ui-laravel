@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                پروفایل کاربری
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce">
        <div class="pb-3">
            <div class="card">
                <form action="{{ route('user.profile') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>
                                        نام و نام خانوادگی
                                    </label>
                                    <input type="text" value="{{ auth()->user()->name }}" disabled class="form-control"
                                        name="name" />
                                </div>
                                <div class="form-group">
                                    <label>
                                        آدرس ایمیل
                                    </label>
                                    <input type="text" value="{{ auth()->user()->email }}" disabled class="form-control"
                                        name="email" />
                                </div>
                                <div class="form-group">
                                    <label>
                                        قیمت فروش
                                        (تومان)
                                    </label>
                                    <input type="number" value="{{ auth()->user()->sell_price }}" class="form-control"
                                        name="sell_price" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-success" type="submit">
                            بروز رسانی
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection