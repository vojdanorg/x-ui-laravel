@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{ $pageTitle }}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="pb-3">
                <div class="row row-eq-height mt-3">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    حداکثر مبلغ قابل تسویه:
                                    {{number_format($unSettledTransactions->sum('amount'))}}
                                    تومان
                                </div>
                            </div>
                            <form action="{{ route('user.settle',$user->id) }}" method="post">
                                @csrf
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>
                                                    مبلغ تسویه
                                                </label>
                                                <input type="number" min="0" max="{{$unSettledTransactions->sum('amount')}}" class="form-control" name="amount"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button onclick="return confirm('آیا از مقدار وارد شده اطمینان کامل دارید؟ مقادیر تغییر کرده غیرقابل بازگشت است');" class="btn btn-success">تسویه حساب</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
