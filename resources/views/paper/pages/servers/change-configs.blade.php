@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{ $pageTitle }}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="pb-3">
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card">
                            <form action="{{route('servers.configs.change',$server->id)}}" method="post">
                                @csrf
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-6 mx-auto">

                                            <div class="form-group">
                                                <label>
                                                    تغییر کانفیگ برای کاربران سرور زیر انجام خواهد شد
                                                </label>
                                                <input type="text" class="form-control" disabled
                                                       value="{{ $server->server_name }}" />
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    پروتکل
                                                </label>
                                                <select name="protocol" class="form-control" id="protocol" >
                                                    <option value="vmess">vmess</option>
                                                    <option value="vless" selected>vless</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Transmission
                                                </label>
                                                <select name="transmission" class="form-control" id="transmission_selector">
                                                    <option value="tcp" selected>tcp</option>
                                                    <option value="websocket">websocket</option>
                                                </select>
                                            </div>
                                            <hr>
                                            <small class="text-danger mt-3 d-block mb-3 text-center">
                                                مقادیر زیر اجباری نیستند، درصورتی که میخواید این مقادیر در سرور خالی باشند، این مقادیر را حالت پیشفرض (چک باکس ها خاموش و فیلد ها خالی) قرار دهید
                                            </small>
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <div class="material-switch float-left">
                                                        <input id="tls_protocol" role="switch" name="tls_protocol"
                                                               type="checkbox">
                                                        <label for="tls_protocol" class="bg-primary"></label>
                                                    </div>
                                                    TLS
                                                </li>
                                            </ul>
                                            <ul class="list-group" id="xtls_box">
                                                <li class="list-group-item">
                                                    <div class="material-switch float-left">
                                                        <input id="xtls_protocol" role="switch" name="xtls_protocol"
                                                               type="checkbox">
                                                        <label for="xtls_protocol" class="bg-primary"></label>
                                                    </div>
                                                    XTLS
                                                </li>
                                            </ul>
                                            <div class="form-group">
                                                <label>
                                                    دامنه
                                                </label>
                                                <input type="text" class="form-control" name="domain" dir="ltr" value="{{$server->hostname}}" />
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    آدرس فایل کلید عمومی
                                                </label>
                                                <input type="text" class="form-control" name="public_key" dir="ltr"  placeholder="/etc/letsencrypt/live/{{$server->hostname}}/fullchain.pem" />
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    آدرس فایل کلید خصوصی
                                                </label>
                                                <input type="text" class="form-control" name="private_key" dir="ltr"  placeholder="/etc/letsencrypt/live/{{$server->hostname}}/privkey.pem" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="card-footer">
                            <button
                                onclick="return confirm('آیا از انجام این عملیات اطمینان دارید؟ برای تمام کاربران این سرور باید لینک جدید ارسال شود.');"
                                class="btn btn-success">تغییر</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $("#tls_protocol").change(function() {
                if($(this).is(':checked')) {
                    $('#xtls_protocol').prop('checked', false);
                }
            });
            $("#xtls_protocol").change(function() {
                if($(this).is(':checked')) {
                    $('#tls_protocol').prop('checked', false);
                }
            });

            $('#protocol').change(function(){
                if($(this).val() === 'vmess' || $('#transmission_selector').val() === 'websocket' ){
                    $('#xtls_box').hide();
                    $('#xtls_protocol').prop('checked', false);
                }else{
                    $('#xtls_box').show();
                }
            });
            $('#transmission_selector').change(function(){
                if($(this).val() === 'websocket' || $('#protocol').val() === 'vmess'){
                    $('#xtls_box').hide();
                    $('#xtls_protocol').prop('checked', false);
                }else{
                    $('#xtls_box').show();
                }
            });
        });
    </script>
@endpush
