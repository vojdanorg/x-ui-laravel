@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full">
    <header class="dark-blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                افزودن سرور
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce bg-dark">
        <div class="pb-3 bg-dark">
            <div class="row row-eq-height bg-dark">
                <!-- Daily Sale Report-->
                <div class="col-12 bg-dark">
                    <div class="card bg-dark">
                        <form action="{{ route('servers.add') }}" method="post">
                            @csrf
                            <div class="card-body  bg-dark">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row  bg-dark">
                                    <div class="col-6 mx-auto bg-dark">
                                        <div class="form-group bg-dark">
                                            <label>
                                                نام سرور
                                            </label>
                                            <input type="text" class="form-control" name="server_name" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                ظرفیت
                                            </label>
                                            <input type="number" class="form-control" name="max_users" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                دامنه ایران
                                            </label>
                                            <input type="text" class="form-control" name="hostname" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                ایپی ایران
                                            </label>
                                            <input type="text" class="form-control" name="iran_ip" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                Panel Port
                                            </label>
                                            <input type="text" class="form-control" name="panel_port" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                Panel Cookie
                                            </label>
                                            <input type="text" class="form-control" name="panel_cookie" />
                                        </div>
                                        <hr>
                                        <label>اطلاعات بیشتر سرور</label>
                                        <br>
                                        <div class="form-group">
                                            <label>
                                                Server Provider Website
                                            </label>
                                            <input type="text" class="form-control" name="server_provider_website" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                Direct Server Domain
                                            </label>
                                            <input type="text" class="form-control" name="tunnel_ip" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                Direct Server IP
                                            </label>
                                            <input type="text" class="form-control" name="eu_ip" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                Direct Panel Cookie
                                            </label>
                                            <input type="text" class="form-control" name="tunnel_cookie" />
                                        </div>
                                        <hr>
                                        <br>
                                        <h4>
                                            تنظیمات SSL
                                            <small>
                                                درصورتی که
                                                SSL
                                                ندارید
                                                /
                                                قرار دهید
                                            </small>
                                        </h4>
                                        <div class="form-group">
                                            <label>
                                                آدرس فایل کلید عمومی
                                            </label>
                                            <input type="text" class="form-control" name="public_key" dir="ltr"  placeholder="e.g /etc/letsencrypt/live/google.com/fullchain.pem" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                آدرس فایل کلید خصوصی
                                            </label>
                                            <input type="text" class="form-control" name="private_key" dir="ltr"  placeholder="e.g /etc/letsencrypt/live/google.com/privkey.pem" />
                                        </div>
                                        <hr>
                                        <br>
                                        <h4>
                                            تنظیمات CDN
                                            -
                                            <small>
                                                دقت کنید تنها از Reverse Proxy پشتیبانی میشود
                                                -
                                                درصورتی که این تنظیمات را نمیخواهید آن را خالی بگذارید
                                            </small>
                                        </h4>
                                        <div class="form-group mt-3">
                                            <label>
                                                دامنه CDN
                                            </label>
                                            <input type="text" class="form-control" placeholder="e.g cdn.google.com" name="cdn_domain">
                                        </div>
                                        <div class="form-group mt-3">
                                            <label>
                                                Reverse Proxy Path
                                            </label>
                                            <small>
                                                مقدار را بدون اسلش در ابتدا و انتها قرار دهید
                                            </small>
                                            <input type="text" class="form-control" placeholder="e.g downloader" name="cdn_path">
                                        </div>

                                        <div class="form-group mt-3">
                                            <label>
                                               پورت HTTPS
                                            </label>
                                            <input type="text" class="form-control" placeholder="e.g 443" name="https_port">
                                        </div>
                                        <hr>
                                        <br>
                                        <h4>
                                            ارتباط با کلودفلیر
                                        </h4>
                                        <div class="form-group mt-3">
                                            <label>
                                                اکانت کلودفلیر
                                            </label>
                                            <select name="cloudflare_id" class="form-control">
                                                <option value="0">
                                                    هیچکدام
                                                </option>
                                                @forelse($cloudflares as $cloudflare)
                                                    <option value="{{$cloudflare->id}}">{{$cloudflare->name}}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-success">افزودن</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
