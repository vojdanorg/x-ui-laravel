@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full bg-dark">
    <header class="dark-blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                وضعیت سرور ها
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce bg-dark">
        @include('paper.components.admin-alerts')
        <div class="pb-3 bg-dark">
            <div class="row row-eq-height bg-dark">
                <!-- Daily Sale Report-->
                <div class="col-12 bg-dark">
                    <div class="card my-3 no-b bg-dark">
                        <div class="card-header b-0 p-3 d-flex justify-content-between bg-dark text-white">
                            <div>
                                <h4 class="card-title text-white">
                                    سرور های
                                    V2Ray
                                </h4>
                            </div>
                            <a data-toggle="collapse" href="#salesCard" aria-expanded="false" aria-controls="salesCard">
                                <i class="icon-menu"></i>
                            </a>
                        </div>
                        <div class="show bg-dark text-white" id="salesCard">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table earning-box bg-dark">
                                        <thead class="bg-dark text-white">
                                            <tr class="text-white">
                                                <th>نام سرور</th>
                                                <th>ظرفیت کل</th>
                                                <th>ظرفیت استفاده شده</th>
                                                <th>وضعیت</th>
                                                @if(auth()->user()->hasRole('admin'))
                                                <th>عملیات</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($v2ray_servers as $server)
                                            <tr id="server_{{ $server->id }}" class="text-white">
                                                <td>
                                                    {{ $server->server_name }}
                                                </td>
                                                <td>
                                                    {{ $server->max_users }}
                                                </td>
                                                <td id="server_clients">
                                                    <span class="badge badge-warning">
                                                        درحال بارگیری
                                                    </span>
                                                </td>

                                                <td id="server_status">
                                                    <span class="badge badge-warning">
                                                        درحال بارگیری
                                                    </span>
                                                </td>
                                                @if(auth()->user()->hasRole('admin'))
                                                <td>
                                                    <a href="{{ route('servers.edit',$server->id) }}">
                                                        <button class="btn btn-sm btn-primary">
                                                            ویرایش
                                                        </button>
                                                    </a>
                                                    <a href="{{ route('servers.delete',$server->id) }}" onclick="return confirm('آیا از حذف این سرور اطمینان کامل دارید؟ مقادیر حذف شده غیرقابل بازگشت است');">
                                                        <button class="btn btn-sm btn-danger">
                                                            حذف
                                                        </button>
                                                    </a>
                                                    <a href="{{ route('servers.sync.accounts',$server->id) }}" title="همگام سازی اکانت ها">
                                                        <button class="btn btn-sm btn-success">
                                                            <i class="icon-sync text-white"></i>
                                                        </button>
                                                    </a>
                                                    <a href="{{ route('servers.move.accounts',$server->id) }}" title="انتقال همه کاربران به سرور دیگر">
                                                        <button class="btn btn-sm btn-info">
                                                            <i class="icon-settings_ethernet text-white"></i>
                                                        </button>
                                                    </a>
                                                    <a href="{{ route('servers.configs.change',$server->id) }}" title="تغییر کانفیگ همه کاربران">
                                                        <button class="btn btn-sm btn-warning">
                                                            <i class="icon-cogs text-white"></i>
                                                        </button>
                                                    </a>
                                                </td>
                                                @endif
                                            </tr>
                                            @empty

                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card my-3 no-b ">
                        <div class="card-header bg-dark b-0 p-3 d-flex justify-content-between ">
                            <div>
                                <h4 class="card-title text-white">
                                    سرور های
                                    OpenVPN
                                </h4>
                            </div>
                            <a data-toggle="collapse" href="#salesCard" aria-expanded="false" aria-controls="salesCard">
                                <i class="icon-menu"></i>
                            </a>
                        </div>
                        <div class="show" id="salesCard">
                            <div class="card-body p-0 bg-dark">
                                <div class="table-responsive">
                                    <table class="table table-hover earning-box">
                                        <thead class="bg-dark">
                                        <tr class="text-white">
                                            <th>نام سرور</th>
                                            <th>ظرفیت کل</th>
                                            <th>ظرفیت استفاده شده</th>
                                            <th>وضعیت</th>
                                            @if(auth()->user()->hasRole('admin'))
                                                <th>عملیات</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse ($pritunl_servers as $server)
                                            <tr id="server_{{ $server->id }}">
                                                <td>
                                                    {{ $server->server_name }}
                                                </td>
                                                <td>
                                                    {{ $server->max_users }}
                                                </td>
                                                <td id="server_clients">
                                                    <span class="badge badge-warning">
                                                        درحال بارگیری
                                                    </span>
                                                </td>
                                                <td id="server_status">
                                                    <span class="badge badge-warning">
                                                        درحال بارگیری
                                                    </span>
                                                </td>
                                                @if(auth()->user()->hasRole('admin'))
                                                    <td>
                                                        <a href="{{ route('servers.edit',$server->id) }}">
                                                            <button class="btn btn-sm btn-primary">
                                                                ویرایش
                                                            </button>
                                                        </a>
                                                        <a href="{{ route('servers.delete',$server->id) }}" onclick="return confirm('آیا از حذف این سرور اطمینان کامل دارید؟ مقادیر حذف شده غیرقابل بازگشت است');">
                                                            <button class="btn btn-sm btn-danger">
                                                                حذف
                                                            </button>
                                                        </a>
                                                    </td>
                                                @endif
                                            </tr>
                                        @empty

                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@php
$serverList = [];
foreach ($v2ray_servers as $server){
    $serverList[] = $server->id;
}
foreach ($pritunl_servers as $server){
    $serverList[] = $server->id;
}
@endphp
@push('scripts')
<script src="{{ url('assets/js/servers-status.js') }}"></script>
<script>
    let server = new serversStatus("{{ route('api.panel.server-stauts') }}");
    let serverList = JSON.parse('{{ json_encode($serverList) }}');
    console.log(serverList);
    $(document).ready(function() {
        $.each(serverList,function(index,value){
            server.getServerDetails(value,'server_'+value);
        });
    });


</script>
@endpush
