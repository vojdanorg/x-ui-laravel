@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full bg-dark">
        <header class="dark-blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    ویرایش سرور
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce bg-dark">
            <div class="pb-3 bg-dark">
                <div class="row row-eq-height mt-3 bg-dark">
                    <!-- Daily Sale Report-->
                    <div class="col-6 bg-dark">
                        <div class="card bg-dark">
                            <div class="card-header bg-dark">
                                <div class="card-title text-white">
                                    مشخصات سرور
                                </div>
                            </div>
                            <form action="{{ route('servers.edit',$server->id) }}" method="post">
                                @csrf
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-12 mx-auto">
                                            <div class="form-group">
                                                <label>
                                                    نام سرور
                                                </label>
                                                <input type="text" class="form-control" name="server_name"
                                                       value="{{ $server->server_name }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    ظرفیت
                                                </label>
                                                <input type="number" class="form-control" name="max_users"
                                                       value="{{ $server->max_users }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    دامنه ایران
                                                </label>
                                                <input type="text" class="form-control" name="hostname"
                                                       value="{{ $server->hostname }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    ایپی ایران
                                                </label>
                                                <input type="text" class="form-control" name="iran_ip"
                                                       value="{{ $server->iran_ip }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Panel Port
                                                </label>
                                                <input type="text" class="form-control" name="panel_port"
                                                       value="{{ $server->panel_port }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Panel Cookie
                                                </label>
                                                <input type="text" class="form-control" name="panel_cookie"
                                                       value="{{ $server->panel_cookie }}"/>
                                            </div>
                                            <hr>
                                            <label>اطلاعات بیشتر سرور</label>
                                            <br>
                                            <div class="form-group">
                                                <label>
                                                    Server Provider website
                                                </label>
                                                <input type="text" class="form-control" name="server_provider_website"
                                                       value="{{ $server->server_provider_website }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Direct Server Domain
                                                </label>
                                                <input type="text" class="form-control" name="tunnel_ip"
                                                       value="{{ $server->tunnel_ip }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Direct Server IP
                                                </label>
                                                <input type="text" class="form-control" name="eu_ip"
                                                       value="{{ $server->eu_ip }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Direct Panel Cookie
                                                </label>
                                                <input type="text" class="form-control" name="tunnel_cookie"
                                                       value="{{ $server->tunnel_cookie }}"/>
                                            </div>
                                            <h4>
                                                تنظیمات SSL
                                                <small>
                                                    درصورتی که
                                                    SSL
                                                    ندارید
                                                    /
                                                    قرار دهید
                                                </small>
                                            </h4>
                                            <div class="form-group">
                                                <label>
                                                    آدرس فایل کلید عمومی
                                                </label>
                                                <input type="text" class="form-control" name="public_key" dir="ltr"
                                                       placeholder="e.g /etc/letsencrypt/live/google.com/fullchain.pem"
                                                       value="{{@$server->json_data['keys']['public_key']}}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    آدرس فایل کلید خصوصی
                                                </label>
                                                <input type="text" class="form-control" name="private_key" dir="ltr"
                                                       placeholder="e.g /etc/letsencrypt/live/google.com/privkey.pem"
                                                       value="{{@$server->json_data['keys']['private_key']}}"/>
                                            </div>
                                            <hr>
                                            <br>
                                            <h4>
                                                تنظیمات CDN
                                                -
                                                <small>
                                                    دقت کنید تنها از Reverse Proxy پشتیبانی میشود
                                                    -
                                                    درصورتی که این تنظیمات را نمیخواهید آن را خالی بگذارید
                                                </small>
                                            </h4>
                                            <div class="form-group mt-3">
                                                <label>
                                                    دامنه CDN
                                                </label>
                                                <input type="text" class="form-control" placeholder="e.g cdn.google.com"
                                                       name="cdn_domain"
                                                       value="{{@$server->json_data['cdn']['domain']}}">
                                            </div>
                                            <div class="form-group mt-3">
                                                <label>
                                                    Reverse Proxy Path
                                                </label>
                                                <small>
                                                    مقدار را بدون اسلش در ابتدا و انتها قرار دهید
                                                </small>
                                                <input type="text" class="form-control" placeholder="e.g downloader"
                                                       name="cdn_path" value="{{@$server->json_data['cdn']['path']}}">
                                            </div>

                                            <div class="form-group mt-3">
                                                <label>
                                                    پورت HTTPS
                                                </label>
                                                <input type="text" class="form-control" placeholder="e.g 443"
                                                       name="https_port"
                                                       value="{{@$server->json_data['cdn']['https_port']}}">
                                            </div>
                                            <hr>
                                            <br>
                                            <h4>
                                                ارتباط با کلودفلیر
                                            </h4>
                                            <div class="form-group mt-3">
                                                <label>
                                                    اکانت کلودفلیر
                                                </label>
                                                @php

                                                    if(!isset($server->json_data['cloudflare'])){
                                                        $cloudFlareID = 0;
                                                    }else{
                                                         $cloudFlareID = $server->json_data['cloudflare'];
                                                    }
                                                @endphp
                                                <select name="cloudflare_id" class="form-control">
                                                    <option value="0">
                                                        هیچکدام
                                                    </option>
                                                    @forelse($cloudflares as $cloudflare)
                                                        <option
                                                            @if($cloudflare->id == $cloudFlareID) selected
                                                            @endif value="{{$cloudflare->id}}">{{$cloudflare->name}}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-success">ویرایش</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-6 bg-dark">
                        <div class="row bg-dark">
                            <div class="col-12 bg-dark">
                                <div class="card bg-dark">
                                    <div class="card-header">
                                        <div class="card-title text-white">
                                            سایر دامنه ها و ایپی های سرور
                                        </div>
                                    </div>
                                    <form action="{{ route('servers.extra-domain-or-ip.add',$server->id) }}"
                                          method="post">
                                        @csrf
                                        <div class="card-body">
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            <div class="row">
                                                <div class="col-12 mx-auto">
                                                    <div class="form-group">
                                                        <label>
                                                            دامنه/ایپی
                                                        </label>
                                                        <input type="text" class="form-control"
                                                               name="extra_domain_or_ip"
                                                               value=""/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button class="btn btn-success btn-sm">افزودن</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-12 mt-3 bg-dark">
                                <div class="card bg-dark text-white">
                                    <div class="card-header bg-dark">
                                        <div class="card-title">
                                            لیست دامنه ها و ایپی های سرور
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover earning-box">
                                                <thead class="bg-light">
                                                <tr>
                                                    <th>
                                                        دامنه
                                                        /
                                                        ایپی
                                                    </th>
                                                    <th>عملیات</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(isset($server->json_data['extra_domain_or_ip']))
                                                    @forelse ($server->json_data['extra_domain_or_ip'] as $key => $item )
                                                        <tr>
                                                            <td>
                                                                {{$item}}
                                                            </td>
                                                            <td>
                                                                <a href="{{route('servers.extra-domain-or-ip.delete',[$server->id,$key])}}">
                                                                    <button class="btn btn-danger btn-sm">
                                                                        <i class="icon-trash"></i>
                                                                    </button>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @empty

                                                    @endforelse
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
