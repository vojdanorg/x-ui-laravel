@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                {{ $pageTitle }}
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce">
        <div class="pb-3">
            <div class="row row-eq-height">
                <!-- Daily Sale Report-->
                <div class="col-12">
                    <div class="card">
                        @if($accountsWithChangedPort)
                            @forelse ($accountsWithChangedPort as $acc )
                                <div class="alert alert-warning">
                                    اکانت با ریمارک
                                    {{ $acc['previous']['remark'] }}
                                    در پنل 
                                    X-UI
                                    و ریمارک
                                    {{ $acc['ondb']->account_name }}
                                    در پنل
                                    از پورت
                                    {{ $acc['previous']['port'] }} 
                                    به
                                    {{ $acc['new_port'] }}
                                    تغییر کرد
                                </div>
                            @empty
                                
                            @endforelse
                        @endif
                        <form action="{{ route('servers.move.accounts',$server->id) }}" method="post">
                            @csrf
                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col-6 mx-auto">
                                        <div class="form-group">
                                            <label>
                                                از
                                            </label>
                                            <input type="text" class="form-control" disabled
                                                value="{{ $server->server_name }}" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                به
                                            </label>
                                            <select name="destination_server_id" class="form-control">
                                                @foreach ($servers as $sv)
                                                <option value="{{ $sv->id }}">{{ $sv->server_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <div class="material-switch float-left">
                                                    <input id="remove-on-origin-server" role="switch" name="remove_on_origin" checked
                                                        type="checkbox">
                                                    <label for="remove-on-origin-server" class="bg-primary"></label>
                                                </div>
                                                حذف کاربران از سرور مبدا
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="card-footer">
                        <button
                            onclick="return confirm('آیا از انجام این عملیات اطمینان دارید؟ برای تمام کاربران این سرور باید لینک جدید ارسال شود.');"
                            class="btn btn-success">انتقال</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection