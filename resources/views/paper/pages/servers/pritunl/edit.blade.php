@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{$pageTitle}}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="pb-3">
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card">
                            <form action="{{ route('servers.edit',$server->id) }}" method="post">
                                @csrf
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>
                                                    نام سرور
                                                </label>
                                                <input type="text" class="form-control" name="server_name"
                                                       value="{{$server->server_name}}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    دامنه
                                                    Pritunl
                                                </label>
                                                <input type="text" class="form-control" name="server_domain"
                                                       value="{{$server->tunnel_ip}}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    API TOKEN
                                                </label>
                                                <input type="text" class="form-control" name="api_token"
                                                       value="{{$server->json_data['pritunl_data']['api_token']}}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    API SECRET
                                                </label>
                                                <input type="text" class="form-control" name="api_secret"
                                                       value="{{$server->json_data['pritunl_data']['api_secret']}}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    ظرفیت
                                                </label>
                                                <input type="number" class="form-control" name="max_users"
                                                       value="{{$server->max_users}}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Organization
                                                </label>
                                                <select name="organization_id" required class="form-control">
                                                    @forelse($response as $item)
                                                        <option value="{{$item['id']}}"
                                                                @if($server->json_data['pritunl_data']['organization_id'] == $item['id']) checked @endif>{{$item['name']}}</option>
                                                    @empty
                                                        <option>
                                                            لطفا حداقل یک
                                                            Organization
                                                            ایجاد کنید
                                                        </option>
                                                    @endforelse
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Server
                                                </label>
                                                <select name="server_id" required class="form-control">
                                                    @forelse($servers as $item)
                                                        <option value="{{$item['id']}}"
                                                                @if($server->json_data['pritunl_data']['server_id'] == $item['id']) checked @endif>{{$item['name']}}</option>
                                                    @empty
                                                        <option>
                                                            لطفا حداقل یک
                                                            Server
                                                            ایجاد کنید
                                                        </option>
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-success">ذخیره</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
