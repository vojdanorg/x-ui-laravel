@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                {{$pageTitle}}
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce">
        <div class="pb-3">
            <div class="row row-eq-height">
                <!-- Daily Sale Report-->
                <div class="col-12">
                    <div class="card">
                        <form action="{{ route('pritunl.servers.add.second-step') }}" method="post">
                            @csrf
                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col-6 mx-auto">
                                        <div class="form-group">
                                            <label>
                                                نام سرور
                                            </label>
                                            <input type="text" class="form-control" name="server_name" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                دامنه
                                                Pritunl
                                            </label>
                                            <input type="text" class="form-control" name="server_domain" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                API TOKEN
                                            </label>
                                            <input type="text" class="form-control" name="api_token" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                API SECRET
                                            </label>
                                            <input type="text" class="form-control" name="api_secret" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                ظرفیت
                                            </label>
                                            <input type="number" class="form-control" name="max_users" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-success">مرحله بعد</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
