@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{$pageTitle}}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="pb-3">
                <div class="row row-eq-height mt-3">
                    <!-- Daily Sale Report-->
                    <div class="col-6">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    افزودن ایپی جدید
                                </div>
                            </div>
                            <form action="{{ route('servers.bot.monitoring.more-ips') }}" method="post">
                                @csrf
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-12 mx-auto">
                                            <div class="form-group">
                                                <label>
                                                    آدرس ایپی
                                                </label>
                                                <input type="text" class="form-control" name="ip_address" dir="ltr"
                                                       required
                                                       value=""/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    ارائه دهنده
                                                </label>
                                                <input type="text" class="form-control" name="provider" dir="ltr"
                                                       required
                                                       value=""/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    برای
                                                </label>
                                                <select name="server_id" class="form-control" required>
                                                    @forelse($servers as $server)
                                                        <option
                                                            value="{{$server->id}}">{{$server->server_name}}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-success">ذخیره</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    لیست ایپی ها
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 mx-auto">
                                        <div class="collapse show rounded" id="salesCard">
                                            <div class="card-body p-0 rounded">
                                                <div class="table-responsive rounded">
                                                    <table class="table table-hover earning-box rounded">
                                                        <thead class="bg-light">
                                                        <tr>
                                                            <th>آدرس ایپی</th>
                                                            <th>ارائه دهنده</th>
                                                            <th>وضعیت</th>
                                                            <th>برای</th>
                                                            <th>عملیات</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @forelse($additionalIPS as $item)
                                                            <tr>
                                                                <td>
                                                                    {{$item->ip_address}}
                                                                </td>
                                                                <td>
                                                                    {{$item->provider}}
                                                                </td>
                                                                <td>
                                                                    @switch($item->status)
                                                                        @case(1)
                                                                        <span class="badge badge-success">
                                                                            درحال استفاده
                                                                        </span>
                                                                        @break
                                                                        @case(2)
                                                                        <span class="badge badge-danger">
                                                                            فیلتر شده
                                                                        </span>
                                                                        @break
                                                                        @case(0)
                                                                        <span class="badge badge-warning">
                                                                            در صف
                                                                        </span>
                                                                        @break
                                                                    @endswitch

                                                                </td>
                                                                <td>
                                                                    {{@$item->server->server_name}}
                                                                </td>
                                                                <td>
                                                                    <a href="{{route('servers.bot.monitoring.more-ips.delete',$item->id)}}"
                                                                       onclick="return confirm('آیا از حذف این ایپی اطمینان کامل دارید؟ مقادیر حذف شده غیرقابل بازگشت است');">
                                                                        <button class="btn btn-danger btn-sm">
                                                                            <i class="icon-trash"></i>
                                                                        </button>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @empty
                                                            چیزی یافت نشد
                                                        @endforelse
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <div class="row">
                                                    <div class="col-12 mx-auto mb-5">
                                                        {{ $additionalIPS->appends(request()->input())->links() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
