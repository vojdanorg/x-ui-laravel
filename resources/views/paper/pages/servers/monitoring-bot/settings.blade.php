@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{$pageTitle}}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="pb-3">
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card">
                            <form action="{{ route('servers.bot.monitoring') }}" method="post">
                                @csrf
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>
                                                    توکن روبات
                                                </label>
                                                <input type="text" class="form-control" name="robot_token" dir="ltr"
                                                       value="{{get_static_option('monitorbot_robot_token')}}"/>
                                            </div>
                                            <hr>
                                            <h3 class="mt-3">
                                                هاست های دلخواه
                                                check-host.com
                                            </h3>
                                            <div class="form-group">
                                                <label>
                                                    هاست اول
                                                </label>
                                                <input type="text" class="form-control" name="first_check_host_name"
                                                       value="{{get_static_option('monitorbot_first_check_host_name')}}"
                                                       placeholder="e.g ir1.node.check-host.net" dir="ltr"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    هاست دوم
                                                </label>
                                                <input type="text" class="form-control" name="second_check_host_name"
                                                       value="{{get_static_option('monitorbot_second_check_host_name')}}"
                                                       placeholder="e.g ir3.node.check-host.net" dir="ltr"/>
                                            </div>
                                            <hr>
                                            <div class="form-group">
                                                <label>
                                                    ایدی عددی
                                                    مدیر(ان)
                                                    در تلگرام
                                                    <small class="text-danger">
                                                        برای ایجاد ادمین های بیشتر، با کاما ایدی ها را از هم جدا کنید.
                                                    </small>
                                                </label>
                                                <input type="text" class="form-control"
                                                       value="{{get_static_option('monitorbot_admin_telegram_number_id')}}"
                                                       name="admin_telegram_number_id" dir="ltr"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    تعداد دفعات تست با چک هاست
                                                </label>
                                                <input type="number" min="1" max="10" class="form-control"
                                                       value="{{get_static_option('monitorbot_retry_attempts')}}"
                                                       name="retry_attempts" dir="ltr"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    میزان مکث بین درخواست چک و دریافت نتیجه از چک هاست
                                                    (به ثانیه)
                                                </label>
                                                <input type="number" min="10" max="120" class="form-control"
                                                       value="{{get_static_option('monitorbot_wait_for_check_result')}}"
                                                       name="wait_for_check_result" dir="ltr"/>
                                            </div>
                                            <h4>
                                                سرورهای مجاز به بررسی
                                            </h4>
                                            <br>
                                            <div class="collapse show rounded" id="salesCard">
                                                <div class="card-body p-0 rounded">
                                                    <div class="table-responsive rounded">
                                                        <table class="table table-hover earning-box rounded">
                                                            <thead class="bg-light">
                                                            <tr>
                                                                <th>انتخاب</th>
                                                                <th>نام سرور</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @forelse($servers as $server)
                                                                <tr id="server_{{$server->id}}">
                                                                    <td>
                                                                        <input type="checkbox"
                                                                               @if(checkIfServerIsAllowedToBeMonitored($server->id))
                                                                                   checked
                                                                               @endif
                                                                               name="allowed_servers[]"
                                                                               value="{{$server->id}}">
                                                                    </td>
                                                                    <td>
                                                                        {{$server->server_name}}
                                                                    </td>
                                                                </tr>
                                                            @empty
                                                            @endforelse
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <ul class="list-group">
                                                    <li class="list-group-item">
                                                        <div class="material-switch float-left">
                                                            <input id="bot_status" role="switch" name="bot_status"
                                                                   @if(get_static_option('monitorbot_bot_status')) checked
                                                                   @endif
                                                                   type="checkbox">
                                                            <label for="bot_status" class="bg-primary"></label>
                                                        </div>
                                                        وضعیت روبات
                                                        (روشن/خاموش)
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-success">ذخیره</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
