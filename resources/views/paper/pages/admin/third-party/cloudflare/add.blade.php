@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{$pageTitle}}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="pb-3">
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card my-3 no-b ">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div id="salesCard">
                                <form action="{{route('third-party.cloudflare.add')}}" method="post">
                                    @csrf
                                    <div class="card-body px-5">
                                        <div class="form-group">
                                            <label>
                                                نام
                                            </label>
                                            <input type="text" class="form-control" name="name" placeholder="e.g domain.name">
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                ایمیل
                                            </label>
                                            <input type="email" class="form-control" name="email" placeholder="e.g email@domain.com" dir="ltr">
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                Api Token
                                            </label>
                                            <input type="text" class="form-control" name="api_token" placeholder="e.g Eziq4Cr1yODgby52fMvH732i0n1F0gsL4QEFCgzYk-" dir="ltr">
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                Zone ID
                                            </label>
                                            <input type="text" class="form-control" name="zone_id" placeholder="e.g ejc43ftd2u5439jeg7k816345h573437-" dir="ltr">
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                Global Key
                                            </label>
                                            <input type="text" class="form-control" name="global_key" placeholder="e.g bf78a8545kedcf136u7fhj687b044cj5mc5d0-" dir="ltr">
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-success btn-sm" type="submit">افزودن</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
