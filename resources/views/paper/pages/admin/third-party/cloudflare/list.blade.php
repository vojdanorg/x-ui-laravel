@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{$pageTitle}}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            @include('paper.components.admin-alerts')
            <div class="pb-3">
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card my-3 no-b ">
                            <div class="card-header white b-0 p-3 d-flex justify-content-between ">
                                <div>
                                    <h4 class="card-title">
                                        <a href="{{route('third-party.cloudflare.add')}}">
                                            <button class="btn btn-success btn-sm">
                                                افزودن
                                            </button>
                                        </a>
                                    </h4>
                                </div>
                                <a data-toggle="collapse" href="#salesCard" aria-expanded="false"
                                   aria-controls="salesCard">
                                    <i class="icon-menu"></i>
                                </a>
                            </div>
                            <div class="collapse show" id="salesCard">
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table table-hover earning-box">
                                            <thead class="bg-light">
                                            <tr>
                                                <th>نام</th>
                                                <th>زون</th>
                                                <th>ایمیل</th>
                                                @if(auth()->user()->hasRole('admin'))
                                                    <th>عملیات</th>
                                                @endif
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @forelse ($cloudflares as $cloudflare)
                                                <tr>
                                                    <td>
                                                        {{$cloudflare->name}}
                                                    </td>
                                                    <td>
                                                        {{$cloudflare->zone_id}}
                                                    </td>
                                                    <td>
                                                        {{$cloudflare->email}}
                                                    </td>
                                                    <td>
                                                        <a href="{{route('third-party.cloudflare.edit',$cloudflare->id)}}">
                                                            <button class="btn btn-primary btn-sm">
                                                                <i class="icon-edit"></i>
                                                            </button>
                                                        </a>
                                                        <button class="btn btn-danger btn-sm">
                                                            <i class="icon-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @empty

                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
