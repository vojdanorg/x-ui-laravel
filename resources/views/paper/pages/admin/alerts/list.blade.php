@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="dark-blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{$pageTitle}}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce bg-dark">
            <div class="pb-3 bg-dark">

                <div class="row row-eq-height bg-dark">
                    <!-- Daily Sale Report-->
                    <div class="col-12 bg-dark">
                        <div class="card my-3 no-b  bg-dark">
                            <div class="card-header  bg-dark b-0 p-3 d-flex justify-content-between">
                                <div>
                                    <a href="{{route('alerts.add')}}">
                                        <button class="btn btn-success btn-sm">افزودن</button>
                                    </a>
                                </div>
                                <a data-toggle="collapse" href="#salesCard" aria-expanded="false"
                                   aria-controls="salesCard">
                                    <i class="icon-menu"></i>
                                </a>

                            </div>

                            <div class="collapse show" id="salesCard">
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table table-hover earning-box">
                                            <thead class="bg-dark">
                                            <tr class="text-white">
                                                <th>ردیف</th>
                                                <th>پیام</th>
                                                <th>نوع</th>
                                                <th>عملیات</th>
                                            </tr>
                                            </thead>
                                            <tbody class="text-white">
                                            @forelse ($alerts as $alert)
                                                <tr>
                                                    <td>
                                                        {{$alert->id}}
                                                    </td>
                                                    <td>{{$alert->message}}</td>
                                                    <td>
                                                        <span class="badge badge-{{$alert->class}} alert-sm">
                                                            @switch($alert->class)
                                                                @case('danger')
                                                                اخطاری
                                                                @break
                                                                @case('success')
                                                                موفقیت آمیز
                                                                @break
                                                                @case('warning')
                                                                هشداری
                                                                @break
                                                                @case('info')
                                                                خبری
                                                            @endswitch
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a href="{{route('alerts.delete',$alert->id)}}"
                                                           onclick="return confirm('آیا از حذف این اطلاعیه اطمینان کامل دارید؟ مقادیر حذف شده غیرقابل بازگشت است');">
                                                            <button class="btn btn-danger btn-sm">
                                                                <i class="icon-trash"></i>
                                                            </button>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @empty

                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
