@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{$pageTitle}}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="pb-3">
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card my-3 no-b ">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div id="salesCard">
                                <form action="{{route('alerts.add')}}" method="post">
                                    @csrf
                                <div class="card-body px-5">
                                    <div class="form-group">
                                        <label>نوع</label>
                                        <select name="class" class="form-control">
                                            <option value="danger">اخطاری</option>
                                            <option value="warning">هشداری</option>
                                            <option value="info">خبری</option>
                                            <option value="success">موفقیت آمیز</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>پیام</label>
                                        <textarea name="message" class="form-control"></textarea>
                                    </div>
                                </div>
                                    <div class="card-footer">
                                        <button class="btn btn-success btn-sm" type="submit">افزودن</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
