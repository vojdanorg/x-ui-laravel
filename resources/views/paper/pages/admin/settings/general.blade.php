@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="dark-blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    تنظیمات اصلی
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce bg-dark">
            <div class="pb-3 bg-dark">
                <div class="row row-eq-height bg-dark">
                    <!-- Daily Sale Report-->
                    <div class="col-md-12 mt-5 bg-dark">
                        <div class="card bg-dark">
                            <form action="{{ route('settings.general') }}" method="post">
                                @csrf
                                <div class="card-body bg-dark">
                                    @if ($errors->any())
                                        <div class="alert alert-danger text-white">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-md-6 mx-auto">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <ul class="list-group">
                                                            <li class="list-group-item">
                                                                <div class="material-switch float-left">
                                                                    <input id="tunnel_ip_for_server_connection"
                                                                           role="switch"
                                                                           name="tunnel_ip_for_server_connection"
                                                                           @if(get_static_option('tunnel_ip_for_server_connection')) checked
                                                                           @endif
                                                                           type="checkbox">
                                                                    <label for="tunnel_ip_for_server_connection"
                                                                           class="bg-primary"></label>
                                                                </div>
                                                                استفاده از ایپی تونل برای دریافت اطلاعات
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <ul class="list-group">
                                                            <li class="list-group-item">
                                                                <div class="material-switch float-left">
                                                                    <input id="stop_users_from_buying_accounts"
                                                                           role="switch"
                                                                           name="stop_users_from_buying_accounts"
                                                                           @if(get_static_option('stop_users_from_buying_accounts')) checked
                                                                           @endif
                                                                           type="checkbox">
                                                                    <label for="stop_users_from_buying_accounts"
                                                                           class="bg-primary"></label>
                                                                </div>
                                                                بستن خرید سرویس کاربری برای همه کاربران
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <ul class="list-group">
                                                            <li class="list-group-item">
                                                                <div class="material-switch float-left">
                                                                    <input
                                                                        id="count_disabled_services_as_server_capacity"
                                                                        role="switch"
                                                                        name="count_disabled_services_as_server_capacity"
                                                                        @if(get_static_option('count_disabled_services_as_server_capacity')) checked
                                                                        @endif
                                                                        type="checkbox">
                                                                    <label
                                                                        for="count_disabled_services_as_server_capacity"
                                                                        class="bg-primary"></label>
                                                                </div>
                                                                احتساب سرویس های غیرفعال در شمارش ظرفیت سرور
                                                                (روشن/خاموش)
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <ul class="list-group">
                                                            <li class="list-group-item">
                                                                <div class="material-switch float-left">
                                                                    <input
                                                                        id="auto_delete_deactivated_clients_on_servers"
                                                                        role="switch"
                                                                        name="auto_delete_deactivated_clients_on_servers"
                                                                        @if(get_static_option('auto_delete_deactivated_clients_on_servers')) checked
                                                                        @endif
                                                                        type="checkbox">
                                                                    <label
                                                                        for="auto_delete_deactivated_clients_on_servers"
                                                                        class="bg-primary"></label>
                                                                </div>
                                                                حذف خودکار حساب های غیرفعال
                                                                (روشن/خاموش)
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <ul class="list-group">
                                                            <li class="list-group-item">
                                                                <div class="material-switch float-left">
                                                                    <input id="allowed_user_register"
                                                                           role="switch"
                                                                           name="allowed_user_register"
                                                                           @if(get_static_option('allowed_user_register')) checked
                                                                           @endif
                                                                           type="checkbox">
                                                                    <label for="allowed_user_register"
                                                                           class="bg-primary"></label>
                                                                </div>
                                                                امکان ثبت نام کاربر جدید
                                                                (روشن/خاموش)
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <ul class="list-group">
                                                            <li class="list-group-item">
                                                                <div class="material-switch float-left">
                                                                    <input id="show_resellers_leaderboard_publicly"
                                                                           role="switch"
                                                                           name="show_resellers_leaderboard_publicly"
                                                                           @if(get_static_option('show_resellers_leaderboard_publicly')) checked
                                                                           @endif
                                                                           type="checkbox">
                                                                    <label for="show_resellers_leaderboard_publicly"
                                                                           class="bg-primary"></label>
                                                                </div>
                                                                نمایش رتبه بندی روزانه فروشندگان بر اساس تعداد فروش
                                                                (روشن/خاموش)
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>
                                                            هاست برای قرار دادن در لینک کلاینت
                                                        </label>
                                                        <input type="text" class="form-control" name="default_uri_hosts"
                                                               dir="ltr"
                                                               value="{{ get_static_option('default_uri_hosts') }}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>
                                                            آدرس سرویس های مولتی یوزر
                                                        </label>
                                                        <input type="text" class="form-control"
                                                               name="multi_user_services_addr"
                                                               dir="ltr"
                                                               value="{{ get_static_option('multi_user_services_addr') }}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>
                                                            تعداد روزهای در انتظار تمدید حساب‌های غیرفعال قبل از حذف
                                                            خودکار
                                                        </label>
                                                        <input type="number" min="1" class="form-control"
                                                               name="auto_delete_deactivated_clients_on_servers_wait_before_delete"
                                                               dir="ltr"
                                                               value="{{ get_static_option('auto_delete_deactivated_clients_on_servers_wait_before_delete') }}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>
                                                            هاست برای امنیت (REALITY) فقط (TLS3)
                                                        </label>
                                                        <input type="text" placeholder="account.zula.ir:443"  class="form-control"
                                                               name="reality_server_dest"
                                                               dir="ltr"
                                                               value="{{ get_static_option('reality_server_dest') }}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>
                                                            نام سرور برای  (REALITY) فقط (TLS3)
                                                        </label>
                                                        <input type="text" placeholder="account.zula.ir" class="form-control"
                                                               name="reality_server_names"
                                                               dir="ltr"
                                                               value="{{ get_static_option('reality_server_names') }}"/>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>
                                                            پورت برای  (REALITY) فقط (TLS3)
                                                        </label>
                                                        <input type="text" placeholder="443" class="form-control"
                                                               name="reality_server_port"
                                                               dir="ltr"
                                                               value="{{ get_static_option('reality_server_port') }}"/>
                                                    </div>
                                                </div>


                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>
                                                            کلید خصوصی برای (REALITY)
                                                        </label>
                                                        <input type="text" placeholder="GD1oTwgOIh3hBuSXb2OnPtCx0a8_FwBBZh_Gk840qGU" class="form-control"
                                                               name="reality_private_key"
                                                               dir="ltr"
                                                               value="{{ get_static_option('reality_private_key') }}"/>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>
                                                            کلید عمومی برای (REALITY)
                                                        </label>
                                                        <input type="text" placeholder="cDaDzPr3PlS3NM8lreHZbdo" class="form-control"
                                                               name="reality_public_key"
                                                               dir="ltr"
                                                               value="{{ get_static_option('reality_public_key') }}"/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-success">ذخیره</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
