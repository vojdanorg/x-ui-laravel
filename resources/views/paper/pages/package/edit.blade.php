@extends('paper.layouts.master')
@php
    $isMultiUser = false;
        if(isset($package->protocol['multi_user'])){
            if($package->protocol['multi_user']){
                $isMultiUser = true;
            }
        }
@endphp
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="dark-blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{ $pageTitle }}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce bg-dark">
            <div class="pb-3 bg-dark">
                <div class="row row-eq-height bg-dark">
                    <!-- Daily Sale Report-->
                    <div class="col-12 bg-dark">
                        <div class="card bg-dark">
                            <form action="{{ route('package.edit',['v2ray',$package->id]) }}" method="post">
                                @csrf
                                <div class="card-body bg-dark">
                                    @if ($errors->any())
                                        <div class="alert alert-danger text-white">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-6 mx-auto">

                                            <div class="form-group">
                                                <label>
                                                    نام پکیج
                                                </label>
                                                <input type="text" class="form-control" name="name"
                                                       value="{{ $package->name }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    قیمت
                                                    (تومان)
                                                </label>
                                                <input type="number" class="form-control" name="price"
                                                       value="{{ $package->price }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    انقضا
                                                    (روز)
                                                </label>
                                                <input type="number" class="form-control" name="expire_at"
                                                       value="{{ $package->expire_at }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    پهنای باند
                                                    (گیگابایت)
                                                </label>
                                                <input type="number" class="form-control" name="bandwidth"
                                                       value="{{ $package->bandwidth }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    تعداد کاربر
                                                </label>
                                                <input type="number" class="form-control" name="user_limit"
                                                       value="{{ $package->user_limit }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    توضیحات پکیج (معرفی به فروشنده در زمان ایجاد سرویس جدید)
                                                </label>
                                                <input type="text" class="form-control" name="description" value="{{$package->description}}"/>
                                            </div>
                                            <small class="text-danger mt-3 d-block mb-3 text-center">
                                                بالانس بسته را انتخاب کنید
                                            </small>
                                            <ul class="form-group">
                                                <select name="price_level" class="form-control" id="price_level" >
                                                    <option value="High">High</option>
                                                    <option value="Medium"> Medium </option>
                                                    <option value="Low"> low </option>
                                                </select>

                                            </ul>
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <div class="material-switch float-left">
                                                        <input id="status" role="switch" name="status"
                                                               @if($package->status == 1) checked @endif
                                                               type="checkbox">
                                                        <label for="status" class="bg-primary"></label>
                                                    </div>
                                                    وضعیت
                                                    (فعال/غیرفعال)
                                                </li>
                                            </ul>
                                            <hr>
                                            <div class="form-group">
                                                <label>
                                                    پروتکل
                                                </label>
                                                <select name="protocol" class="form-control" id="protocol">
                                                    <option value="vmess"
                                                            @if(@$package->protocol['protocol'] === 'vmess') selected @endif>
                                                        vmess
                                                    </option>
                                                    <option value="vless"
                                                            @if(@$package->protocol['protocol'] === 'vless') selected @endif>
                                                        vless
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Transmission
                                                </label>
                                                <select name="transmission" class="form-control"
                                                        id="transmission_selector">
                                                    <option value="tcp"
                                                            @if(@$package->protocol['transmission'] === 'tcp') selected @endif>
                                                        tcp
                                                    </option>
                                                    <option value="websocket"
                                                            @if(@$package->protocol['transmission'] === 'ws') selected @endif>
                                                        websocket
                                                    </option>
                                                </select>
                                            </div>
                                            <hr>
                                            <small class="text-danger mt-3 d-block mb-3 text-center">
                                                مقادیر زیر اجباری نیستند، درصورتی که میخواید این مقادیر در سرور خالی
                                                باشند، این مقادیر را حالت پیشفرض (چک باکس ها خاموش و فیلد ها خالی) قرار
                                                دهید
                                            </small>
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <div class="material-switch float-left">
                                                        <input id="tls_protocol" role="switch" name="tls_protocol"
                                                               @if(@$package->protocol['ssl_protocol'] === 'tls') checked
                                                               @endif
                                                               type="checkbox">
                                                        <label for="tls_protocol" class="bg-primary"></label>
                                                    </div>
                                                    TLS
                                                </li>
                                            </ul>
                                            <ul class="list-group" id="xtls_box">
                                                <li class="list-group-item">
                                                    <div class="material-switch float-left">
                                                        <input id="xtls_protocol" role="switch" name="xtls_protocol"
                                                               @if(@$package->protocol['ssl_protocol'] === 'xtls') checked
                                                               @endif
                                                               type="checkbox">
                                                        <label for="xtls_protocol" class="bg-primary"></label>
                                                    </div>
                                                    XTLS
                                                </li>
                                            </ul>
                                            <hr>
                                            <small class="text-danger mt-3 d-block mb-3 text-center"
                                                   id="reality_helper">
                                                مقدار زیر مشخص کننده استفاده از امنیت پروتکل ریلیتی میباشد
                                                درصورتی که نمیخواهید استفاده کنید، لطفا تیک آن را
                                            </small>

                                            <ul class="list-group" id="reality_box">
                                                <li class="list-group-item">
                                                    <div class="material-switch float-left">
                                                        <input id="reality_security" role="switch"
                                                               name="reality_security"
                                                               @if(@$package->protocol['reality_security'] === true) checked
                                                               @endif
                                                               type="checkbox">
                                                        <label for="reality_security" class="bg-primary"></label>
                                                    </div>
                                                    REALITY SECURITY
                                                </li>
                                            </ul>
                                            <hr>
                                            <br>
                                            <h3>
                                                تنظیمات چند کاربر روی یک پورت
                                            </h3>
                                            <br>
                                            <div class="form-group">
                                                <label>
                                                    پکیج همگام
                                                </label>
                                                @php
                                                    $selectedSyncPackage = false;
                                                        if(isset($package->protocol['in_sync_package_id'])){
                                                            if($package->protocol['in_sync_package_id']){
                                                                $selectedSyncPackage = $package->protocol['in_sync_package_id'];
                                                            }
                                                        }
                                                @endphp
                                                <select name="in_sync_package_id" class="form-control">
                                                    <option value="false">هیچکدام</option>
                                                    @forelse($packages as $pack)
                                                        <option
                                                            value="{{$pack->id}}"
                                                            @if((int)$selectedSyncPackage == $pack->id) selected @endif>

                                                            {{ $pack->name }}
                                                            -
                                                            {{ $pack->bandwidth }}
                                                            گیگابایت
                                                            -
                                                            {{ number_format($pack->price) }}
                                                            تومان
                                                        </option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>
                                            <ul class="list-group" id="xtls_box">
                                                <li class="list-group-item">
                                                    <div class="material-switch float-left">
                                                        <input id="allow_multi_user" role="switch"
                                                               @if($isMultiUser) checked @endif
                                                               name="allow_multi_user"
                                                               type="checkbox">
                                                        <label for="allow_multi_user" class="bg-primary"></label>
                                                    </div>
                                                    فعال
                                                </li>
                                            </ul>
                                            <div id="allow_multi_user_list">
                                                <br>
                                                <small>
                                                    سرور و اکانت مستر مورد نظر رو انتخاب کنید
                                                </small>
                                                <br>
                                                <small class="text-danger">
                                                    با فعال سازی این سرویس، تمامی اکانت های ساخته شده تحت کانفیگ اکانت
                                                    مستر ساخته میشوند. به معناست که سایر تنظیمات پکیج، بجز تنظیمات حجم و
                                                    زمان انقضا درنظر گرفته نمیشوند!
                                                </small>
                                                <br>
                                                <div class="collapse show rounded" id="salesCard">
                                                    <div class="card-body p-0 rounded">
                                                        <div class="table-responsive rounded">
                                                            <table class="table table-hover earning-box rounded">
                                                                <thead class="bg-light">
                                                                <tr>
                                                                    <th>انتخاب</th>
                                                                    <th>نام سرور</th>
                                                                    <th> اکانت مستر</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @forelse($servers as $server)
                                                                    <tr id="server_{{$server->id}}">
                                                                        <td>
                                                                            <input type="checkbox"
                                                                                   @if(checkIfServerIsAllowedForPackageToBeMultiUser($server,$package)) checked
                                                                                   @endif
                                                                                   name="allowed_servers[]"
                                                                                   value="{{$server->id}}">
                                                                        </td>
                                                                        <td>
                                                                            {{$server->server_name}}
                                                                        </td>
                                                                        <td id="server_accounts">
                                                                        <span class="badge badge-warning"
                                                                              id="loading_badge">
                                                                            درحال بارگیری
                                                                        </span>
                                                                            <select name="master_accounts[]"
                                                                                    class="form-control"
                                                                                    id="accounts_list">
                                                                                <option value="-1">
                                                                                    انتخاب کنید ...
                                                                                </option>
                                                                            </select>
                                                                        </td>
                                                                    </tr>
                                                                @empty
                                                                @endforelse
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-success">ویرایش</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#allow_multi_user_list').hide();
            $("#tls_protocol").change(function () {
                if ($(this).is(':checked')) {
                    $('#xtls_protocol').prop('checked', false);
                    $('#reality_security').prop('checked', false);
                }
            });
            $("#xtls_protocol").change(function () {
                if ($(this).is(':checked')) {
                    $('#tls_protocol').prop('checked', false);
                    $('#reality_security').prop('checked', false);

                }
            });

            $("#reality_security").change(function () {
                if ($(this).is(':checked')) {
                    $('#xtls_protocol').prop('checked', false);
                    $('#tls_protocol').prop('checked', false);
                }
            });

            $('#protocol').change(function () {
                if ($(this).val() === 'vmess' || $('#transmission_selector').val() === 'websocket') {
                    $('#xtls_box').hide();
                    $('#reality_box').hide();
                    $('#reality_security').prop('checked', false);
                    $('#reality_helper').hide();
                    $('#xtls_protocol').prop('checked', false);
                } else {
                    $('#xtls_box').show();
                    $('#reality_box').show();
                    $('#reality_helper').show();
                }
            });
            $('#transmission_selector').change(function () {
                if ($(this).val() === 'websocket' || $('#protocol').val() === 'vmess') {
                    $('#xtls_box').hide();
                    $('#xtls_protocol').prop('checked', false);
                } else {
                    $('#xtls_box').show();
                }
            });

            @if($isMultiUser)
            $('#allow_multi_user_list').show();
            @endif
            //Allow MultiUser
            $("#allow_multi_user").change(function () {
                if ($(this).is(':checked')) {
                    $('#allow_multi_user_list').slideDown(200);
                } else {
                    $('#allow_multi_user_list').slideUp(200);
                }
            });

        });
    </script>
    @php
        $serverList = [];
        foreach ($servers as $server){
            $serverList[] = $server->id;
        }
        $selectedAccounts = [];
        if($isMultiUser){
            foreach ($package->protocol['multi_user'] as $item){
                $selectedAccounts[$item['server']] = $item['master_account'];
            }
        }
    @endphp

    <script src="{{ url('assets/js/servers-accounts.js') }}"></script>
    <script>
        let server = new serversAccounts("{{ route('api.panel.admin.packages.get-accounts') }}");
        let serverList = JSON.parse('{{ json_encode($serverList) }}');
        {{--let selectedAccounts = JSON.parse('{{ json_encode($selectedAccounts) }}');--}}
        let selectedAccounts = {!! json_encode($selectedAccounts) !!};
        $(document).ready(function () {
            $.each(serverList, function (index, value) {
                server.getServerAccounts(value, 'server_' + value, selectedAccounts);
            });
        });


    </script>
@endpush
