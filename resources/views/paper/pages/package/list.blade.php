@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="dark-blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{ $pageTitle }}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce bg-dark">
            <div class="pb-3 bg-dark">
                <div class="row row-eq-height bg-dark">
                    <!-- Daily Sale Report-->
                    <div class="col-12 bg-dark">
                        <div class="card my-3 no-b bg-dark">
                            <div class="card-header  bg-dark b-0 p-3 d-flex justify-content-between ">
                                <div>
                                    <h4 class="card-title">
                                        {{ $pageTitle }}
                                    </h4>
                                    <small class="card-subtitle mb-2 text-muted">
                                        مروری بر پکیج ها
                                    </small>
                                </div>
                                <a data-toggle="collapse" href="#salesCard" aria-expanded="false"
                                   aria-controls="salesCard">
                                    <i class="icon-menu"></i>
                                </a>
                            </div>
                            <div class="collapse show" id="salesCard">
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table table-hover earning-box">
                                            <thead class="bg-dark">
                                            <tr class="text-white">
                                                <th>نام پکیج</th>
                                                <th>نوع</th>
                                                <th>قیمت</th>
                                                <th>انقضا</th>
                                                <th>پهنای باند</th>
                                                <th>تعداد کاربر</th>
                                                <th>سرویس های فعال</th>
                                                <th>توضیحات</th>
                                                <th>وضعیت</th>
                                                @if(auth()->user()->hasRole('admin'))
                                                    <th>وضعیت مبلغ</th>
                                                    <th>عملیات</th>
                                                @endif
                                            </tr>
                                            </thead>
                                            <tbody class="text-white">
                                            @forelse ($packages as $package)
                                                @php
                                                    $type = 'v2ray';
                                                    if(isset($package->protocol['type'])){
                                                        $type = $package->protocol['type'];
                                                    }
                                                @endphp
                                                <tr>
                                                    <td>{{ $package->name }}</td>
                                                    <td>
                                                        @if($type == 'v2ray')
                                                            <div class="badge badge-info" >
                                                                V2Ray
                                                            </div>
                                                        @elseif($type == 'pritunl')
                                                            <div class="badge badge-warning" style="color:black;">
                                                                OpenVPN
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ number_format($package->price) }}
                                                        تومان
                                                    </td>
                                                    <td>
                                                        {{ $package->expire_at }}
                                                        روزه
                                                    </td>
                                                    <td>
                                                        {{ $package->bandwidth }} GB
                                                    </td>
                                                    <td>
                                                        {{ $package->user_limit }}
                                                        کاربره
                                                    </td>
                                                    <td>
                                                        {{ $package->accounts->count() }}
                                                    </td>
                                                    <td>
                                                        {{ $package->description }}
                                                    </td>
                                                    <td>
                                                        @if($package->status == 1)
                                                            <span class="badge badge-success">فعال</span>
                                                        @else
                                                            <span class="badge badge-danger">غیرفعال</span>
                                                        @endif
                                                    </td>
                                                    @if(auth()->user()->hasRole('admin'))
                                                        <td>
                                                            <span>{{$package->price_level}}</span>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('package.edit',[$type,$package->id]) }}">
                                                                <button class="btn-sm btn btn-primary">ویرایش</button>
                                                            </a>
                                                            <a href="{{ route('package.delete',[$type,$package->id]) }}"
                                                               onclick="return confirm('آیا از حذف این پکیج اطمینان کامل دارید؟ مقادیر حذف شده غیرقابل بازگشت است');">
                                                                <button class="btn-sm btn btn-danger">حذف</button>
                                                            </a>
                                                        </td>
                                                    @endif
                                                </tr>
                                            @empty

                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
