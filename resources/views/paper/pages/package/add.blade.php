@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full">
    <header class="dark-blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                {{ $pageTitle }}
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce bg-dark">
        <div class="pb-3 bg-dark">
            <div class="row row-eq-height bg-dark">
                <!-- Daily Sale Report-->
                <div class="col-12 bg-dark">
                    <div class="card bg-dark">
                        <form action="{{ route('package.add','v2ray') }}" method="post">
                            @csrf
                            <div class="card-body bg-dark">
                                @if ($errors->any())
                                <div class="alert alert-danger text-white">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col-6 mx-auto">

                                        <div class="form-group">
                                            <label>
                                                نام پکیج
                                            </label>
                                            <input type="text" class="form-control" name="name" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                قیمت
                                                (تومان)
                                            </label>
                                            <input type="number" class="form-control" name="price" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                انقضا
                                                (روز)
                                            </label>
                                            <input type="number" class="form-control" name="expire_at" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                پهنای باند
                                                (گیگابایت)
                                            </label>
                                            <input type="number" class="form-control" name="bandwidth" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                تعداد کاربر
                                            </label>
                                            <input type="number" class="form-control" name="user_limit" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                توضیحات پکیج (معرفی به فروشنده در زمان ایجاد سرویس جدید)
                                            </label>
                                            <input type="text" class="form-control" name="description" />
                                        </div>
                                        <small class="text-danger mt-3 d-block mb-3 text-center">
                                            بالانس بسته را انتخاب کنید
                                        </small>
                                        <ul class="form-group">
                                            <select name="price_level" class="form-control" id="price_level" >
                                                <option value="High">High</option>
                                                <option value="Medium"> Medium </option>
                                                <option value="Low"> low </option>
                                            </select>

                                        </ul>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <div class="material-switch float-left">
                                                    <input id="status" role="switch" name="status" checked
                                                           type="checkbox">
                                                    <label for="status" class="bg-primary"></label>
                                                </div>
                                                وضعیت
                                                (فعال/غیرفعال)
                                            </li>
                                        </ul>
                                        <hr>
                                        <div class="form-group">
                                            <label>
                                                پروتکل
                                            </label>
                                            <select name="protocol" class="form-control" id="protocol" >
                                                <option value="vmess">vmess</option>
                                                <option value="vless" selected>vless</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                Transmission
                                            </label>
                                            <select name="transmission" class="form-control" id="transmission_selector">
                                                <option value="tcp" selected>tcp</option>
                                                <option value="websocket">websocket</option>
                                            </select>
                                        </div>
                                        <hr>
                                        <small class="text-danger mt-3 d-block mb-3 text-center">
                                            مقادیر زیر اجباری نیستند، درصورتی که میخواید این مقادیر در سرور خالی باشند، این مقادیر را حالت پیشفرض (چک باکس ها خاموش و فیلد ها خالی) قرار دهید
                                        </small>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <div class="material-switch float-left">
                                                    <input id="tls_protocol" role="switch" name="tls_protocol"
                                                           type="checkbox">
                                                    <label for="tls_protocol" class="bg-primary"></label>
                                                </div>
                                                TLS
                                            </li>
                                        </ul>
                                        <ul class="list-group" id="xtls_box">
                                            <li class="list-group-item">
                                                <div class="material-switch float-left">
                                                    <input id="xtls_protocol" role="switch" name="xtls_protocol"
                                                           type="checkbox">
                                                    <label for="xtls_protocol" class="bg-primary"></label>
                                                </div>
                                                XTLS
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-success">افزودن</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $("#tls_protocol").change(function() {
                if($(this).is(':checked')) {
                    $('#xtls_protocol').prop('checked', false);
                }
            });
            $("#xtls_protocol").change(function() {
                if($(this).is(':checked')) {
                    $('#tls_protocol').prop('checked', false);
                }
            });

            $('#protocol').change(function(){
                if($(this).val() === 'vmess' || $('#transmission_selector').val() === 'websocket' ){
                    $('#xtls_box').hide();
                    $('#xtls_protocol').prop('checked', false);
                }else{
                    $('#xtls_box').show();
                }
            });
            $('#transmission_selector').change(function(){
                if($(this).val() === 'websocket' || $('#protocol').val() === 'vmess'){
                    $('#xtls_box').hide();
                    $('#xtls_protocol').prop('checked', false);
                }else{
                    $('#xtls_box').show();
                }
            });
        });
    </script>
@endpush
