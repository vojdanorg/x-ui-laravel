@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{ $pageTitle }}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="pb-3">
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card">
                            <form action="{{ route('package.add','pritunl') }}" method="post">
                                @csrf
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-6 mx-auto">

                                            <div class="form-group">
                                                <label>
                                                    نام پکیج
                                                </label>
                                                <input type="text" class="form-control" name="name"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    قیمت
                                                    (تومان)
                                                </label>
                                                <input type="number" class="form-control" name="price"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    انقضا
                                                    (روز)
                                                </label>
                                                <input type="number" class="form-control" name="expire_at"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    پهنای باند
                                                    (گیگابایت)
                                                </label>
                                                <input type="text" class="form-control" value="نامحدود" disabled/>
                                            </div>
                                            <hr>
                                            <div class="alert alert-info">
                                                سایر تنظیمات بر اساس تنظیمات سرور ایجاد شده خواهد بود. برای مثال اگر
                                                تنظیمات محدود کردن به ۲ دستگاه ایجاد کرده باشید، ‌این تنظیمات نیز لحاظ
                                                خواهند شد.
                                            </div>
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <div class="material-switch float-left">
                                                        <input id="status" role="switch" name="status" checked
                                                               type="checkbox">
                                                        <label for="status" class="bg-primary"></label>
                                                    </div>
                                                    وضعیت
                                                    (فعال/غیرفعال)
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-success">افزودن</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
