@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full bg-dark">
    <header class="dark-blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                 {{ $pageTitle }}
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce bg-dark">
        @include('paper.components.admin-alerts')
        <div class="pb-3 bg-dark">
            <div class="card bg-dark">
                <form method="post">
                    <div class="card-body">

                        <div class="alert alert-warning text-white">
                            در حفظ و نگهداری توکن خود دقت فرمایید. درصورت فاش شدن توکن شما، عواقب آن بر عهده شما خواهد بود.
                        </div>
                        <div class="alert alert-warning text-white">
                            درصورتی که توکن شما مشکوک به فاش شدن است. در اولین فرصت برای بروزرسانی توکن با ادمین در ارتباط باشید.
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>
                                        توکن وب سرویس شما
                                    </label>
                                    <input type="text" value="{{ auth()->user()->api_token }}" class="form-control"
                                        name="name" />
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection
