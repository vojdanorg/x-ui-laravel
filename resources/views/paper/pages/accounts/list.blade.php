@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full dark-grey">
        <header class="dark-blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{ $pageTitle }}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            @include('paper.components.admin-alerts')
            <div class="pb-3">
                <!--Today Tab Start-->
                <div class="tab-pane animated fadeInUpShort show activ" id="v-pills-1">


                    <div class="row row-eq-height">
                        <!-- Daily Sale Report-->
                        <div class="col-12">
                            <div class="card my-3 no-b">
                                <div class="card-header b-0 p-3 d-flex justify-content-between dark-grey">
                                    <div>
                                        <h4 class="card-title">
                                            حساب ها
                                            -
                                            صفحه
                                            {{ $dbAccounts->currentPage() }}
                                        </h4>
                                        <small class="card-subtitle mb-2 text-white dark-grey">
                                            مروری بر آخرین حساب های خریداری شده
                                        </small>
                                    </div>
                                </div>
                                <div class="dark-grey">
                                    <div class="card-body p-0">
                                        <div class="container px-5">
                                            <form action="{{ route('accounts.search') }}" method="get">
                                                <div class="row mb-3 pt-3 pb-3 ">
                                                    <div class="col-md-3 text-dark text-center ">
                                                        جستجو بر اساس
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select name="search_type" class="form-control">
                                                            <option value="name">نام</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" name="term"
                                                               style="direction:ltr;text-align:left;"
                                                               placeholder="مقدار را وارد کنید">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button type="submit" class="btn btn-outline-primary">جستجو</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table earning-box">
                                                <thead class="dark-grey">
                                                <tr class="text-white">
                                                    <th>نام حساب</th>
                                                    <th>
                                                        ترافیک مصرف شده
                                                        /
                                                        ترافیک کل
                                                    </th>
                                                    <th>پورت</th>
                                                    <th>تاریخ انقضا</th>
                                                    <th>سرور</th>
                                                    <th>وضعیت</th>
                                                    <th>ایمیل</th>
                                                    <th>لینک سرور</th>
                                                    <th>کیوار کد</th>
                                                    <th>عملیات</th>
                                                    @if(auth()->user()->hasRole('admin'))
                                                        <th>عملیات</th>
                                                    @endif
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @forelse ($accounts as $account )
                                                    <tr id="account_{{ $account->id }}" class="text-white">
                                                        <td>
                                                            {{ $account->account_name }}
                                                        </td>
                                                        <td id="account_traffic" style="direction:ltr;text-align:left;">
                                                            <span class="badge badge-warning animated-background">درحال بارگیری</span>
                                                        </td>
                                                        <td id="account_port">
                                                            <span class="badge badge-warning animated-background">درحال بارگیری</span>
                                                        </td>
                                                        <td id="account_expires_at">
                                                            <span class="badge badge-warning animated-background">درحال بارگیری</span>
                                                        </td>
                                                        <td>
                                                            {{ @$account->servers->server_name }}
                                                        </td>
                                                        <td id="account_status">
                                                            <span class="badge badge-warning animated-background">درحال بارگیری</span>
                                                        </td>
                                                        <td id="account_email">
                                                            @if(isset($account->json_data->multi_user))
                                                                @if($account->json_data->multi_user)
                                                                    @if($account->json_data->multi_user->email)
                                                                        {{ $account->json_data->multi_user->email }}
                                                                    @else
                                                                        ندارد
                                                                    @endif
                                                                @endif
                                                            @else
                                                                ندارد
                                                            @endif
                                                        </td>
                                                        <td id="account_address">
                                                            <span class="badge badge-warning animated-background">درحال بارگیری</span>
                                                        </td>
                                                        <td id="qr_code">
                                                            <span class="badge badge-warning animated-background">درحال بارگیری</span>
                                                        </td>
                                                        <td id="more_functions">
                                                            @if(!checkIfAccountIsPritunl($account))
                                                                <a href="{{route('account.change-protocol',$account->id)}}">
                                                                    <button
                                                                        class="btn btn-sm btn-success lighten-1"
                                                                        title="تغییر پروتکل">
                                                                        <i class="icon-track_changes text-white s-18"></i>
                                                                    </button>
                                                                </a>
                                                                @if(!isset($account->json_data->multi_user))
                                                                    <a href="{{route('account.order-package',$account->id)}}">
                                                                        <button
                                                                            class="btn btn-sm btn-success lighten-1"
                                                                            title="تمدید دوره">
                                                                            <i class="icon-recycle text-white s-18"></i>
                                                                        </button>
                                                                    </a>
                                                                @else
                                                                    @if(!$account->json_data->multi_user)
                                                                        <a href="{{route('account.order-package',$account->id)}}">
                                                                            <button
                                                                                class="btn btn-sm btn-success lighten-1"
                                                                                title="تمدید دوره">
                                                                                <i class="icon-recycle text-white s-18"></i>
                                                                            </button>
                                                                        </a>
                                                                    @endif
                                                                @endif
                                                                <a href="{{route('account.switch-power',$account->id)}}"
                                                                   id="account_power_switch" style="display: none;">
                                                                    <button
                                                                        id="account_power_button"
                                                                        class="btn btn-sm btn-danger lighten-1"
                                                                        title="غیرفعال سازی">
                                                                        <i class="icon-power-off text-white s-18"></i>
                                                                    </button>
                                                                </a>
                                                                {{--                                                                @if(!@$account->json_data->multi_user)--}}
                                                                {{--                                                                    <a href="{{ route('accounts.change.protocol',[$account->id,1]) }}">--}}
                                                                {{--                                                                        <button class="btn btn-sm btn-primary"--}}
                                                                {{--                                                                                title="تعویض پروتکل به VMESS+GRPC">--}}
                                                                {{--                                                                            <i class="icon-redo text-white s-18"></i>--}}
                                                                {{--                                                                        </button>--}}
                                                                {{--                                                                    </a>--}}
                                                                {{--                                                                @endif--}}
                                                                @if(!@$account->json_data->multi_user)
                                                                    <a href="{{ route('accounts.change.protocol',[$account->id,0]) }}">
                                                                        <button class="btn btn-sm btn-warning"
                                                                                title="تغییر پروتکل به VMESS+TCP">
                                                                            <i class="icon-redo text-white s-18"></i>
                                                                        </button>
                                                                    </a>
                                                                @endif
                                                            @endif
                                                            {{-- <a href="{{ route('accounts.recharge',$account->id) }}">
                                                                <button class="btn btn-sm btn-success lighten-1" title="خرید حجم اضافه/تمدید دوره">
                                                                    <i class="icon-network_check text-white s-18"></i>
                                                                </button>
                                                            </a> --}}
                                                        </td>
                                                        @if(auth()->user()->hasRole('admin'))
                                                            <td>
                                                                @if(!checkIfAccountIsPritunl($account))
                                                                    <a href="{{ route('admin.accounts.edit',$account->id) }}">
                                                                        <button class="btn btn-primary btn-sm"><i
                                                                                class="icon-edit"></i></button>
                                                                    </a>
                                                                @endif
                                                                <a onclick="return confirm('آیا از حذف این اکانت اطمینان کامل دارید؟ مقادیر حذف شده غیرقابل بازگشت است');"
                                                                   href="{{ route('admin.accounts.delete',$account->id) }}">
                                                                    <button class="btn btn-danger btn-sm"><i
                                                                            class="icon-trash"></i></button>
                                                                </a>
                                                            </td>
                                                        @endif
                                                    </tr>
                                                @empty

                                                @endforelse
                                                </tbody>
                                            </table>
                                            <div class="row">
                                                <div class="col-12 mx-auto mb-5">
                                                    {{ $dbAccounts->appends(request()->input())->links() }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Modal -->
                <div class="modal fade" id="qrCodeModal" tabindex="-1" role="dialog"
                     aria-labelledby="qrCodeModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">اسکن کنید</h5>
                            </div>
                            <div class="modal-body">
                                <canvas id="qrCodeModalqr_code" style="width:100%;height:100%;"></canvas>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Today Tab End-->
                <!--Yesterday Tab Start-->
            </div>
        </div>
    </div>
@endsection
@php
    $accountList = [];
    foreach ($accounts as $account){
    $accountList[] = $account->id;
    }
@endphp
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.10/dist/clipboard.min.js"></script>
    <script src="https://cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>
    <script src="{{ url('assets/custom/qrious.min.js') }}"></script>
    <script>
        new ClipboardJS('#copyClipboard');
    </script>
    <script src="{{ url('assets/js/get-account-details.js?v=1.0.1') }}"></script>
    <script>
        $('#account_power_switch').hide();
        let account = new getAccountDetails("{{ route('api.panel.get-account-details') }}");
        let accountList = JSON.parse('{{ json_encode($accountList) }}');
        $(document).ready(function () {
            $.each(accountList, function (index, value) {
                account.accountDetails(value, 'account_' + value);
            });

            // var qrcode = new QRCode(document.getElementById("qrCodeModalqr_code"), {
            //         text: '1',
            //         width: 128,
            //         height: 128,
            //         colorDark : "#000000",
            //         colorLight : "#ffffff",
            //         correctLevel : QRCode.CorrectLevel.H
            //     });
            // var qr = new QRious({
            //   element: document.getElementById('qrCodeModalqr_code'),
            // });
            // qr.padding = 0;
            // qr.size = 300;
            $('#qrCodeModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var accountSelector = button.data('account-selector') // Extract info from data-* attributes
                var isIran = button.data('is-iran') // Extract info from data-* attributes

                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                if (isIran == '1') {
                    modal.find('.modal-title').text('اسکن کنید - ایپی ایران')
                    qrvalue = $('#account_link_' + accountSelector).val();
                    this.qrcode = new QRious({
                        element: document.querySelector('#qrCodeModalqr_code'),
                        size: 260,
                        value: qrvalue,
                    });
                    // qrcode.makeCode($('#account_link_'+accountSelector).val());
                } else {
                    modal.find('.modal-title').text('اسکن کنید - ایپی خارج')
                    qrvalue = $('#account_link_tunnel_' + accountSelector).val();
                    this.qrcode = new QRious({
                        element: document.querySelector('#qrCodeModalqr_code'),
                        size: 260,
                        value: qrvalue,
                    });
                    // qrcode.makeCode($('#account_link_tunnel_'+accountSelector).val());
                }
                // modal.find('.modal-body div.qr_code').val()
            })

        });


    </script>

@endpush
