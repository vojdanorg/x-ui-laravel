@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                {{ $pageTitle }}
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce">
        <div class="pb-3">
            <div class="row row-eq-height">
                <!-- Daily Sale Report-->
                <div class="col-12">
                    <div class="card">
                        <form action="{{ route('admin.accounts.edit',$account->id) }}" method="post">
                            @csrf
                            <div class="card-body">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col-6 mx-auto">
                                        <div class="form-group">
                                            <label>
                                                مالک سرویس
                                            </label>
                                            <input type="hidden" name="transaction_id" value="{{ $account->transactions_id }}">
                                            <select class="form-control" name="user_id">
                                                @foreach (getAllUsers() as $user )
                                                    <option @if($user->id == $account->user_id) selected @endif value="{{ $user->id }}">{{ $user->name }}</option>
                                                @endforeach    
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                سرور
                                            </label>
                                            <select class="form-control" name="server_id">
                                                @foreach ($servers as $server )
                                                    <option @if($server->id == $account->server_id) selected @endif value="{{ $server->id }}">{{ $server->server_name }}</option>
                                                @endforeach    
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                نام سرویس
                                            </label>
                                            <input type="text" class="form-control" name="account_name" dir="ltr" value="{{ $account->account_name }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-success">ذخیره</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection