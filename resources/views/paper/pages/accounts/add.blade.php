@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full bg-dark">
        <header class="dark-blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-add ml-2"></i>
                    افزودن حساب جدید
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce bg-dark">
            @include('paper.components.admin-alerts')
            <div class="pb-3 bg-dark">
                <div class="row row-eq-height bg-dark">
                    <!-- Daily Sale Report-->
                    <div class="col-12 bg-dark">
                        <div class="card bg-dark">
                            <form action="{{ (isset($isTest) ? route('accounts.test') : route('accounts.add') )}}"
                                  method="post" onkeydown="return event.key != 'Enter';" id="create_account">
                                @csrf
                                <div class="card-body bg-dark">
                                    @if ($errors->any())
                                        <div class="alert alert-danger bg-dark">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div id="canBuildOnThisPackageAndServer" class="alert alert-danger">
                                        شما نمیتوانید با استفاده از پکیج انتخاب شده، روی سروری که انتخاب کرده اید سرویس
                                        ایجاد کنید.
                                    </div>
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>
                                                    نوع سرویس
                                                </label>
                                                <select name="package_id" class="form-control" id="packageList">
                                                    <option value="-1">
                                                        انتخاب کنید ...
                                                    </option>
                                                    @forelse ($packages as $package )
                                                        @php
                                                            $isPackageMultiUser = 0;
                                                                if(isset($package->protocol['multi_user'])){
                                                                    if($package->protocol['multi_user']){
                                                                        $isPackageMultiUser = 1;
                                                                    }
                                                                }
                                                        @endphp
                                                        <option value="{{ $package->id }}"
                                                                data-is_multi_user="{{$isPackageMultiUser}}"
                                                                data-is_pritunl="{{(checkIfPackageIsPritunl($package) ? "true" : "false")}}">
                                                            {{ $package->name }}
                                                            -
                                                            @if(!checkIfPackageIsPritunl($package))
                                                                {{ $package->bandwidth }}
                                                                گیگابایت
                                                                -
                                                            @endif
                                                            {{ number_format($package->price) }}
                                                            تومان
                                                        </option>
                                                    @empty

                                                    @endforelse
                                                </select>
                                            </div>
                                            {{--                                        <div class="form-group">--}}
                                            {{--                                            <label>--}}
                                            {{--                                                پروتکل--}}
                                            {{--                                            </label>--}}
                                            {{--                                            <select name="protocol" class="form-control">                                       --}}
                                            {{--                                                    <option value="vmess">vmess</option>--}}
                                            {{--                                                    <option value="vless">vless</option>--}}
                                            {{--                                            </select>--}}
                                            {{--                                        </div>--}}
                                            <div class="form-group">
                                                <label>
                                                    سرور
                                                    <span class="badge badge-warning" id="loadingBadge">
                                                    درحال بارگیری
                                                </span>
                                                </label>
                                                <select name="server_id" id="serverList" class="form-control">
                                                    <option value="null" data-allowed_for_multi_user="false">انتخاب
                                                        کنید
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    حجم سرویس
                                                </label>
                                                <input type="number" class="form-control" name="account_gig" id="accountGig" dir="ltr"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    نام حساب
                                                </label>
                                                <input type="text" class="form-control" name="account_name" dir="ltr"/>
                                            </div>

                                            <div id="createByCDN" class="hide">
                                                <div class="form-group">
                                                    <ul class="list-group">
                                                        <li class="list-group-item">
                                                            <div class="material-switch float-left">
                                                                <input id="make_as_cdn" role="switch" name="make_as_cdn"
                                                                       disabled
                                                                       type="checkbox">
                                                                <label for="make_as_cdn" class="bg-primary"></label>
                                                            </div>
                                                            ساخت روی CDN
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-success" id="submitButton">افزودن</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@php
    $serverList = [];
    foreach (getAllServers(true) as $server){
        $serverList[] = $server->id;
    }
    $serversCount = count($serverList);
@endphp
@push('scripts')
    <script src="{{ url('assets/js/add-client-servers-list.js') }}"></script>
    <script>


        $('#createByCDN').hide();
        let server = new addClientServersStatus("{{ route('api.panel.add-client.server-stauts') }}");
        let serverList = JSON.parse('{{ json_encode($serverList) }}');
        var serverListCount = <?= $serversCount ?>;
        $(document).ready(function () {
            $.each(serverList, function (index, value) {
                server.getServerDetails(value, 'server_' + value);
            });
        });
        $('#canBuildOnThisPackageAndServer').hide();
        $('#serverList').change(function () {
            var selectedItem = $(this).val();
            var hasCDN = $('option:selected', this).data("has_cdn");
            if ($('#serverList').find(':selected').data("is_pritunl") == true) {
                if ($('#packageList').find(':selected').data('is_pritunl') == false) {
                    $('#submitButton').prop('disabled', true);
                    $('#canBuildOnThisPackageAndServer').show();
                } else {
                    $('#submitButton').prop('disabled', false);
                    $('#canBuildOnThisPackageAndServer').hide();
                }
            } else {
                if (parseInt($('#packageList').find(':selected').data("is_multi_user")) === 1) {
                    var packagesAllowedServers = [];
                    if ($('#serverList').find(':selected').data('allowed_for_multi_user') !== false) {
                        packagesAllowedServers = $('#serverList').find(':selected').data('allowed_for_multi_user').split(',');
                    }
                    if (!packagesAllowedServers.includes($('#packageList').find(':selected').val())) {
                        $('#submitButton').prop('disabled', true);
                        $('#canBuildOnThisPackageAndServer').show();
                    } else {
                        $('#submitButton').prop('disabled', false);
                        $('#canBuildOnThisPackageAndServer').hide();
                    }
                } else {
                    if ($('#serverList').find(':selected').data('allowed_for_multi_user') === false && parseInt($('#packageList').find(':selected').data("is_multi_user")) === 0) {
                        $('#submitButton').prop('disabled', false);
                        $('#canBuildOnThisPackageAndServer').hide();
                    } else {
                        $('#submitButton').prop('disabled', true);
                        $('#canBuildOnThisPackageAndServer').show();
                    }
                }
            }
            if (hasCDN) {
                $('#createByCDN').show();
            } else {
                $('#createByCDN').hide();
            }
        });
        $('#packageList').change(function () {
            var selectedItem = $(this).val();
            var hasMultiUser = $('option:selected', this).data("is_multi_user");
            if ($('#serverList').find(':selected').data('is_pritunl') == true) {
                if ($('option:selected', this).data("is_pritunl") == false) {
                    $('#submitButton').prop('disabled', true);
                    $('#canBuildOnThisPackageAndServer').show();
                } else {
                    $('#submitButton').prop('disabled', false);
                    $('#canBuildOnThisPackageAndServer').hide();
                }
            } else {
                if (hasMultiUser === 1) {
                    var serverAllowedPackages = [];
                    if ($('#serverList').find(':selected').data('allowed_for_multi_user') !== false) {
                        serverAllowedPackages = $('#serverList').find(':selected').data('allowed_for_multi_user').split(',');
                    }
                    if (!serverAllowedPackages.includes(selectedItem) && $('#serverList').find(':selected').data('allowed_for_multi_user') != false) {
                        $('#submitButton').prop('disabled', true);
                        $('#canBuildOnThisPackageAndServer').show();
                    } else {
                        $('#submitButton').prop('disabled', false);
                        $('#canBuildOnThisPackageAndServer').hide();
                    }
                } else {
                    if ($('#serverList').find(':selected').data('allowed_for_multi_user') === false && parseInt($('#packageList').find(':selected').data("is_multi_user")) === 0) {
                        $('#submitButton').prop('disabled', false);
                        $('#canBuildOnThisPackageAndServer').hide();
                    } else {
                        $('#submitButton').prop('disabled', true);
                        $('#canBuildOnThisPackageAndServer').show();
                    }
                }
            }
        });
        $('#create_account').submit(function () {
            $('#submitButton').attr("disabled", true);
            $('#submitButton').addClass("disabled");
        });

    </script>
@endpush
