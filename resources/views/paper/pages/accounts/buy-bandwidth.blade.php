@extends('paper.layouts.master')
@section('content')
<div class="page has-sidebar-right height-full">
    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <h2 class="pr-3">
                <i class="icon-box ml-2"></i>
                {{ $pageTitle }}
            </h2>
        </div>
    </header>
    <div class="container-fluid relative animatedParent animateOnce">
        <div class="pb-3">
            <div class="row row-eq-height">
                <!-- Daily Sale Report-->
                <div class="col-12">
                    <div class="card">
                        <form action="" method="post">
                            @csrf
                            <div class="card-body">

                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
        
                                <div class="row">
                                    <div class="col-6 mx-auto">
                                        <div class="form-group">
                                            <label>
                                                نام حساب 
                                            </label>
                                            <input type="text" class="form-control" disabled value="{{ $account->account_name }}" dir="ltr" />
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                  پهنای باند اضافه
                                            </label>
                                            <input type="number" min="0" class="form-control" value="0" dir="ltr" />
                                        </div>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <div class="material-switch float-left">
                                                    <input id="renew_cycle" role="switch" name="renew_cycle" checked
                                                        type="checkbox">
                                                    <label for="renew_cycle" class="bg-primary"></label>
                                                </div>
                                                تمدید دوره ماهیانه
                                                <small class="text-muted text-red accent-1 d-block">
                                                    - دوره بر اساس پکیج کاربر تمدید میگردد و هزینه کامل دوره + هزینه حجم دوره  از حساب شما کسر میگردد
                                                    <br>
                                                    - زمان باقی مانده کاربر از دوره میسوزد و دوره جدید جایگزین میشود
                                                </small>
                                            </li>
                                        </ul>
                                        <hr>
                                        <div class="card no-b my-3 shadow">
                                            <div class="card-body">
                                                <small class="float-right">فاکتور خرید</small>
                                                <div class="my-4">
                                                    <span class="badge badge-primary badge-pill">قابل پرداخت</span>
                                                </div>
                                                 <div class="my-3">
                                                     <small>حجم اضافه</small>
                                                     <h6>
                                                        ۱
                                                        گیگابایت
                                                     </h6>
                                                     <h6>
                                                        ۱۵۰۰
                                                        تومان
                                                     </h6>
                                                 </div>
                                                <div class="my-3">
                                                    <small>تمدید دوره</small>
                                                    <h6>
                                                        200 
                                                        گیگ
                                                        ۳۰
                                                        روزه
                                                        ۵ 
                                                        کاربره
                                                    </h6>
                                                    <h6>
                                                        ۱۰۰،۰۰۰
                                                        تومان
                                                    </h6>
                                                </div>
                                                <div class="my-3">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="text-primary s-24 my-3">
                                                                101,500
                                                                تومان
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-success" id="submitButton" >تمدید</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')

@endpush