@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{ $pageTitle }}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="pb-3">
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12 mt-3">
                        <div class="card">
                            <form action="{{ route('account.order-package',$account->id) }}" method="post">
                                @csrf
                                <div class="card-body">
                                    <div class="alert alert-danger">
                                        با تمدید سرویس کاربری باقی مانده روز و حجم سرویس سوخته خواهد شد و پکیج جدید
                                        جایگزین میشود
                                    </div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>
                                                    پکیج مورد نظر برای تمدید
                                                </label>
                                                <select class="form-control" name="package_id">
                                                    @foreach ($packages as $package )
                                                        @if(!isset($package->protocol['multi_user']))
                                                            <option @if($account->package_id == $package->id) selected
                                                                    @endif value="{{$package->id}}">
                                                                {{ $package->name }}
                                                                -
                                                                {{ $package->bandwidth }}
                                                                گیگابایت
                                                                -
                                                                {{ number_format($package->price) }}
                                                                تومان
                                                            </option>
                                                        @else
                                                            @if(!$package->protocol['multi_user'])
                                                                <option @if($account->package_id == $package->id) selected
                                                                        @endif value="{{$package->id}}">
                                                                    {{ $package->name }}
                                                                    -
                                                                    {{ $package->bandwidth }}
                                                                    گیگابایت
                                                                    -
                                                                    {{ number_format($package->price) }}
                                                                    تومان
                                                                </option>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
{{--                                            <div class="form-group">--}}
{{--                                                <ul class="list-group">--}}
{{--                                                    <li class="list-group-item">--}}
{{--                                                        <div class="material-switch float-left">--}}
{{--                                                            <input id="regenerate_uuid" role="switch"--}}
{{--                                                                   name="regenerate_uuid"--}}
{{--                                                                   type="checkbox">--}}
{{--                                                            <label for="regenerate_uuid" class="bg-primary"></label>--}}
{{--                                                        </div>--}}
{{--                                                        حذف لینک قبلی و ایجاد لینک جدید--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}

                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button onclick="return confirm('آیا از انتخاب خود مطمئن هستید؟');"
                                            class="btn btn-success">تمدید
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
