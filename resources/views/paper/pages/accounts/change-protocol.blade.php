@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full">
        <header class="blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    {{ $pageTitle }}
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="pb-3">
                <div class="row row-eq-height">
                    <!-- Daily Sale Report-->
                    <div class="col-12">
                        <div class="card">
                            <div class="alert alert-warning">
                                بعد از تغییر پروتکل، لینک قبلی منقضی شده و میبایست از لینک جدید استفاده شود.
                            </div>
                            <form
                                action="{{ route('accounts.change.protocol',[$account->id,($isMultiUser ?  1 :0 )]) }}"
                                method="post">
                                @csrf
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6 mx-auto">
                                            <div class="form-group">
                                                <label>
                                                    نام حساب
                                                </label>
                                                <input type="text" value="{{ $account->account_name }}"
                                                       class="form-control" disabled dir="ltr"/>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    سرور مقصد
                                                    <small>
                                                        لطفا از سروری که ادمین به شما معرفی کرده استفاده کنید.
                                                    </small>
                                                    <span class="badge badge-warning" id="loadingBadge">
                                                    درحال بارگیری
                                                </span>
                                                </label>
                                                <select name="server_id" id="serverList" class="form-control">
                                                    <option value="null" data-allowed_for_multi_user="false">انتخاب
                                                        کنید
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-success" id="submitButton">ویرایش</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@php
    $serverList = [];
    foreach (getAllServers(true) as $server){
        $serverList[] = $server->id;
    }
    $serversCount = count($serverList);
@endphp
@push('scripts')
    <script src="{{ url('assets/js/add-client-servers-list.js') }}"></script>
    <script>
        let server = new addClientServersStatus("{{ route('api.panel.add-client.server-stauts') }}");
        let serverList = JSON.parse('{{ json_encode($serverList) }}');
        var serverListCount = <?= $serversCount ?>;
        $(document).ready(function () {
            $.each(serverList, function (index, value) {
                server.getServerDetails(value, 'server_' + value);
            });

        });
    </script>
@endpush
