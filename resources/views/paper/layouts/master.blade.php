<!DOCTYPE html>
<html lang="zxx" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/basic/favicon.ico" type="image/x-icon">
    <title>{{ $pageTitle }}</title>
    <!-- CSS -->
    <link rel="stylesheet" href="/assets/css/app.css?v=2">
    <style>
        @font-face {
            font-family: 'MyCustomFont';
            src: url('{{ asset('assets/fonts/vazir_bold.woff2') }}') format('truetype');
            /* Add additional font formats if needed */
        }

        .font-sans {
            font-family: 'MyCustomFont', sans-serif;
        }
        .animated-background {
            width: 100px;
            height: 25px;
            animation: colorChange 3s infinite;
        }

        @keyframes colorChange {
            0% { background-color: #007eff; }
            25% { background-color: rgba(0, 255, 196, 0.76); }
            50% { background-color: #5e00ff; }
            75% { background-color: #ff0000; }
            100% { background-color: rgba(255, 242, 0, 0.55); }
        }
        .loader {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: #F5F8FA;
            z-index: 9998;
            text-align: center;
        }

        .plane-container {
            position: absolute;
            top: 50%;
            left: 50%;
        }

        div.notify {
            z-index: 99999999999;
            left: 33%;
            direction: rtl;
        }
    </style>
    @notifyCss
    @stack('styles')
</head>

<body class="font-sans">
<!-- Pre loader -->
<div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="app">

    <!--Sidebar Start-->
    <aside class="main-sidebar fixed offcanvas shadow dark-grey" data-toggle='offcanvas'>
        <section class="sidebar">
            <div class="w-80px mt-3 mb-3 mr-3">

            </div>
            <div class="relative dark-grey">
                <a data-toggle="collapse" href="#userSettingsCollapse" role="button" aria-expanded="false"
                   aria-controls="userSettingsCollapse"
                   class="btn-fab btn-fab-sm fab-left-bottom absolute fab-top btn-primary shadow1 ">
                    <i class="icon-cogs"></i>
                </a>
                <div class="user-panel p-3 light mb-2 dark-grey">
                    <div>
                        <div class="float-right image">
                            <img class="user_avatar" src="/assets/img/dummy/u1.png" alt="User Image">
                        </div>
                        <div class="float-right info mr-3">
                            <h6 class="font-weight-light mt-2 mb-1 text-white">{{ auth()->user()->name }}</h6>
                            <i class="icon-circle text-white blink"></i>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="collapse multi-collapse" id="userSettingsCollapse">
                        <div class="list-group mt-3 shadow">
                            <a href="{{ route('user.profile') }}" class="list-group-item list-group-item-action ">
                                <i class="ml-2 icon-umbrella text-blue"></i>پروفایل کاربری
                            </a>
                            <a href="{{ route('user.change-password') }}"
                               class="list-group-item list-group-item-action"><i
                                    class="ml-2 icon-security text-purple"></i>
                                تغییر رمز عبور
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="sidebar-menu">
                <li class="header"><strong>
                        منوی اصلی
                    </strong></li>

                <li><a href="{{ route('dashboard') }}">
                        <i class="icon-dashboard2 text-warning blue-text s-18"></i>
                        <span>داشبورد</span>
                    </a>
                </li>
                <li class="treeview"><a href="#"><i class="icon-account_balance_wallet light-green-text s-18"></i>
                        <i class="icon-angle-left s-18 pull-left"></i>

                        سرویس های کاربری
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('accounts.list') }}"><i class="icon-circle-o"></i>لیست سرویس ها</a>
                        </li>
                        <li><a href="{{ route('accounts.add') }}"><i class="icon-add"></i>خرید سرویس جدید</a>
                        </li>
                        {{-- <li><a href="#"><i class="icon-circle-o"></i>خرید حجم اضافه</a>
                        </li>
                        <li><a href="#"><i class="icon-circle-o"></i>دریافت اکانت تست</a>
                        </li> --}}
                    </ul>
                </li>
                <li class="treeview"><a href="#">
                        <i class="icon-money light-purple-text s-18"></i>
                        <i class="icon-angle-left s-18 pull-left"></i>
                        <span>مالی</span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('transactions.add') }}"><i class="icon-add"></i>افزایش موجودی</a>
                        </li>
                        <li><a href="{{ route('transactions.list') }}"><i class="icon-circle-o"></i>تراکنش ها</a>
                        </li>
                    </ul>
                </li>
                <li class="header light mt-3 dark-blue"><strong>امکانات پیشرفته</strong></li>
                <li class="treeview ">
                    <a href="#">
                        <i class="icon-package text-lime s-18"></i> <span>وب سرویس</span>
                        <i class="icon-angle-left s-18 pull-left"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="panel-page-chat.html"><i class="icon-circle-o"></i>مستندات</a>
                        </li>

                    </ul>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('webservice.settings') }}"><i class="icon-circle-o"></i>تنظیمات</a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="{{ route('servers.status') }}">
                        <i class="icon-server text-blue s-18"></i> <span>وضعیت سرور ها</span>
                    </a>
                </li>
                @if(auth()->user()->hasRole('agent'))
                <li class="header light mt-3"><strong>مدیریت مجموعه</strong></li>
                <li>
                    <a href="{{ route('agent.register') }}">
                        <i class="icon-user text-blue s-18"></i> <span>ایجاد کاربر جدید (رجیستر موقت)</span>
                    </a>

                </li>
                <li>
                    <a href="{{ route('agent.package') }}">
                        <i class="icon-package text-blue s-18"></i> <span>قیمت گزاری پکیج ها</span>
                    </a>

                </li>
{{--                <li>--}}
{{--                    <a href="{{ route('agent.change-protocol') }}">--}}
{{--                        <i class="icon-package text-blue s-18"></i> <span>تغییر پروتکل (جدید)</span>--}}
{{--                    </a>--}}
{{----}}
{{--                </li>--}}
                @endif
                @if(auth()->user()->hasRole('admin'))
                    <li class="header light mt-3 dark-blue"><strong>ادمین</strong></li>
                    <li class="treeview ">
                        <a href="#">
                            <i class="icon-server text-lime s-18"></i> <span>سرور ها</span>
                            <i class="icon-angle-left s-18 pull-left"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('servers.status') }}"><i class="icon-circle-o"></i>لیست سرور ها</a>
                            </li>

                        </ul>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('servers.add') }}"><i class="icon-add"></i>
                                    افزودن سرور
                                    V2ray
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('pritunl.servers.add') }}"><i class="icon-add"></i>
                                    افزودن سرور
                                    OpenVPN
                                </a>
                            </li>
                        </ul>
                        <ul class="treeview-menu">
                            <li class="treeview">
                                <a href="#">
                                    <i class="icon-network_check text-blue s-18"></i> <span>روبات مانیتورینگ</span>
                                    <i class="icon-angle-left s-18 pull-left"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li>
                                        <a href="{{ route('servers.bot.monitoring') }}"><i class="icon-circle-o"></i>تنظیمات</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('servers.bot.monitoring.more-ips') }}"><i
                                                class="icon-circle-o"></i>ایپی های اضافه</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="treeview ">
                        <a href="#">
                            <i class="icon-package text-red s-18"></i> <span>پکیج ها</span>
                            <i class="icon-angle-left s-18 pull-left"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('package.list') }}"><i class="icon-circle-o"></i>لیست پکیج ها</a>
                            </li>

                        </ul>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('package.add','v2ray') }}"><i class="icon-add"></i>افزودن پکیج
                                    V2Ray</a>
                            </li>
                            <li>
                                <a href="{{ route('package.add','pritunl') }}"><i class="icon-add"></i>افزودن پکیج
                                    OpenVPN</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('settings.general') }}">
                            <i class="icon-cogs text-blue s-18"></i> <span>تنظیمات اصلی</span>
                        </a>

                    </li>
                    <li>
                        <a href="{{ route('alerts.list') }}">
                            <i class="icon-bell text-yellow s-18"></i> <span>اطلاعیه ها</span>
                        </a>

                    </li>
                    <li>
                        <a href="{{ route('transactions.add-by-admin') }}">
                            <i class="icon-plus text-green s-18"></i> <span>افزودن تراکنش</span>
                        </a>

                    </li>
                    <li>
                        <a href="{{ route('user.list') }}">
                            <i class="icon-person text-blue s-18"></i> <span>لیست کاربران</span>
                        </a>

                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="icon-globalization text-black s-18"></i> <span>تنظیمات وب سرویس ها</span>
                            <i class="icon-angle-left s-18 pull-left"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('third-party.cloudflare') }}"><i class="icon-cloud text-orange"></i>کلودفلیر</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin.accounts.list') }}">
                            <i class="icon-package text-blue s-18"></i> <span>اکانت ها</span>
                        </a>

                    </li>
                    <li>
                        <a href="/admin/log-viewer" target="_blank">
                            <i class="icon-error text-blue s-18"></i> <span>لاگ ها</span>
                        </a>

                    </li>
                @endif
            </ul>
        </section>
    </aside>
    <!--Sidebar End-->
    <div class="has-sidebar-right">
        <div class="pos-f-t">
        </div>
        <div class="sticky">
            <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar dark-grey accent-3">
                <div class="relative">
                    <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
                        <i></i>
                    </a>
                </div>
                <!--Top Menu Start -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        @php
                            $unreadNotificationsCount = auth()->user()->unreadNotifications()->get()->count();
                            $lastTenNotifications = auth()->user()->notifications()->take(10)->get();
                        @endphp
                        <li class="dropdown custom-dropdown notifications-menu">
                            <a href="#" class=" nav-link" data-toggle="dropdown" aria-expanded="false">
                                <i class="icon-notifications "></i>
                                @if($unreadNotificationsCount > 0)
                                    <span
                                        class="badge badge-danger badge-mini rounded-circle">{{$unreadNotificationsCount}}</span>
                                @endif
                            </a>
                            <ul class="dropdown-menu dropdown-menu-left" style="width: 15vw;">
                                <li class="header text-center">
                                    @if($unreadNotificationsCount > 0)
                                        شما
                                        {{$unreadNotificationsCount}}
                                        اطلاعیه خوانده نشده دارید
                                    @endif
                                </li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    @forelse($lastTenNotifications as $item)
                                        <ul class="menu text-right" dir="rtl"
                                            @if($item->read_at) style="background:#f5f8fa !important;" @endif>
                                            <li class="text-right" dir="rtl">
                                                <a href="{{route('notifications.read-by-id',$item->id)}}">
                                                    <i class="icon icon-{{$item->data['icon']}} text-{{$item->data['type']}}"></i>
                                                    {{$item->data['data']}}
                                                    <br>
                                                    <small class="text-muted">
                                                        {{$item->created_at->diffForHumans()}}
                                                    </small>
                                                </a>

                                            </li>
                                        </ul>
                                    @empty
                                        <ul class="menu">
                                            <li class="text-center">
                                                پیام جدیدی ندارید
                                            </li>
                                        </ul>
                                    @endforelse
                                </li>
                                <li class="footer p-2 text-center">
                                    <div class="row">
                                        <div class="col-5">
                                            <a href="{{route('notifications.all')}}" class="btn btn-primary btn-sm d-block w-100" style="width: 100%">مشاهده همه</a>
                                        </div>
                                        <div class="col-6">
                                            <a href="{{route('notifications.read-all')}}" class="btn btn-outline-primary btn-sm d-block w-100"  style="width: 100%">همه خوانده شود</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- User Account-->
                        <li class="dropdown custom-dropdown user user-menu">
                            <a href="#" class="nav-link" data-toggle="dropdown">
                                <i class="icon-more_vert "></i>
                                <img src="/assets/img/dummy/u1.png" class="user-image" alt="User Image">

                            </a>
                            <div class="dropdown-menu p-4 dropdown-menu-left">
                                <div class="row box justify-content-between my-4">
                                    <div class="col"><a href="{{ route('user.profile') }}">
                                            <i class="icon-person indigo lighten-2 avatar  r-5"></i>
                                            <div class="pt-1">پروفایل</div>
                                        </a></div>
                                </div>
                                <div class="row box justify-content-between my-4">
                                    <div class="col"><a href="{{ route('user.change-password') }}">
                                            <i class="icon-fingerprint indigo lighten-2 avatar  r-5"></i>
                                            <div class="pt-1">تغییر رمز عبور</div>
                                        </a></div>
                                </div>
                                <hr>
                                <div class="row box justify-content-between my-4">
                                    <div class="col">
                                        <a href="{{ route('user.logout') }}">
                                            <i class="icon-power-off red lighten-0 avatar  r-5"></i>
                                            <div class="pt-1">خروج</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    @include('notify::components.notify')
    @yield('content')
    <div class="control-sidebar-bg shadow dark-grey fixed"></div>
</div>
<!--/#app -->
<script src="/assets/js/app.js"></script>
<script src="/assets/custom/clipboard.min.js"></script>
<script>
    new ClipboardJS('#copyClipboard');
    $(document).ready(function () {
        $('.navbar-expand').click(function () {
            $('#sidebar').hide();

        })
    })
</script>


<!--
--- Footer Part - Use Jquery anywhere at page.
--- http://writing.colin-gourlay.com/safely-using-ready-before-including-jquery/
-->
{{-- <script>
    (function($,d){$.each(readyQ,function(i,f){$(f)});$.each(bindReadyQ,function(i,f){$(d).bind("ready",f)})})(jQuery,document)
</script> --}}
@notifyJs
@stack('scripts')

</body>

</html>
