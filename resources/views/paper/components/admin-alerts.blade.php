<div class="mt-3 ">
    @forelse(getAllAdminAlerts() as $alert)
        <div class="alert bold dark-grey text-white">
            <h1>
                {{$alert->message}}
            </h1>
        </div>
    @empty
    @endforelse
</div>
