<!DOCTYPE html>
<html>
<head>
    <title>{{$pageTitle}}</title>
    <style>
        body {
            background-color: #f7f7f7;
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
        }

        h1 {
            color: #333333;
            text-align: center;
        }

        .container {
            max-width: 800px;
            margin: 0 auto;
            text-align: center;
        }

        .button {
            display: inline-block;
            padding: 10px 20px;
            font-size: 18px;
            font-weight: bold;
            text-decoration: none;
            background-color: #333333;
            color: #ffffff;
            border-radius: 5px;
            margin: 10px;
            transition: background-color 0.3s ease;
        }

        .button:hover {
            background-color: #555555;
        }

        @keyframes typing {
            from { width: 0; }
            to { width: 100%; }
        }

        .typing-animation {
            display: inline-block;
            overflow: hidden;
            white-space: nowrap;
            animation: typing 4s steps(30, end);
        }
    </style>
</head>
<body>
<div class="container">
    <h1 class="typing-animation">What do you want?</h1>
    <div>
        <a class="button" href="change-protocol">Change My Protocol To New (MCI) Operator</a>
        <a class="button" href="account-details">Show My Account Information</a>
    </div>
</div>
</body>
</html>
