<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <title>User Information</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <style>
        body {
            font-family: "Arial", sans-serif;
            background-color: #f7f7f7;
        }

        @font-face {
            font-family: 'MyCustomFont';
            src: url('{{ asset('assets/fonts/vazir_bold.woff2') }}') format('truetype');
            /* Add additional font formats if needed */
        }


        .font-sans {
            font-family: 'MyCustomFont', sans-serif;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 40px;
            background-color: #f5f5f5;
            box-shadow: 0 0 10px rgb(222, 222, 222);
            border-radius: 4px;
            text-align: center;
        }

        h1 {
            font-size: 28px;
            margin-bottom: 20px;
            color: #282828;
        }

        .info-container {
            text-align: right;
            margin-bottom: 30px;
        }

        .info-container label {
            font-weight: bold;
        }

        #qrcode_mci {
            /* Set the size of the QR code image */
            width: 200px;
            height: 200px;
            /* Add some box-shadow for a subtle effect */
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
        }
        #qrcode_mtn {
            /* Set the size of the QR code image */
            width: 200px;
            height: 200px;
            /* Add some box-shadow for a subtle effect */
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
        }
    </style>
</head>

<body class="font-sans">
<div class="container">
    <div class="alert alert-warning" role="alert">
        هشدار: اطلاعات این صفحه تا 3 ساعت فعال میباشد
    </div>

    <div class="info-container">
        <label>نام حساب</label>
        <p id="firstName">{{$account->remark}}</p>
    </div>

    <div class="info-container">
        <label>حجم دانلود شده</label>
        <p id="lastName">{{round($account->download / (1024 * 1024 * 1024)) . ' گیگابایت'}}</p>
    </div>

    <div class="info-container">
        <label>حجم آپلود شده</label>
        <p id="email">{{round($account->upload / (1024 * 1024 * 1024)) . ' گیگابایت'}}</p>
    </div>

    <div class="info-container">
        <label>حجم کل سرویس</label>
        <p id="username">{{round($account->total / (1024 * 1024 * 1024)) . ' گیگابایت' }}</p>
    </div>

    <div class="info-container">
        <label>زمان ساخت این صفحه</label>
        <p id="accountType">{{$account->expire_link_time}}</p>
    </div>


    <div class="alert alert-info" role="alert">
        <label>لینک همراه اول</label>
        <br>
        <hr>
        <br>
        {{$ir_link}}

        <div class="modal-body" id="qrCodeModal">
            <convas id="qrcode_mci" style="width:100%;height:100%;"> </convas>
        </div>
    </div>

    <div class="alert alert-warning" role="alert">
        <label>لینک ایرانسل و رایتل</label>
        <br>
        <hr>
        <br>
        {{$account->connection}}

        <div class="modal-body" id="qrCodeModal">
            <convas id="qrcode_mtn" style="width:100%;height:100%;"> </convas>
        </div>

    </div>
    <button id="showQrCode" class="btn btn-primary center">نمایش کیو ار کد</button> <br><hr><br>
    <button id="copyClipboardPageLink" class="btn btn-warning center">کپی کردن و اشتراک گذاری آدرس این صفحه با دوستان</button>

</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script>
    
</script>
</body>

</html>
