@extends('paper.layouts.master')
@section('content')
    <div class="page has-sidebar-right height-full theme-dark">
        <header class="dark-blue accent-3 relative nav-sticky">
            <div class="container-fluid text-white">
                <h2 class="pr-3">
                    <i class="icon-box ml-2"></i>
                    داشبورد
                </h2>
            </div>
        </header>
        <div class="container-fluid relative animatedParent animateOnce">
            <div class="pb-3">
                @if($unSettledTransactions->count() > 0)
                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="alert alert-danger text-white bold">
                                <h1>
                                    شما
                                    {{ number_format($unSettledTransactions->sum('amount')) }}
                                    تومان تراکنش تسویه نشده دارید
                                </h1>
                            </div>
                        </div>
                    </div>
                @endif
                @include('paper.components.admin-alerts')
                <!--Today Tab Start-->
                <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">
                    <div class="row my-3">
                        <div class="col-md-3">
                            <div class="counter-box dark-grey r-5 p-3">
                                <div class="p-4 text-white">
                                    <div class="float-left">
                                        <span class="icon icon-account_balance  s-48"></span>
                                    </div>
                                    <div class="counter-title">حساب ها</div>
                                    <h5 class="sc-counter mt-3">{{ $accountsCount }}</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="counter-box  dark-grey r-5 p-3">
                                <div class="p-4 text-white">
                                    <div class="float-left">
                                        <span class="icon icon-google-wallet s-48"></span>
                                    </div>
                                    <div class="counter-title ">موجودی کیف پول</div>
                                    <h5 class="mt-3">
                                        {{ number_format(auth()->user()->wallet) }}
                                        تومان
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="counter-box  dark-grey r-5 p-3">
                                <div class="p-4 text-white">
                                    <div class="float-left">
                                        <span class="icon icon-transform s-48"></span>
                                    </div>
                                    <div class="counter-title">تراکنش ها</div>
                                    <h5 class="sc-counter mt-3">{{ $transactions->count() }}</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="counter-box  dark-grey r-5 p-3">
                                <div class="p-4 text-white">
                                    <div class="float-left">
                                        <span class="icon icon-dashboard2 s-48"></span>
                                    </div>
                                    <div class="counter-title ">
                                        <small style="font-size: 70%;">
                                            تراکنش های تسویه نشده
                                        </small>
                                    </div>
                                    <h5 class="mt-3">
                                        {{ $unSettledTransactions->count() }}
                                    </h5>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        @if(get_static_option('show_resellers_leaderboard_publicly') || auth()->user()->hasRole('admin'))
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="card-title">
                                            رتبه بندی روزانه فروشندگان
                                            @if(!get_static_option('show_resellers_leaderboard_publicly') && auth()->user()->hasRole('admin'))
                                                (درحال حاضر این قسمت فقط برای ادمین قابل مشاهده است)
                                            @endif
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="box-body">
                                            <table class="table table-hover-earning-box">
                                                <thead class="bg-dark text-white">
                                                <tr>
                                                    <th style="width: 10px">رتبه</th>
                                                    <th>نام</th>
                                                    <th>تعداد فروش</th>
                                                </tr>
                                                </thead>
                                                <tbody class="text-light">
                                                @forelse($todayLeaderboard as $item)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}.</td>
                                                        <td>{{$item->name}}</td>
                                                        <td>
                                                            {{$item->total_transactions}}
                                                        </td>
                                                        </td>
                                                    </tr>
                                                @empty
                                                @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">تعداد تراکنش در روز</div>

                                <div class="card-body">

                                    {{-- <h1>{{ $chart1->options['chart_title'] }}</h1> --}}
                                    {!! $incomeChart->renderHtml() !!}

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row row-eq-height">
                        <!-- Daily Sale Report-->
                        <div class="col-12">
                            <div class="card my-3 no-b ">
                                <div class="card-header dark-blue b-0 p-3 d-flex justify-content-between ">
                                    <div>
                                        <h4 class="card-title  text-white">
                                            حساب ها

                                        </h4>
                                        <small class="card-subtitle mb-2 text-white">
                                            مروری بر آخرین حساب های خریداری شده
                                        </small>
                                    </div>
                                    <a data-toggle="collapse" href="#salesCard" aria-expanded="false"
                                       aria-controls="salesCard">
                                        <i class="icon-menu"></i>
                                    </a>
                                </div>
                                <div class="collapse show" id="salesCard">
                                    <div class="card-body p-0">
                                        <div class="table-responsive">
                                            <table class="table table-hover earning-box">
                                                <thead class="bg-dark text-white">
                                                <tr>
                                                    <th>نام حساب</th>
                                                    <th>
                                                        ترافیک مصرف شده
                                                        /
                                                        ترافیک کل
                                                    </th>
                                                    <th>پورت</th>
                                                    <th>تاریخ انقضا</th>
                                                    <th>سرور</th>
                                                    <th>وضعیت</th>
                                                    <th>آدرس سرور</th>
                                                    @if(auth()->user()->hasRole('admin'))
                                                        <th>عملیات</th>
                                                    @endif
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @forelse ($accounts as $account )
                                                    <tr id="account_{{ $account->id }}" class="text-white">
                                                        <td>
                                                            {{ $account->account_name }}
                                                        </td>
                                                        <td id="account_traffic" style="direction:ltr;text-align:left;">
                                                            <span class="badge badge-warning">درحال بارگیری</span>
                                                        </td>
                                                        <td id="account_port">
                                                            <span class="badge badge-warning">درحال بارگیری</span>
                                                        </td>
                                                        <td id="account_expires_at">
                                                            <span class="badge badge-warning">درحال بارگیری</span>
                                                        </td>
                                                        <td>
                                                            {{ @$account->servers->server_name }}
                                                        </td>
                                                        <td id="account_status">
                                                            <span class="badge badge-warning">درحال بارگیری</span>
                                                        </td>
                                                        <td id="account_address">
                                                            <span class="badge badge-warning">درحال بارگیری</span>
                                                        </td>
                                                        @if(auth()->user()->hasRole('admin'))
                                                            <td>
                                                                <a
                                                                    href="{{ route('admin.accounts.edit',$account->id) }}">
                                                                    <button class="btn btn-primary btn-sm"><i
                                                                            class="icon-edit"></i></button>
                                                                </a>

                                                                <a onclick="return confirm('آیا از حذف این اکانت اطمینان کامل دارید؟ مقادیر حذف شده غیرقابل بازگشت است');"
                                                                   href="{{ route('admin.accounts.delete',$account->id) }}">
                                                                    <button class="btn btn-danger btn-sm"><i
                                                                            class="icon-trash"></i></button>
                                                                </a>
                                                            </td>
                                                        @endif
                                                    </tr>
                                                @empty

                                                @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Today Tab End-->
                <!--Yesterday Tab Start-->
            </div>
        </div>
    </div>
@endsection
@php
    $accountList = [];
    foreach ($accounts as $account){
        $accountList[] = $account->id;
    }
@endphp
@push('scripts')
        {!! $incomeChart->renderChartJsLibrary() !!}
        {!! $incomeChart->renderJs() !!}
        <script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.10/dist/clipboard.min.js"></script>
        <script>
            new ClipboardJS('#copyClipboard');
        </script>
        <script src="{{ url('assets/js/get-account-details.js') }}"></script>
        <script>
            let account = new getAccountDetails("{{ route('api.panel.get-account-details') }}");
            let accountList = JSON.parse('{{ json_encode($accountList) }}');
            $(document).ready(function () {
                $.each(accountList, function (index, value) {
                    account.accountDetails(value, 'account_' + value);
                });
            });


        </script>
@endpush
