@extends('paper.layouts.auth_master')
@section('content')
<div class="d-flex row">
    <div class="col-md-5 white">
        <div class="p-5">
            <h3>ایجاد حساب کاربری جدید</h3>
            <p>
                ثبت نام کن و کسب درآمد کن
            </p>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('register') }}" method="post">
                @csrf
                <div class="form-group has-icon"><i class="icon-envelope-o"></i>
                    <input type="text" name="name" class="form-control form-control-lg"
                        placeholder="نام و نام خانوادگی">
                </div>
                <div class="form-group has-icon"><i class="icon-envelope-o"></i>
                    <input type="text" name="email" class="form-control form-control-lg" placeholder="آدرس ایمیل">
                    <x-input-error :messages="$errors->get('email')" class="mt-2" />
                </div>
                <div class="form-group has-icon"><i class="icon-user-secret"></i>
                    <input type="password" name="password" class="form-control form-control-lg" placeholder="رمز عبور">
                    <x-input-error :messages="$errors->get('password')" class="mt-2" />
                </div>
                <div class="form-group has-icon"><i class="icon-user-secret"></i>
                    <input type="password" name="password_confirmation" class="form-control form-control-lg"
                        placeholder="تکرار رمز عبور">
                    <x-input-error :messages="$errors->get('password')" class="mt-2" />
                </div>
                <input type="submit" class="btn btn-primary btn-lg btn-block" value="ثبت نام">
            </form>
        </div>
    </div>
    <div class="col-md-7  height-full blue accent-3 align-self-center text-center" data-bg-repeat="false"
        data-bg-possition="center" style="background: url('assets/img/icon/icon-plane.png') #FFEFE4">
    </div>
</div>
@endsection
{{-- <x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div>
                <x-input-label for="name" :value="__('Name')" />

                <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required
                    autofocus />

                <x-input-error :messages="$errors->get('name')" class="mt-2" />
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-input-label for="email" :value="__('Email')" />

                <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')"
                    required />

                <x-input-error :messages="$errors->get('email')" class="mt-2" />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-input-label for="password" :value="__('Password')" />

                <x-text-input id="password" class="block mt-1 w-full" type="password" name="password" required
                    autocomplete="new-password" />

                <x-input-error :messages="$errors->get('password')" class="mt-2" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-input-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-text-input id="password_confirmation" class="block mt-1 w-full" type="password"
                    name="password_confirmation" required />

                <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-primary-button class="ml-4">
                    {{ __('Register') }}
                </x-primary-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout> --}}