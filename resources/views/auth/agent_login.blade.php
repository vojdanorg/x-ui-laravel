@extends('paper.layouts.auth_master')
@section('content')
<div class="d-flex row">
    <div class="col-md-5 white">
        <div class="p-5">
            <h3>مشتری عزیز بسیار خوش آمدید</h3>
            <p>
                دنیای ارتباطات با تیم ما سادست، کافیه که وارد حسابتون بشید!
            </p>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('loginAsMember') }}" method="post">
                @csrf
                <div class="form-group has-icon"><i class="icon-envelope-o"></i>
                    <input type="text" name="email" class="form-control form-control-lg"
                           placeholder="آدرس ایمیل">
                    <x-input-error :messages="$errors->get('email')" class="mt-2" />
                </div>
                <div class="form-group has-icon"><i class="icon-user-secret"></i>
                    <input type="password" name="password" class="form-control form-control-lg"
                           placeholder="رمز عبور">
                           <x-input-error :messages="$errors->get('password')" class="mt-2" />
                </div>
                <div class="form-group has-icon"><i class="icon-user-secret"></i>
                    <input type="password" name="agent_code" class="form-control form-control-lg"
                           placeholder="کد فروشنده (از فروشنده دریافت کرده اید)">
                           <x-input-error :messages="$errors->get('password')" class="mt-2" />
                </div>
                <div class="form-group has-icon"><i class="icon-user-secret"></i>
                    <input type="password" name="agent_accept_code" class="form-control form-control-lg"
                           placeholder="کد تاییدی فروشنده (از فروشنده دریافت کرده اید)">
                           <x-input-error :messages="$errors->get('password')" class="mt-2" />
                </div>
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('مرا به خاطر بسپار') }}</span>
                </label>
                <input type="submit" class="btn btn-primary btn-lg btn-block" value="ورود">
            </form>
        </div>
    </div>
    <div class="col-md-7  height-full blue accent-3 align-self-center text-center" data-bg-repeat="false"
         data-bg-possition="center"
         style="background: url('assets/img/icon/icon-plane.png') #ffffff">
    </div>
</div>
@endsection
