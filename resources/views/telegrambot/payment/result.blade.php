<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>نتیجه پرداخت</title>
    <style>
        .alert {
            text-align: center;
            width: 100%;
            padding: 7px;
            border-radius: 10px;
            color: black;
            font-size: 17px;
            font-weight: bold;
        }

        .alert-danger {
            background-color: red;
            color: white;
        }

        .alert-success {
            background-color: green;
            color: white;
        }
    </style>
</head>

<body>

    @if($result['success'] == false)
    <div class="alert alert-danger">
        {{ $result['message'] }}
    </div>
    @else
    <div class="alert alert-success">
        {{ $result['message'] }}
        <br>
        کد پیگیری پرداخت شما:
        {{ $result['payment_id'] }}
    </div>
    @endif

</body>

</html>