<!doctype html>
<html lang="zxx" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/basic/favicon.ico" type="image/x-icon">
    <title>{{ $pageTitle }}</title>
    <!-- CSS -->
    <link rel="stylesheet" href="/assets/css/app.css?v=2">
    @notifyCss
    @stack('styles')

    <style>
        body {
            background-color: rgba(232, 232, 232, 0.84);
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
        }

        h1 {
            color: #333333;
            text-align: center;
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
            text-align: center;
        }

        .button {
            display: inline-block;
            padding: 10px 20px;
            font-size: 18px;
            font-weight: bold;
            text-decoration: none;
            background-color: #333333;
            color: #262626;
            border-radius: 5px;
            margin: 10px;
            transition: background-color 0.3s ease;
        }

        .button:hover {
            background-color: #555555;
        }

        @keyframes typing {
            from { width: 0; }
            to { width: 100%; }
        }

        .typing-animation {
            display: inline-block;
            overflow: hidden;
            white-space: nowrap;
            animation: typing 4s steps(30, end);
        }
        .custom-input {
            height: 40px; /* Adjust the height value as per your requirement */
            padding: 5px;
            font-size: 16px;
        }
        .luxury-input {
            border: none;
            border-bottom: 2px solid #0015ff;
            border-radius: 0;
            background-color: transparent;
            padding: 5px;
            font-size: 16px;
            color: #b7b7b7;
            box-shadow: none;
            transition: border-color 0.3s ease;
        }

        .luxury-input:focus {
            border-color: #333333;
            box-shadow: none;
        }
    </style>

</head>


<body>

<div class="container">
    <h1 class="typing-animation">Upgrade Link</h1>

    <form class="mt-10" action="{{route('doChangeProtocol')}}" method="post" id="connections">
        @csrf
        <div class="mb-3 custom-input">
            <input class="form-control luxury-input typing-animation" id="link" name="link" placeholder="Your VLESS-REALITY Protocol Link" />
        </div>

        <button type="submit" class="btn btn-primary plane-container">Accept Upgrade</button>
    </form>
</div>
@include('notify::components.notify')
@yield('content')
<div class="control-sidebar-bg shadow white fixed"></div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="/assets/custom/clipboard.min.js"></script>
<script>
    new ClipboardJS('#copyClipboard');
    $(document).ready(function () {
        $('.navbar-expand').click(function () {
            $('#sidebar').hide();

        })
    })
</script>


@notifyJs
@stack('scripts')
</body>
</html>
