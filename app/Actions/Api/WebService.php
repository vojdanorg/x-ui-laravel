<?php

namespace App\Actions\Api;

use Exception;
use Illuminate\Support\Facades\Http;

class WebService
{

    public function __construct($hostname, $port, $cookie)
    {
        $this->host = 'http://' . $hostname . ':' . $port . '/';
        $this->cookie = $cookie;
    }

    public function POST($endPoint, $data = false)
    {
        try {
            if ($data) {
                $response = Http::withHeaders([
                    'Cookie' => $this->cookie
                ])->post($this->host . $endPoint, $data);
            } else {
                $response = Http::withHeaders([
                    'Cookie' => $this->cookie
                ])->post($this->host . $endPoint);
            }

            if ($response->body() != '404 page not found') {
                return $response->body();
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
    public function GET($endPoint)
    {
        try {
            $response = Http::withHeaders([
                'Cookie' => $this->cookie
            ])->get($this->host . $endPoint);
            if ($response->body() != '404 page not found') {
                return $response->body();
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
}
