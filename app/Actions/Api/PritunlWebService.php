<?php

namespace App\Actions\Api;

use Illuminate\Support\Facades\Http;

class PritunlWebService
{
    public $API_TOKEN, $API_SECRET, $BASE_URL;

    public function __construct($apiToken, $apiSecret, $domain)
    {
        $this->API_TOKEN = $apiToken;
        $this->API_SECRET = $apiSecret;
        $this->BASE_URL = 'https://' . $domain;
    }

    private function sendRequest($method, $endPoint, $data = false)
    {
        $timestamp = time();
        $random_bytes = random_bytes(16);
        $nonce = bin2hex($random_bytes);
        $authString = implode('&', array($this->API_TOKEN, $timestamp, $nonce, strtoupper($method), '/' . $endPoint));
        $auth_signature = base64_encode(hash_hmac('sha256', $authString, $this->API_SECRET, true));
        $method = strtoupper($method);
        $headers = [
            'Auth-Token' => $this->API_TOKEN,
            'Auth-Timestamp' => $timestamp,
            'Auth-Nonce' => $nonce,
            'Auth-Signature' => $auth_signature
        ];
        switch ($method) {
            case "GET":
                try {
                    $response = Http::withHeaders($headers)->get($this->BASE_URL . '/' . $endPoint);
                    if ($response->body() == '401: Unauthorized') {
                        return false;
                    } else {
                        return json_decode($response->body(), true);
                    }
                } catch (Exception $e) {
                    return false;
                }
                break;
            case "POST":
                try {
                    $response = Http::withHeaders($headers)->post($this->BASE_URL . '/' . $endPoint, $data);
                    if ($response->body() == '401: Unauthorized') {
                        return false;
                    } else {
                        return json_decode($response->body(), true);
                    }
                } catch (Exception $e) {
                    return false;
                }
                break;
            case "DELETE":
                try {
                    $response = Http::withHeaders($headers)->delete($this->BASE_URL . '/' . $endPoint, $data);
                    if ($response->body() == '401: Unauthorized') {
                        return false;
                    } else {
                        return json_decode($response->body(), true);
                    }
                } catch (Exception $e) {
                    return false;
                }
                break;
            case "PUT":
                try {
                    $response = Http::withHeaders($headers)->put($this->BASE_URL . '/' . $endPoint, $data);
                    if ($response->body() == '401: Unauthorized') {
                        return false;
                    } else {
                        return json_decode($response->body(), true);
                    }
                } catch (Exception $e) {
                    return false;
                }
                break;
            default:
                return false;
                break;
        }

    }

    public function getOrganizations()
    {
        $response = $this->sendRequest('GET', 'organization');
        if (!$response) {
            return false;
        }
        return $response;
    }

    public function getOrganizationUsersByID($OID)
    {
        $response = $this->sendRequest('GET', 'user/' . $OID);
        if (!$response) {
            return false;
        }
        return $response;
    }

    public function getServers()
    {
        $response = $this->sendRequest('GET', 'server');
        if (!$response) {
            return false;
        }
        return $response;
    }

    public function createAccountUsingBulk($data, $OID)
    {
        $response = $this->sendRequest('POST', 'user/' . $OID . '/multi', $data);
        if (!$response) {
            return false;
        }
        return $response;
    }

    public function getUserDetails($OID, $UID)
    {
        $response = $this->sendRequest('GET', 'user/' . $OID . '/' . $UID);
        if (!$response) {
            return false;
        }
        return $response;
    }

    public function getAccountLinks($OID, $UID)
    {
        $response = $this->sendRequest('GET', 'key/' . $OID . '/' . $UID);
        if (!$response) {
            return false;
        }
        return $response;
    }

    public function deleteAccount($OID, $UID)
    {
        $response = $this->sendRequest('DELETE', 'user/' . $OID . '/' . $UID);
        if (!$response) {
            return false;
        }
        return $response;
    }

    public function disableUser($OID, $UID, $data)
    {
        $data['disabled'] = true;
        $response = $this->sendRequest('PUT', 'user/' . $OID . '/' . $UID, $data);
        if (!$response) {
            return false;
        }
        return $response;
    }
}
