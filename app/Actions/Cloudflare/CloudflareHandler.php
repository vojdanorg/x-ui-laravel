<?php

namespace App\Actions\Cloudflare;

use Illuminate\Support\Facades\Http;

//
//Inside very $endPoint You must put <zone_id> for script to auto replace it
//
class CloudflareHandler
{
    public $email, $apiToken, $zoneIdentifire, $globalKey;

    public function __construct($email, $apiToken, $zoneIdentifire, $globalKey)
    {
        $this->email = $email;
        $this->apiToken = $apiToken;
        $this->zoneIdentifire = $zoneIdentifire;
        $this->globalKey = $globalKey;
    }

    private function sendRequest($type, $endPoint, $data = false)
    {
        $endPoint = str_replace('<zone_id>', $this->zoneIdentifire, $endPoint);
        $headers = [
            'Authorization' => 'Bearer ' . $this->apiToken,
            'Content-Type' => 'application/json',
            'X-Auth-Email' => $this->email,
            'X-Auth-Key' => $this->globalKey
        ];
        if ($type == 'post') {
            try {
                $response = Http::withHeaders($headers)->post('https://api.cloudflare.com/client/v4/' . $endPoint,$data);
                return $response->json();
            } catch (\Exception $e) {
                return false;
            }
        } elseif ($type == 'get') {
            try {
                $response = Http::withHeaders($headers)->get('https://api.cloudflare.com/client/v4/' . $endPoint);
                return $response->json();
            } catch (\Exception $e) {
                return false;
            }
        } elseif ($type == 'put') {
            try {
                $response = Http::withHeaders($headers)->put('https://api.cloudflare.com/client/v4/' . $endPoint,$data);
                return $response->json();
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    public function getListOfDnsZones()
    {
        $endPoint = 'zones/<zone_id>/dns_records';
        return $this->sendRequest('get', $endPoint);
    }

    public function getDNSRecordDetailsByName($name)
    {
        $zones = $this->getListOfDnsZones();
        if ($zones) {
            foreach ($zones['result'] as $key => $zone) {
                if ($zone['name'] == $name) {
                    return $zones['result'][$key];
                }
            }
            return 404;
        } else {
            return false;
        }

    }

    public function updateDnsZoneByID($id, $data)
    {
        /*
         * Data requires as array:
         * type : e.g A
         * name : e.g m.fr
         * content : 127.0.0.1
         * ttl : e.g 1 => auto
         */
        $endPoint = 'zones/<zone_id>/dns_records/' . $id;
        return $this->sendRequest('put', $endPoint, $data);
    }
    public function createDnsZone($data)
    {
        /*
         * Data requires as array:
         * type : e.g A
         * name : e.g m.fr
         * content : 127.0.0.1
         * ttl : e.g 1 => auto
         */
        $endPoint = 'zones/<zone_id>/dns_records';
        return $this->sendRequest('post', $endPoint, $data);
    }
}
