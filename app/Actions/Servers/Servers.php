<?php

namespace App\Actions\Servers;

use App\Actions\Api\WebService;
use App\Models\Servers as ModelsServers;
use Illuminate\Support\Facades\Http;

class Servers
{
    private $webService;

    private function getServerById($id)
    {
        $server = ModelsServers::where('id', $id)->first();
        if (is_null($server)) {
            return false;
        }
        if (get_static_option('tunnel_ip_for_server_connection') == 1 && !is_null($server->tunnel_ip) && !is_null($server->tunnel_cookie)) {
            $this->webService = new WebService($server->tunnel_ip, $server->panel_port, $server->tunnel_cookie);
        } else {
            $this->webService = new WebService($server->hostname, $server->panel_port, $server->panel_cookie);
        }
        return $server;
    }

    public function getServerStatus($id)
    {
        $server = $this->getServerById($id);
        $this->checkForNotFoundException($server);
        $status = $this->webService->POST('xui/inbound/list');
        if ($status == false) {
            return false;
        }
        return true;
    }

    public function getServerClients($id)
    {
        $server = $this->getServerById($id);
        if (!$server || is_null($server)) {
            return false;
        }
        $this->checkForNotFoundException($server);
        $status = $this->webService->POST('xui/inbound/list');
        if ($status == false) {
            return false;
        }
        return (object)json_decode($status, true);
    }

    public function createAccount($id, $data)
    {
        $server = $this->getServerById($id);
        $this->checkForNotFoundException($server);
        $status = $this->webService->POST('xui/inbound/add', $data);
        if ($status == false) {
            return false;
        }
        return (object)json_decode($status, true);
    }

    public function editAccount($id, $data, $accountId)
    {
        $server = $this->getServerById($id);
        $this->checkForNotFoundException($server);
        $status = $this->webService->POST('xui/inbound/update/' . $accountId, $data);
        if ($status == false) {
            return false;
        }
        return (object)json_decode($status, true);
    }

    public function deleteAccount($id, $accountId)
    {
        $server = $this->getServerById($id);
        $this->checkForNotFoundException($server);
        $status = $this->webService->POST('xui/inbound/del/' . $accountId);
        if ($status == false) {
            return false;
        }
        return (object)json_decode($status, true);
    }

    private function checkForNotFoundException($server)
    {
        if ($server == false) {
            return [
                'code' => 404,
                'message' => 'Server Not Found'
            ];
        } else {
            return true;
        }
    }
}
