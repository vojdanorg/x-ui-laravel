<?php

namespace App\Console\Commands;

use App\Actions\Servers\Servers;
use App\Models\Accounts;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class BlockUserLimitAbusers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'abusers:block';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Block users who have more than specified ips connected!';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Logger
//        Log::emergency($message);
//        Log::alert($message);
//        Log::critical($message);
//        Log::error($message);
//        Log::warning($message);
//        Log::notice($message);
//        Log::info($message);
//        Log::debug($message);
        //End of logger

        $accounts = Accounts::with('package', 'servers')->get();
        foreach ($accounts as $account) {
            if (is_null($account->package)) {
                Log::warning('Account ' . $account->account_name . ' with id ' . $account->id . ' dont have a package. please add package for it. Check failed. Going for next account');
                continue;
            }
            if (is_null($account->servers)) {
                Log::alert('Account has no servers ' . $account->name . ' id ' . $account->id) . '. Check failed. Going for next account';
                continue;
            }
            $accountDetail = getUserAccountDetail($account);
            if (!$accountDetail) {
                Log::alert('Cant connect to the server of ' . $account->name . ' id ' . $account->id . '. Server: ' . $account->servers->server_name . '. Check failed. Going for next account');
                continue;
            }
            if (is_null($account->ips)) {
                Log::info('No ips has logged for user ' . $account->account_name . ' with ID ' . $account->id . ' Moving for next account');
                continue;
            }
            $ips = json_decode($account->ips, true);
            if (count($ips) > $account->package->user_limit && $account->block_reason == 0) {
                Log::emergency('User with name: ' . $account->account_name . ' ID:' . $account->id . ' On Server:' . $account->servers->server_name . ' Port: ' . $accountDetail['port'] . ' is abusing maximum ' . $account->package->user_limit . ' ips with ' . count($ips) . ' ips connected. System is going to block this user.  ips are: ',$ips);
                $accountDetail['enable'] = false;
                $blockResponse = (new Servers)->editAccount($account->servers->id, $accountDetail, $accountDetail['id']);
                if (!$blockResponse) {
                    Log::alert('Cant connect to the server of ' . $account->name . ' id ' . $account->id . '. Server: ' . $account->servers->server_name . '. Blocking user' . $account->account_name . ' failed!. Going for next account');
                }
                Accounts::where('id', $account->id)->update([
                    'block_reason' => 3
                ]);
                Log::notice('User '.$account->account_name.' blocked. Reason: Too Much IPs Connected. Maximum allowed: '.$account->package->user_limit.'  ips are: ',$ips);
            }
        }
//        return Command::SUCCESS;
    }
}
