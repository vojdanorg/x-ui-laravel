<?php

namespace App\Console\Commands;

use App\Actions\Api\WebService;
use App\Models\Accounts;
use Illuminate\Console\Command;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use PDO;

class MoveCustomersUsingSqliteAndJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'move_customers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */

    private $destinationServer = [
        'hostname' => '159.69.194.199',
        'cookie' => 'session=MTY2ODM3MTkwNnxEdi1CQkFFQ180SUFBUkFCRUFBQVpmLUNBQUVHYzNSeWFXNW5EQXdBQ2t4UFIwbE9YMVZUUlZJWWVDMTFhUzlrWVhSaFltRnpaUzl0YjJSbGJDNVZjMlZ5XzRNREFRRUVWWE5sY2dIX2hBQUJBd0VDU1dRQkJBQUJDRlZ6WlhKdVlXMWxBUXdBQVFoUVlYTnpkMjl5WkFFTUFBQUFJZi1FSGdFQ0FRVmhaRzFwYmdFU1RXVm9aR2xBSTFsMWIzTmxabWt5TURBNEFBPT18SWwsa8Y59C0EZacv0YJVWTs9o_sIiOwFHZssTTi0-qM=',
        'port' => '2000'
    ];
    private $replacementServerId = 31;
    private $addBonuse = [
        'days' => 7,
        'bandwidth' => 10000000000
    ];
    public function handle()
    {
        $filePath = storage_path('x-ui.db');
        $this->info('Reading File');
        $result = $this->getFileAndPassItToReaderFuntion($filePath);
        return Command::SUCCESS;
    }
    private function readJsonFile($data)
    {
        //make a foreach for accounts and 
        //Create account on server and get new values from server for this account.
        //do $this->checkIfUserIsCreatedWithReseller($user,$newData)
        //loop it for all accounts...
    }
    private function readSqliteFile($filePath)
    {
        $this->warn('Gettings Inbounds from db file');
        Config::set("database.connections.external_sqlite", [
            'driver' => 'sqlite',
            "database" => $filePath,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ]);
        $sqlite = DB::connection('external_sqlite');
        $inbounds = $sqlite->select('SELECT * FROM inbounds');
        $this->info('File Read Compelete...');
        foreach ($inbounds as $inbound) {
            $this->warn('Adding User With Port ' . $inbound->port . ' to the ' . $this->destinationServer['hostname']);
            $inbountArray = json_decode(json_encode($inbound), true);
            if ($inbountArray['enable'] == 1) {
                unset($inbountArray['enable']);
                $inboundArray['enable'] = true;
            } else {
                unset($inbountArray['enable']);
                $inboundArray['enable'] = false;
            }
            $response = $this->addUserToDestinationServer($inbountArray, $this->destinationServer);
            $this->info('User Succesfully added');
            $this->warn('checking for user on reseller database...');
            if ($response) {
                $resellerBuildCheck = $this->checkIfUserIsCreatedWithReseller($inbound);
            }
            sleep(3);
            $this->info('===================================');
        }
    }
    private function getFileAndPassItToReaderFuntion($filePath)
    {
        if (!file_exists($filePath)) {
            $this->error('File not found!!!!');
            exit();
        }
        $extension = pathinfo($filePath, PATHINFO_EXTENSION);
        if ($extension == 'db') {
            $this->warn('Passing to SQLITE function');
            return $this->readSqliteFile($filePath);
        } else {
            //get file content and convert it to json
            // return $this->readJsonFile()
        }
    }

    private function addUserToDestinationServer($data, $server)
    {
        $this->warn('connecting to the destination server...');
        $this->webService = new WebService($this->destinationServer['hostname'], $this->destinationServer['port'], $this->destinationServer['cookie']);
        $response = $this->createAccount($data);
        if ($response->success == true) {
            $this->info('Succefully Added to the server');
            return true;
        } else {
            $this->error('error!: ' . $response->msg);
            return false;
        }
    }
    public function createAccount($data)
    {
        if ($this->addBonuse) {
            $total = $data['total'] + $this->addBonuse['bandwidth'];
            $data['total'] = $total;
            $expiryTime = strtotime("+" . $this->addBonuse['days'] . " days", $data['expiry_time'] / 1000) * 1000;
            $data['expiryTime'] = $expiryTime;
        }
        $data['streamSettings'] = '{
            "network": "ws",
            "security": "none",
            "wsSettings": {
              "path": "/",
              "headers": {}
            }
          }';
        $data['settings'] = '{
            "clients": [
              {
                "id": "' . md5(now()) . '",
                "alterId": 0
              }
            ],
            "disableInsecureEncryption": false
          }';
        $status = $this->webService->POST('xui/inbound/add', $data);
        if ($status == false) {
            return false;
        }
        return (object)json_decode($status, true);
    }
    private function checkIfUserIsCreatedWithReseller($user)
    {
        $this->warn('Checking for account on database');
        $account = Accounts::where('account_id', $user->remark)->first();
        if (is_null($account)) {
            $this->error('Account with remark: ' . $user->remark . ' port: ' . $user->port . ' not found!');
            return false;
        }
        $account->server_id = $this->replacementServerId;
        $this->info('New Values successfully set for account on database');
        return true;
    }
}
