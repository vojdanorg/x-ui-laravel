<?php

namespace App\Console\Commands\System;

use App\Actions\Cloudflare\CloudflareHandler;
use App\Actions\Telegram\TelegramBotBasicAction;
use App\Models\AdditionalIPS;
use App\Models\Cloudflare;
use App\Models\Servers;
use Cloudflare\API\Adapter\Adapter\Guzzle;
use Cloudflare\API\Auth\APIKey;
use Cloudflare\API\Auth\Auth;
use Cloudflare\API\Endpoints\DNS;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Utopia\Domains\Domain;
use function PHPUnit\Framework\stringContains;

class MonitoringBot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitor:servers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Using this command we will check every servers to see if they are block by Iran or not';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $botSettings = $this->getBotSettings();
        if (!$botSettings['bot_status']) {
            return 'Bot Token is not active!';
        }
        $this->telegramHandler = new TelegramBotBasicAction($botSettings['token']);
        $allowedServers = get_static_option('monitorbot_allowed_servers');
        if ($allowedServers) {
            if (checkIfStringContainsComma($allowedServers)) {
                $allowedServers = explode(',', $allowedServers);
            } else {
                $allowedServers = [$allowedServers];
            }
            $servers = Servers::whereIn('id', $allowedServers)->get();
        } else {
            $servers = [];
        }
        $this->listOfAdmins = $this->getListOfTelegramAdmins($botSettings['chat_id']);
        foreach ($servers as $server) {
            $numberOfCheckAttempts = (get_static_option('monitorbot_retry_attempts') ?? 3);
            for ($attempts = 1; $attempts <= $numberOfCheckAttempts; $attempts++) {
                echo "attempt " . $attempts . " for " . $server->server_name;
                //Check if Server Is Blocked by Iranian GFW
                $result = $this->checkForIpBlockage($server);

                //IF CheckHost TimedOut
                if (is_array($result)) {
                    //Alert Admin that server is blocked by Iran GFW
                    if ($attempts >= $numberOfCheckAttempts) {
                        foreach ($this->listOfAdmins as $tAdmin) {
                            $this->telegramHandler->sendMessage($tAdmin, implode(" \n ", $result));
                        }
                        //Check For cloudflare permission
                        $cloudflare = $this->getCloudflareAccount($server);
                        if ($cloudflare && count($result) >= 4) {
                            echo "\n <br> Everything is set. going to chaning ip <br> \n";
                            $cloudFlareHandler = new CloudflareHandler($cloudflare->email, $cloudflare->api_token, $cloudflare->zone_id, $cloudflare->global_key);
                            $dnsZone = $cloudFlareHandler->getDNSRecordDetailsByName($server->hostname);
                            $checkForMultiUser = checkIfServerIsAllowedByAnyPackageToBeMultiUser($server);
                            if ($checkForMultiUser) {
                                //Create a new CNAME for mutiuser servers
                                $this->handleMultiUserOnDomainChange($server, $cloudflare);
                            } else {
                                //Change IP of server
                                $this->handleServerIPChange($dnsZone, $server, $cloudflare);
                            }
                        }
                        break;
                    } else {
                        sleep(10);
                        echo "Number of attempts was: " . $attempts;
                        continue;
                    }
                } elseif (!$result) { //if we failed to get check results from check-host.
                    $this->telegramHandler->sendMessage($botSettings['chat_id'], implode(" \n ", [
                        "خطا در دریافت نتیجه تست از چک هاست برای سرور زیر: ",
                        $server->server_name
                    ]));
                } else {
                    break;
                }
            }
        }
    }

    private function getListOfTelegramAdmins()
    {
        $listOfAdmins = get_static_option('monitorbot_admin_telegram_number_id');
        if (checkIfStringContainsComma($listOfAdmins)) {
            $listOfAdmins = explode(',', $listOfAdmins);
        } else {
            $listOfAdmins = [$listOfAdmins];
        }
        return $listOfAdmins;
    }

    private
    function getBotSettings()
    {
        $settings = [
            'token' => get_static_option('monitorbot_robot_token'),
            'first_check_host_name' => get_static_option('monitorbot_first_check_host_name'),
            'second_check_host_name' => get_static_option('monitorbot_second_check_host_name'),
            'chat_id' => get_static_option('monitorbot_admin_telegram_number_id'),
            'bot_status' => get_static_option('monitorbot_bot_status'),
        ];
        $this->botSettings = $settings;
        return $settings;
    }

    private
    function checkForIpBlockage($server)
    {
        $httpTest = $this->requestForHttpTestCheckHost($server);
        if (!$httpTest) {
            return false;
        }
        sleep(get_static_option('monitorbot_wait_for_check_result') ?? 60);
        $checkForResult = $this->checkForCheckHostResult($httpTest, $server);
        return $checkForResult;
    }

    private
    function requestForHttpTestCheckHost($server)
    {
        $checkForCDN = checkIfServerHasCDNSettings($server);
        if ($server) {
            $response = Http::accept('application/json')->get('https://check-host.net/check-http?host=https://' . $server->hostname . '&node=' . $this->botSettings['first_check_host_name'] . ($this->botSettings['second_check_host_name'] ? '&node=' . $this->botSettings['second_check_host_name'] : null));
        } else {
            $response = Http::accept('application/json')->get('https://check-host.net/check-http?host=' . $server->hostname . ':' . $server->panel_port . '&node=' . $this->botSettings['first_check_host_name'] . ($this->botSettings['second_check_host_name'] ? '&node=' . $this->botSettings['second_check_host_name'] : null));
        }
        $responseChecker = $this->checkHostResponseStatusCodeHandler($response);
        if ($responseChecker) {
            return $responseChecker['request_id'];
        } else {
            return false;
        }
    }

    private
    function checkHostResponseStatusCodeHandler($response)
    {
        if ($response->successful()) {
            return $response->json();
        } else {
            return false;
        }
    }

    private
    function checkForCheckHostResult($requestID, $server)
    {
        $response = Http::accept('application/json')->get('https://check-host.net/check-result/' . $requestID);
        $responseChecker = $this->checkHostResponseStatusCodeHandler($response);
        if ($responseChecker) {
            $responseArray = [];
            if (is_null($responseChecker[$this->botSettings['first_check_host_name']])) {
                $responseArray[] = $server->hostname . ':' . $server->panel_port . ' ' . $server->server_name;
                $responseArray[] = 'سرور شما در هاست ' . $this->botSettings['first_check_host_name'] . ' بلاک شده';
                if ($this->botSettings['second_check_host_name']) {
                    if (is_null($responseChecker[$this->botSettings['second_check_host_name']])) {
                        $responseArray[] = $server->hostname . ':' . $server->panel_port . ' ' . $server->server_name;
                        $responseArray[] = 'سرور شما در هاست ' . $this->botSettings['second_check_host_name'] . ' بلاک شده';
                    }
                }
            } else {
                if ($responseChecker[$this->botSettings['first_check_host_name']][0][0] == 0 && $responseChecker[$this->botSettings['first_check_host_name']][0][2] == 'Connection timed out') {
                    $responseArray[] = $server->hostname . ':' . $server->panel_port . ' ' . $server->server_name;
                    $responseArray[] = 'سرور شما در هاست ' . $this->botSettings['first_check_host_name'] . ' بلاک شده';
                }
                if ($this->botSettings['second_check_host_name']) {
                    if (is_null($responseChecker[$this->botSettings['second_check_host_name']])) {
                        $responseArray[] = $server->hostname . ':' . $server->panel_port . ' ' . $server->server_name;
                        $responseArray[] = 'سرور شما در هاست ' . $this->botSettings['second_check_host_name'] . ' بلاک شده';
                    } else {
                        if ($responseChecker[$this->botSettings['second_check_host_name']][0][0] == 0 && $responseChecker[$this->botSettings['second_check_host_name']][0][2] == 'Connection timed out') {
                            $responseArray[] = $server->hostname . ':' . $server->panel_port . ' ' . $server->server_name;
                            $responseArray[] = 'سرور شما در هاست ' . $this->botSettings['second_check_host_name'] . ' بلاک شده';
                        }
                    }
                }
            }
            if (empty($responseArray)) {
                return true;
            }
            $responseArray[] = 'بررسی شده در ' . date('Y-m-d H:m:s');
            return $responseArray;
        } else {
            return false;
        }
    }

    private
    function getCloudflareAccount($server)
    {
        $cloudflare = false;
        if (isset($server->json_data['cloudflare'])) {
            if (!$server->json_data['cloudflare']) {
                return false;
            }
            $cloudflare = Cloudflare::where('id', $server->json_data['cloudflare'])->first();
            if (is_null($cloudflare)) {
                $cloudflare = false;
            }
        }
        return $cloudflare;
    }

    private
    function changeCloudFlareIP($cloudflare, $server, $ip_address)
    {
        if ($cloudflare) {
            $cloudFlareHandler = new CloudflareHandler($cloudflare->email, $cloudflare->api_token, $cloudflare->zone_id, $cloudflare->global_key);
            $dnsZone = $cloudFlareHandler->getDNSRecordDetailsByName($server->hostname);
            if ($dnsZone['content'] == $ip_address) {
                return 201;
            }
            AdditionalIPS::where('ip_address', $dnsZone['content'])->update([
                'status' => 2
            ]);
            $response = $cloudFlareHandler->updateDnsZoneByID($dnsZone['id'], [
                'type' => 'A',
                'name' => $server->hostname,
                'content' => $ip_address,
                'ttl' => 1,
                'comment' => 'changed on: ' . Carbon::now()->format("Y-m-d H:i:s")
            ]);
            if ($response['success']) {
                return true;
            } else {
                return false;
            }
        } else {
            return null;
        }
    }

    private
    function createCloudFlareCname($cloudflare, $server)
    {
        if ($cloudflare) {
            $cloudFlareHandler = new CloudflareHandler($cloudflare->email, $cloudflare->api_token, $cloudflare->zone_id, $cloudflare->global_key);
            $mainDomain = $domain = new Domain($server->hostname);
            $randomDomain = getRandomWord(63) . '.' . $mainDomain->getRegisterable();
            $response = $cloudFlareHandler->createDnsZone([
                'type' => 'CNAME',
                'name' => $randomDomain,
                'content' => $server->iran_ip,
                'ttl' => 1,
                'proxied' => true,
                'comment' => 'created on: ' . Carbon::now()->format("Y-m-d H:i:s")
            ]);
            if ($response['success']) {
                $server->hostname = $randomDomain;
                $jsonData = $server->json_data;
                $jsonData['cdn']['domain'] = $randomDomain;
                $jsonData['extra_domain_or_ip'][] = $randomDomain;
                $server->json_data = $jsonData;
                $server->save();
                return $randomDomain;
            } else {
                return false;
            }
        } else {
            return null;
        }
    }

    private
    function handleMultiUserOnDomainChange($server, $cloudflare)
    {
        foreach ($this->listOfAdmins as $tAdmin) {
            $this->telegramHandler->sendMessage($tAdmin, implode(" \n ", [
                'درحال ایجاد دامنه جدید ...',
                'مشخصات سرور:',
                $server->server_name,
                $server->hostname,
            ]));
        }
        $createNewCNAME = $this->createCloudFlareCname($cloudflare, $server);
        if ($createNewCNAME) {
            foreach ($this->listOfAdmins as $tAdmin) {
                $this->telegramHandler->sendMessage($tAdmin, implode(" \n ", [
                    'دامنه جدید با موفقیت ایجاد شد',
                    'مشخصات سرور:',
                    $server->server_name,
                    $server->hostname,
                    'آدرس جدید دامنه:',
                    $createNewCNAME
                ]));
            }
        }
    }

    private
    function handleServerIPChange($dnsZone, $server, $cloudflare)
    {
        //if ($dnsZone['content'] != $server->iran_ip) {
            foreach ($this->listOfAdmins as $tAdmin) {
                $this->telegramHandler->sendMessage($tAdmin, implode(" \n ", [
                    'درحال تغییر آیپی به ایپی جدید ...',
                    'مشخصات سرور:',
                    $server->server_name,
                    $server->hostname,
                    'ایپی جدید:',
                    $server->iran_ip,
                ]));
            }
            $additionalIP = $this->getFirstAvailableIPForServer($server->id);
            echo $additionalIP->ip_address;
            $changeIPResult = $this->changeCloudFlareIP($cloudflare, $server, $additionalIP->ip_address);
            if ($changeIPResult) {
                $additionalIP->status = 1;
                $additionalIP->save();
                foreach ($this->listOfAdmins as $tAdmin) {
                    $this->telegramHandler->sendMessage($tAdmin, implode(" \n ", [
                        'ایپی با موفقیت به ایپی جدید تغییر کرد',
                        'مشخصات سرور:',
                        $server->server_name,
                        $server->hostname,
                        'ایپی جدید:',
                        $server->iran_ip,
                    ]));
                }
            }
        //}
    }

    private function getFirstAvailableIPForServer($serverID)
    {
        $ip = AdditionalIPS::where('server_id', $serverID)->where('status', 0)->first();
        return $ip;
    }
}
