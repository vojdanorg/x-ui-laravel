<?php

namespace App\Console\Commands\System;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use App\Models\Package;
use App\Models\Servers;
use App\Actions\Api\PritunlWebService;
use App\Models\Accounts;

class CheckForPritunlExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pritunl:check-for-expiration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command helps you to find and disable Configs that are Expired';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $accounts = Accounts::whereJsonContains('json_data->type', 'pritunl')->get();
        if ($accounts->count() <= 0) {
            return 0;
        }
        foreach ($accounts as $account) {
            $package = Package::where('id', $account->package_id)->first();
            if (is_null($package)) {
                continue;
            }
            $server = Servers::where('id', $account->server_id)->first();
            if (is_null($server)) {
                continue;
            }
            $addPackageDaysToCreationTime = \Carbon\Carbon::parse($account->created_at)->addDays($package->expire_at);
            if (!$addPackageDaysToCreationTime->isPast()) {
                continue;
            }
            $pritunlHandler = new PritunlWebService($server->json_data['pritunl_data']['api_token'], $server->json_data['pritunl_data']['api_secret'], $server->tunnel_ip);
            $accountDetails = $pritunlHandler->getUserDetails($server->json_data['pritunl_data']['organization_id'], $account->account_id);
            if (!$accountDetails) {
                continue;
            }
            if ($accountDetails['disabled'] == true) {
                $addPackageDaysToCreationTime = \Carbon\Carbon::parse($account->created_at)->addDays($package->expire_at + 2);
                if ($addPackageDaysToCreationTime->isPast()) {
                    $delete = $pritunlHandler->deleteAccount($server->json_data['pritunl_data']['organization_id'], $account->account_id);
                    $account->delete();
                    continue;
                }
            } else {
                $disable = $pritunlHandler->disableUser($server->json_data['pritunl_data']['organization_id'], $account->account_id, $accountDetails);
                continue;
            }
        }
    }
}
