<?php

namespace App\Console\Commands\System;

use App\Models\Servers;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CreateBackupFromServers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:servers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Using this command, You can make json backup from servers';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $servers = Servers::all();
        foreach ($servers as $server) {
            $accounts = getClientsOnServer($server->id);
            if(is_array($accounts)){
                Storage::disk('backup')->put($server->tunnel_ip.'/'.date('Y-m-d').'.json',json_encode($accounts));
            }
        }
    }
}
