<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Actions\Api\WebService;
use App\Models\Accounts;
use App\Models\User;

class addLostAccountsToUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add_lost_accounts_to_users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    private $server = [
        'hostname' => '5.181.80.112',
        'cookie' => 'session=MTY2ODM1NzYyM3xEdi1CQkFFQ180SUFBUkFCRUFBQVpmLUNBQUVHYzNSeWFXNW5EQXdBQ2t4UFIwbE9YMVZUUlZJWWVDMTFhUzlrWVhSaFltRnpaUzl0YjJSbGJDNVZjMlZ5XzRNREFRRUVWWE5sY2dIX2hBQUJBd0VDU1dRQkJBQUJDRlZ6WlhKdVlXMWxBUXdBQVFoUVlYTnpkMjl5WkFFTUFBQUFGXy1FRkFFQ0FRVmhaRzFwYmdFSU1EQTVPVEV4TWpJQXyAbfoCIjFiwI571tRWMVBzxolpfykfSTMCrFtQOE67_w==',
        'port' => '2000'
    ];
    private $replacementServerId = 30;
    public function handle()
    {
        $this->createWebService();
        $response = $this->executeCommand();
        return Command::SUCCESS;
    }
    private function executeCommand()
    {
        $clients = $this->getServerClients();
        if ($clients) {
            foreach ($clients->obj as $client) {
                if (str_contains($client['remark'], 'user')) {
                    $this->doRebuild($client);
                }
            }
        } else {
            $this->error('Faild to connect to the server');
        }
    }

    private function doRebuild($client)
    {
        $remark = explode(',', $client['remark']);
        $users = User::where('id', $remark[1])->first();
        if (is_null($users)) {
            $this->error('user for remark: ' . $client['remark'] . ' not found');
            return false;
        }
        $account = Accounts::where('id', $remark[2])->where('user_id', $users->id)->first();
        if (!is_null($account)) {
            $account->server_id = $this->replacementServerId;
            $account->save();
            $this->info('Client Updated!');
        }
        Accounts::create([
            'user_id' => $users->id,
            'account_name' => $client['port'],
            'server_id' => $this->replacementServerId,
            'account_id' => $client['remark'],
        ]);
        $this->info('Account ' . $client['port'] . ' for reseller: ' . $users->email . ' created');
        sleep(3);
        return true;
    }

    private function createWebService()
    {
        $this->webService = new WebService($this->server['hostname'], $this->server['port'], $this->server['cookie']);
    }
    private function getServerClients()
    {
        $status = $this->webService->POST('xui/inbound/list');
        if ($status == false) {
            return false;
        }
        return (object)json_decode($status, true);
    }
}
