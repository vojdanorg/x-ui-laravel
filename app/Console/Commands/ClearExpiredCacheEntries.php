<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class ClearExpiredCacheEntries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:clear-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear expired cache entries for online users';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cacheKeys = Cache::get('online-users', []);
        $expiredKeys = [];

        foreach ($cacheKeys as $key) {
            if (! Cache::has($key)) {
                $expiredKeys[] = $key;
            }
        }

        Cache::forget($expiredKeys);
        Cache::put('online-users', array_diff($cacheKeys, $expiredKeys));
        return Command::SUCCESS;
    }
}
