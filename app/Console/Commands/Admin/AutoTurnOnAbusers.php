<?php

namespace App\Console\Commands\Admin;

use App\Actions\Servers\Servers;
use App\Models\Accounts;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AutoTurnOnAbusers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'abusers:enable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enable Users who are blocked because of abusing servers';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $accounts = Accounts::where('block_reason',3)->get();
        foreach ($accounts as $account) {
            if (is_null($account->package)) {
                Log::warning('Account ' . $account->account_name . ' with id ' . $account->id . ' dont have a package. please add package for it. Check failed. Going for next account');
                continue;
            }
            if (is_null($account->servers)) {
                Log::alert('Account has no servers ' . $account->name . ' id ' . $account->id) . '. Check failed. Going for next account';
                continue;
            }
            $accountDetail = getUserAccountDetail($account);
            if (!$accountDetail) {
                Log::alert('Cant connect to the server of ' . $account->name . ' id ' . $account->id . '. Server: ' . $account->servers->server_name . '. Check failed. Going for next account');
                continue;
            }
            $accountDetail['enable'] = true;
            $response = (new Servers())->editAccount($account->servers->id,$accountDetail,$accountDetail['id']);
            if(!$response){
                Log::alert('Cant connect to the server of ' . $account->name . ' id ' . $account->id . '. Server: ' . $account->servers->server_name . '. Check failed. Going for next account');
                continue;
            }
            $account->block_reason = 0;
            $account->save();
            Log::notice('User '.$account->account_name.' enabled. Reason was: Too Much IPs Connected. Maximum allowed: '.$account->package->user_limit);
        }
//        return Command::SUCCESS;
    }
}
