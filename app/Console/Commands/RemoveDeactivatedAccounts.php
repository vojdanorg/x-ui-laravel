<?php

namespace App\Console\Commands;

use App\Actions\Servers\Servers;
use App\Models\Accounts;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class RemoveDeactivatedAccounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'removeacc:deactivated';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!get_static_option('auto_delete_deactivated_clients_on_servers')) {
            return false;
        }
        $servers = \App\Models\Servers::all();
        foreach ($servers as $server) {
            if (isset($server->json_data['type'])) {
                continue;
            }
            $clients = getClientsOnServer($server->id);
            if (!$clients) {
                continue;
            }
            if (empty($clients)) {
                continue;
            }
            foreach ($clients as $client) {
                if (!$client['enable']) {
                    $xDaysAgo = Carbon::now()->subDays(get_static_option('auto_delete_deactivated_clients_on_servers_wait_before_delete'));
                    $isMoreThanXDaysOld = Carbon::createFromTimestamp($client['expiryTime'] / 1000)->lt($xDaysAgo);
                    if ($isMoreThanXDaysOld) {
                        $accountId = explode(',', $client['remark']);
                        @$account = Accounts::where('id', $accountId[2])->first();
                        $response = (new Servers)->deleteAccount($server->id, $client['id']);
                        if ($response && $response->success) {
                            @Accounts::where('id', $account->id)->delete();
                        }
                    }
                    sleep(1);
                }
            }
        }
        return Command::SUCCESS;
    }
}
