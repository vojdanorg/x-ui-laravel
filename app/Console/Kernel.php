<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('backup:servers')->hourly()->withoutOverlapping()->runInBackground();
        $schedule->command('monitor:servers')->everyFiveMinutes()->withoutOverlapping()->runInBackground();
        $schedule->command('pritunl:check-for-expiration')->hourly()->withoutOverlapping()->runInBackground();
        $schedule->command('removeacc:deactivated')->hourly()->withoutOverlapping()->runInBackground();
//         $schedule->command('abusers:enable')->everyMinute()->withoutOverlapping()->runInBackground();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
