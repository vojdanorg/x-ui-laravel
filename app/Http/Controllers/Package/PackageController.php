<?php

namespace App\Http\Controllers\Package;

use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\Servers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PackageController extends Controller
{
    public function add($type = 'v2ray')
    {
        $pageTitle = 'افزودن پکیج ' . ucfirst($type);
        $servers = DB::table('servers')->get();
        if (count($servers) > 0){

            if ($type == 'v2ray') {
                return view('paper.pages.package.add', compact('pageTitle', 'servers'));
            }
            if ($type == 'pritunl') {
                return view('paper.pages.package.pritunl.add', compact('pageTitle'));
            }
        } else {
            smilify('error', 'جهت ساخت پکیج جدید لطفا حداقل یک سرور به سایت خود بیافزایید');
            return redirect()->back();
        }
    }

    public function doAdd($type = 'v2ray', Request $request)
    {
        if ($type == 'v2ray') {
            $request->validate([
                'name' => 'required',
                'price' => 'numeric|required',
                'expire_at' => 'numeric|required',
                'bandwidth' => 'numeric|required',
                'user_limit' => 'numeric|required',
                'description' => 'required'
            ]);

            $data = [
                'name' => $request->name,
                'price' => $request->price,
                'expire_at' => $request->expire_at,
                'bandwidth' => $request->bandwidth,
                'user_limit' => $request->user_limit,
                'protocol' => [
                    'protocol' => $request->protocol,
                    'transmission' => ($request->transmission === 'websocket' ? 'ws' : $request->transmission),
                    'ssl_protocol' => (isset($request->xtls_protocol) ? 'xtls' : (isset($request->tls_protocol) ? 'tls' : false)),
                    'reality_security' => (isset($request->reality_security) ? true : false),
                ],
                'description' => $request->description,
                'price_level' => $request->price_level
            ];
        }
        if ($type == 'pritunl') {
            $request->validate([
                'name' => 'required',
                'price' => 'numeric|required',
                'expire_at' => 'numeric|required',
                'description' => 'required'
            ]);
            $data = [
                'name' => $request->name,
                'price' => $request->price,
                'expire_at' => $request->expire_at,
                'bandwidth' => 0,
                'user_limit' => 0,
                'protocol' => [
                    'protocol' => null,
                    'transmission' => null,
                    'ssl_protocol' => null,
                    'type' => 'pritunl'
                ],
                'description' => $request->description,
                'price_level' => $request->price_level
            ];
        }
        $data['status'] = 0;
        if (isset($request->status)) {
            $data['status'] = 1;
        }
        Package::create($data);
        smilify('success', 'پکیج جدید ایجاد شد');
        return redirect()->back();
    }

    public function list()
    {
        $pageTitle = 'لیست پکیج ها';
        $packages = Package::all();
        return view('paper.pages.package.list', compact('pageTitle', 'packages'));
    }

    public function edit($type = 'v2ray', $id)
    {
        $pageTitle = 'ویرایش پکیج';
        $package = Package::findOrFail($id);
        if ($type == 'v2ray') {
            $serverList = Servers::all();
            $servers = [];
            $packages = Package::all();
            foreach ($serverList as $server) {
                if (checkIfServerHasCDNSettings($server)) {
                    $servers[] = $server;
                }
            }


                $user_list = User::all();
                $users = [];
                foreach ($user_list as $user) {
                        $users[] = $user;
                }

                return view('paper.pages.package.edit', compact('pageTitle', 'package', 'servers', 'packages', 'users'));
            } elseif ($type == 'pritunl') {
                return view('paper.pages.package.pritunl.edit', compact('pageTitle', 'package'));
            }
        }

        public
        function doEdit($type = 'v2ray', $id, Request $request)
        {
            if ($type == 'v2ray') {
                $request->validate([
                    'name' => 'required',
                    'price' => 'numeric|required',
                    'expire_at' => 'numeric|required',
                    'bandwidth' => 'numeric|required',
                    'user_limit' => 'numeric|required',
                    'description' => 'required'
                ]);
                $multiUser = false;
                if (isset($request->allow_multi_user)) {
                    $multiUser = $this->validateMultiUser($request);
                    if (!$multiUser) {
                        smilify('error', 'برای استفاده از قابلیت چند کاربر، باید حداقل یک سرور و اکانت مستر انتخاب کنید');
                        return redirect()->back();
                    }
                }
                $data = [
                    'name' => $request->name,
                    'price' => $request->price,
                    'expire_at' => $request->expire_at,
                    'bandwidth' => $request->bandwidth,
                    'user_limit' => $request->user_limit,
                    'protocol' => [
                        'protocol' => $request->protocol,
                        'transmission' => ($request->transmission === 'websocket' ? 'ws' : $request->transmission),
                        'ssl_protocol' => (isset($request->xtls_protocol) ? 'xtls' : (isset($request->tls_protocol) ? 'tls' : false)),
                        'multi_user' => $multiUser,
                        'in_sync_package_id' => $request->in_sync_package_id,
                        'reality_security' => (isset($request->reality_security) ? true : false),
                    ],
                    'description' => $request->description,
                    'price_level' => $request->price_level
                    ];
            } elseif ($type == 'pritunl') {
                $request->validate([
                    'name' => 'required',
                    'price' => 'numeric|required',
                    'expire_at' => 'numeric|required',
                    'description' => 'required'
                ]);
                $data = [
                    'name' => $request->name,
                    'price' => $request->price,
                    'expire_at' => $request->expire_at,
                    'bandwidth' => 0,
                    'user_limit' => 0,
                    'protocol' => [
                        'protocol' => null,
                        'transmission' => null,
                        'ssl_protocol' => null,
                        'type' => 'pritunl'
                    ],
                    'description' => $request->description,
                    'price_level' => $request->price_level

                ];
            }
            $data['status'] = 0;
            if (isset($request->status)) {
                $data['status'] = 1;
            }
            Package::where('id', $id)->update($data);
            smilify('success', 'پکیج جدید ویرایش شد');
            return redirect()->back();
        }

        public
        function delete($type = 'v2ray', $id)
        {
            $package = Package::where('id', $id)->with('accounts')->first();
//            if ($package->accounts->count() > 0) {
//                smilify('error', 'پکیج دارای سرویس های فعال است. امکان حذف وجود ندارد. برای حذف ابتدا سرویس های فعال را حذف کنید');
//                return redirect()->back();
//            }
            $package->delete();
            smilify('success', 'پکیج حذف شد');
            return redirect()->back();
        }

        private
        function validateMultiUser($request)
        {

            if (!isset($request->allowed_servers)) {
                return false;
            }
            if (!isset($request->master_accounts)) {
                return false;
            }
            $servers = Servers::whereIn('id', $request->allowed_servers)->get();
            if ($servers->count() == 0) {
                return false;
            }
            $allowedList = [];
            foreach ($request->master_accounts as $masterAccount) {
                $account = explode(',', $masterAccount);
                foreach ($servers as $server) {
                    if ($server->id == $account[0]) {
                        $accountDetails = getUserAccountDetailById($account[1], $server->id);
                        if (!$accountDetails) {
                            return false;
                        }
                        $allowedList[] = [
                            'server' => $server->id,
                            'master_account' => (int)$account[1]
                        ];
                    }
                }
            }
            return $allowedList;
        }
    }
