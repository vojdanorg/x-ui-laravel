<?php

namespace App\Http\Controllers\CustomerPanel;

use App\Http\Controllers\Controller;
use App\Models\Customer\Customer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function __construct()
    {
        if (isset(getallheaders()['Token'])) {
            $this->user = User::where('api_token', getallheaders()['Token'])->first();
        } else {
            return response()->json([
                'code' => 403,
                'success' => false,
                'message' => 'forbidden',
                'body' => []
            ]);
        }
    }

    public function register(Request $request)
    {
        $validator = [
            'email' => 'required|max:512|email|unique:customer,email',
            'password' => 'required|min:8',
            'fullname' => 'required|max:256',
            'mobile' => 'max:12',
            'telegram_id' => 'max:100'
        ];
        $request->validate($validator);
        $customer = [
            'user_id' => $this->user->id,
            'fullname' => $request->fullname,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'password' => sha1(md5($request->password)),
            'json_data'=>[
                'telegram_id'=>$request->telegram_id
            ]
        ];
        $customer_id = Customer::create($customer)->id;
        if (!$customer_id) {
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => 'خطا در ایجاد حساب کاربری',
                'body' => []
            ]);
        }
        unset($customer['user_id']);
        $customer['customer_id'] = $customer_id;
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'ثبت نام با موفقیت انجام شد',
            'body' => $customer
        ]);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $customer = Customer::where('email', $request->email)->where('password', sha1(md5($request->password)))->first();
        if (is_null($customer)) {
            return response()->json([
                'code' => 403,
                'success' => false,
                'message' => 'نام کاربری یا رمز عبور اشتباه است',
                'body' => []
            ]);
        }
        $customerArray = $customer->toArray();
        unset($customerArray['created_at']);
        unset($customerArray['updated_at']);
        unset($customerArray['user_id']);
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'خوش آمدید ' . $customer->fullname,
            'body' => $customerArray
        ]);
    }

    public function appLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'reseller_id' => 'required',
            'confirm_code' => 'required'
        ]);
        $customer = Customer::where('email', $request->email)->where('password', sha1(md5($request->password)))->first();
        if (is_null($customer)) {
            return response()->json([
                'code' => 403,
                'success' => false,
                'message' => 'نام کاربری یا رمز عبور اشتباه است',
                'body' => []
            ]);
        }
        $customerArray = $customer->toArray();
        unset($customerArray['created_at']);
        unset($customerArray['updated_at']);
        unset($customerArray['user_id']);
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'خوش آمدید ' . $customer->fullname,
            'body' => $customerArray
        ]);
    }
}
