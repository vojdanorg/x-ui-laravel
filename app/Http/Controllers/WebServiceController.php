<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class WebServiceController extends Controller
{
    public function settings()
    {
        if(is_null(auth()->user()->api_token)){
            User::where('id',auth()->user()->id)->update([
                'api_token'=>md5(Hash::make(now()))
            ]);
        }
        $pageTitle = 'تنظیمات وب سرویس';
        return view('paper.pages.web-service.settings', compact('pageTitle'));
    }
}
