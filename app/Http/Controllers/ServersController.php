<?php

namespace App\Http\Controllers;

use App\Actions\Servers\Servers as ServersServers;
use App\Http\Controllers\Account\AccountController;
use App\Models\Accounts;
use App\Models\AdditionalIPS;
use App\Models\Cloudflare;
use App\Models\Servers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Actions\Api\PritunlWebService;

class ServersController extends Controller
{
    public function status()
    {
        $pageTitle = 'وضعیت سرور ها';
        $v2ray_servers = getV2RayServers();
        $pritunl_servers = getPritunlServers();
        return view('paper.pages.servers.status', compact('pageTitle', 'v2ray_servers', 'pritunl_servers'));
    }

    public function add()
    {
        $pageTitle = 'افزودن سرور جدید';
        $cloudflares = Cloudflare::all();
        return view('paper.pages.servers.add', compact('pageTitle', 'cloudflares'));
    }

    public function doAdd(Request $request)
    {
        $validationRules = [
            'hostname' => 'required|max:1024',
            'panel_port' => 'required|max:12',
            'panel_cookie' => 'required',
            'server_name' => 'required|max:1024',
            'max_users' => 'required',
            'server_provider_website' => 'required',
            'tunnel_ip' => 'required',
            'tunnel_cookie' => 'required',
            'public_key' => 'required',
            'private_key' => 'required'
        ];
        $cdn = false;
        if (isset($request->cdn_domain)) {
            $validationRules['cdn_domain'] = 'required';
            $validationRules['cdn_path'] = 'required';
            $validationRules['https_port'] = 'required';
            $cdn = true;
        }
        $validate = $request->validate($validationRules);
        if ($cdn) {
            $cdn = [];
            $cdn['domain'] = $request->cdn_domain;
            $cdn['path'] = $request->cdn_path;
            $cdn['https_port'] = $request->https_port;
        }
        if ($request->cloudflare_id == 0) {
            $cloudflare = false;
        } else {
            $cloudflare = Cloudflare::where('id', $request->cloudflare_id)->first();
            if (is_null($cloudflare)) {
                smilify('danger', 'اکانت کلود فلیر یافت نشد');
                return redirect()->back();
            }
            $cloudflare = $cloudflare->id;
        }
        Servers::create([
            'hostname' => $request->hostname,
            'iran_ip' => $request->iran_ip,
            'panel_port' => $request->panel_port,
            'panel_cookie' => $request->panel_cookie,
            'server_name' => $request->server_name,
            'max_users' => $request->max_users,
            'server_provider_website' => $request->server_provider_website,
            'tunnel_ip' => $request->tunnel_ip,
            'eu_ip' => $request->eu_ip,
            'tunnel_cookie' => $request->tunnel_cookie,
            'json_data' => [
                'keys' => [
                    'public_key' => $request->public_key,
                    'private_key' => $request->private_key
                ],
                'cdn' => $cdn,
                'cloudflare' => $cloudflare
            ]
        ]);
        smilify('success', 'سرور جدید ایجاد شد');
        return redirect()->back();
    }

    public function edit($id)
    {
        $pageTitle = 'ویرایش سرور';
        $server = Servers::where('id', $id)->first();
        if (!checkIfServerIsPritunl($server)) {
            $cloudflares = Cloudflare::all();
            return view('paper.pages.servers.edit', compact('pageTitle', 'server', 'cloudflares'));
        } else {
            $pritunlHandler = new PritunlWebService($server->json_data['pritunl_data']['api_token'], $server->json_data['pritunl_data']['api_secret'], $server->tunnel_ip);
            $response = $pritunlHandler->getOrganizations();
            $servers = $pritunlHandler->getServers();
            if (!$response) {
                $response = [];
            }
            return view('paper.pages.servers.pritunl.edit', compact('pageTitle', 'server', 'response', 'servers'));
        }
    }

    public function doEdit($id, Request $request)
    {
        $server = Servers::where('id', $id)->first();
        if (!checkIfServerIsPritunl($server)) {
            $validationRules = [
                'hostname' => 'required|max:1024',
                'panel_port' => 'required|max:12',
                'panel_cookie' => 'required',
                'server_name' => 'required|max:1024',
                'max_users' => 'required',
                'server_provider_website' => 'required',
                'tunnel_ip' => 'required',
                'tunnel_cookie' => 'required',
                'public_key' => 'required',
                'private_key' => 'required'
            ];
            $validate = $request->validate($validationRules);
            $cdn = false;
            if (isset($request->cdn_domain)) {
                $validationRules['cdn_domain'] = 'required';
                $validationRules['cdn_path'] = 'required';
                $validationRules['https_port'] = 'required';
                $cdn = true;
            }
            $validate = $request->validate($validationRules);
            if ($cdn) {
                $cdn = [];
                $cdn['domain'] = $request->cdn_domain;
                $cdn['path'] = $request->cdn_path;
                $cdn['https_port'] = $request->https_port;
            }
            if ($request->cloudflare_id == 0) {
                $cloudflare = false;
            } else {
                $cloudflare = Cloudflare::where('id', $request->cloudflare_id)->first();
                if (is_null($cloudflare)) {
                    smilify('danger', 'اکانت کلود فلیر یافت نشد');
                    return redirect()->back();
                }
                $cloudflare = $cloudflare->id;
            }
            $server = Servers::where('id', $id)->first();
            $jsonData = $server->json_data;
            $jsonData['keys'] = [
                'public_key' => $request->public_key,
                'private_key' => $request->private_key
            ];
            $jsonData['cdn'] = $cdn;
            $jsonData['cloudflare'] = $cloudflare;
            Servers::where('id', $id)->update([
                'hostname' => $request->hostname,
                'iran_ip' => $request->iran_ip,
                'panel_port' => $request->panel_port,
                'panel_cookie' => $request->panel_cookie,
                'server_name' => $request->server_name,
                'max_users' => $request->max_users,
                'server_provider_website' => $request->server_provider_website,
                'tunnel_ip' => $request->tunnel_ip,
                'eu_ip' => $request->eu_ip,
                'tunnel_cookie' => $request->tunnel_cookie,
                'json_data' => $jsonData
            ]);
        } else {
            $request->validate([
                'server_name' => 'required',
                'server_domain' => 'required',
                'api_token' => 'required',
                'api_secret' => 'required',
                'max_users' => 'required',
                'organization_id' => 'required',
                'server_id' => 'required'
            ]);

            Servers::where('id', $id)->update([
                'hostname' => "null",
                'iran_ip' => "null",
                'panel_port' => "null",
                'panel_cookie' => "null",
                'server_name' => $request->server_name,
                'max_users' => $request->max_users,
                'server_provider_website' => "null",
                'tunnel_ip' => $request->server_domain,
                'eu_ip' => $request->server_domain,
                'tunnel_cookie' => "null",
                'json_data' => [
                    'keys' => [
                        'public_key' => "null",
                        'private_key' => "null"
                    ],
                    'cdn' => false,
                    'cloudflare' => false,
                    'type' => 'pritunl',
                    'pritunl_data' => [
                        'api_token' => $request->api_token,
                        'api_secret' => $request->api_secret,
                        'organization_id' => $request->organization_id,
                        'server_id' => $request->server_id
                    ]
                ]
            ]);
        }
        smilify('success', 'سرور ویرایش شد');
        return redirect()->back();
    }

    public function delete($id)
    {
        Servers::where('id', $id)->delete();
        Accounts::where('server_id', $id)->delete();
        smilify('success', 'سرور حذف شد');
        return redirect()->back();
    }

    public function getServerStatus($serverId)
    {
        $server = Servers::findOrFail($serverId);
        if (!isset($server->json_data['type'])) {
            $countServerClients = countServerClients($serverId);
            if ($countServerClients >= $server->max_users) {
                $countServerClients = '<span class="text-danger">تکمیل</span>';
            }
            $hasCDN = false;
            if (isset($server->json_data['cdn'])) {
                if ($server->json_data['cdn']) {
                    $hasCDN = true;
                }
            }
            $serverStatus = getServerStatus($serverId, true);
            return response()->json([
                'code' => 200,
                'success' => false,
                'message' => 'Successfully retrieved',
                'body' => [
                    'serverClients' => $countServerClients,
                    'serverStatus' => $serverStatus,
                    'has_cdn' => $hasCDN
                ]
            ]);
        } else {
            if ($server->json_data['type'] == 'pritunl') {
                $countServerClients = countServerClients($serverId, false, 'pritunl', $server);
                if ($countServerClients >= $server->max_users) {
                    $countServerClients = '<span class="text-danger">تکمیل</span>';
                }
                $serverStatus = getServerStatus($serverId, true, 'pritunl', $server);
                return response()->json([
                    'code' => 200,
                    'success' => false,
                    'message' => 'Successfully retrieved',
                    'body' => [
                        'serverClients' => $countServerClients,
                        'serverStatus' => $serverStatus,
                        'has_cdn' => false
                    ]
                ]);
            }
        }
    }

    public function addClientGetServerStatus($serverId)
    {
        $server = Servers::findOrFail($serverId);
        if (!checkIfServerIsPritunl($server)) {
            $countServerClients = countServerClients($serverId);
        } else {
            $countServerClients = countServerClients($serverId, false, 'pritunl', $server);
        }
        if ($countServerClients >= $server->max_users) {
            $countServerClients = 'تکمیل';
        } else {
            $countServerClients = $server->max_users - (int)$countServerClients;
        }
        $hasCDN = false;
        if (isset($server->json_data['cdn'])) {
            if ($server->json_data['cdn']) {
                $hasCDN = true;
            }
        }
        $parentMultiUserPackage = checkIfServerIsAllowedByAnyPackageToBeMultiUser($server, true);
        $serverStatus = getServerStatus($serverId, true);
        return response()->json([
            'code' => 200,
            'success' => false,
            'message' => 'Successfully retrieved',
            'body' => [
                'serverName' => $server->server_name,
                'serverId' => $serverId,
                'serverClients' => $countServerClients,
                'serverStatus' => $serverStatus,
                'has_cdn' => $hasCDN,
                'allowed_for_multi_user' => (!$parentMultiUserPackage ? false : implode(',', $parentMultiUserPackage)),
                'is_pritunl' => checkIfServerIsPritunl($server)
            ]
        ]);
    }

    public function getServerClients($serverId)
    {
        $server = Servers::findOrFail($serverId);
        $list = (new \App\Actions\Servers\Servers())->getServerClients($serverId);
        return response()->json([
            'code' => 200,
            'success' => false,
            'message' => 'Successfully retrieved',
            'body' => [
                'serverName' => $server->server_name,
                'serverId' => $serverId,
                'clients' => $list
            ]
        ]);
    }

    public function syncAccounts($id)
    {
        $serverAPI = new ServersServers();
        $server = Servers::findOrFail($id);
        $accounts = getClientsOnServer($server->id);
        if (!$accounts) {
            smilify('error', 'خطا در ارتباط با سرور');
            return redirect()->back();
        }
        if (count($accounts) <= 0) {
            smilify('error', 'اکانتی روی سرور یافت نشد');
            return redirect()->back();
        }
        foreach ($accounts as $account) {
            $checkIfExistsOnDatabase = Accounts::where('account_id', $account['remark'])->first();
            if (is_null($checkIfExistsOnDatabase)) {
                $isCDN = false;
                if ($account['listen'] == '127.0.0.1') {
                    $isCDN = true;
                }
                $newAccountDetail = $account;
                $accountID = Accounts::create([
                    'user_id' => auth()->user()->id,
                    'account_name' => $account['remark'],
                    'server_id' => $server->id,
                    'package_id' => null,
                    'account_id' => 0,
                    'json_data' => [
                        'is_cdn' => $isCDN
                    ]
                ])->id;
                $newAccountDetail['remark'] = 'user,' . auth()->user()->id . ',' . $accountID;
                $editResponse = $serverAPI->editAccount($server->id, $newAccountDetail, $account['id']);
                Accounts::where('id', $accountID)->update([
                    'account_id' => $newAccountDetail['remark']
                ]);
            }
        }
        smilify('success', 'همگام سازی با موفقیت انجام شد');
        return redirect()->back();
    }

    public function moveAccounts($id)
    {
        $accountsWithChangedPort = false;
        $destination_server = false;
        $server = Servers::findOrFail($id);
        $servers = Servers::all();
        $pageTitle = 'انتقال کاربران سرور';
        return view('paper.pages.servers.move-accounts', compact('pageTitle', 'server', 'servers', 'accountsWithChangedPort', 'destination_server'));
    }

    public function doMoveAccounts($id, Request $request)
    {
        $request->validate([
            'destination_server_id' => 'required|numeric',
        ]);
        $removeOnOrigin = false;
        if (isset($request->remove_on_origin)) {
            $removeOnOrigin = true;
        }
        $origin_server = Servers::findOrFail($id);
        $destination_server = Servers::findOrFail($request->destination_server_id);

        $origin_server_accounts = getClientsOnServer($origin_server->id);
        $destination_server_accounts = getClientsOnServer($destination_server->id);
        if (is_bool($origin_server_accounts)) {
            smilify('error', 'خطا در ارتباط با سرور مبدا');
            return redirect()->back();
        }
        if (is_bool($destination_server_accounts)) {
            smilify('error', 'خطا در ارتباط با سرور مقصد');
            return redirect()->back();
        }
        $accountsWithChangedPort = [];
        foreach ($origin_server_accounts as $account) {
            $remark = explode(',', $account['remark']);
            $dbAccount = Accounts::where('id', $remark[2])->first();
            if (!is_null($dbAccount)) {
                $accountOriginID = $account['id'];
                unset($account['id']);
                foreach ($destination_server_accounts as $dacc) {
                    if ($dacc['port'] == $account['port']) {
                        $newport = rand(10000, 65535);
                        $accountsWithChangedPort[] = [
                            'previous' => $account,
                            'new_port' => $newport,
                            'ondb' => $dbAccount
                        ];
                        $account['port'] = $newport;
                    }
                }
                $response = (new ServersServers)->createAccount($destination_server->id, $account);
                if (!$response) {
                    smilify('error', 'خطا در ارتباط با سرور مقصد');
                    return redirect()->back();
                }
                $dbAccount->server_id = $destination_server->id;
                $dbAccount->save();
                if ($removeOnOrigin) {
                    $response = (new ServersServers)->deleteAccount($origin_server->id, $accountOriginID);
                }
            }
        }
        smilify('success', 'انتقال با موفقیت انجام شد');
        $server = $origin_server;
        $servers = Servers::all();
        $pageTitle = 'انتقال کاربران سرور';
        return view('paper.pages.servers.move-accounts', compact('pageTitle', 'server', 'servers', 'destination_server', 'accountsWithChangedPort'));
        return redirect()->back();
    }

    public function changeConfigs($id)
    {
        $server = Servers::findOrFail($id);
        $pageTitle = 'تغییر کانفیگ همه کاربران روی سرور';
        return view('paper.pages.servers.change-configs', compact('pageTitle', 'server'));
    }

    public function doChangeConfigs($id, Request $request)
    {
        $request->validate([
            'protocol' => ['required', Rule::in((new AccountController())->allowedProtocols)]
        ]);
        $server = Servers::findOrFail($id);
        $clients = getClientsOnServer($server->id);
        if (is_bool($clients)) {
            smilify('error', 'خطا در ارتباط با سرور');
            return redirect()->back();
        }
        foreach ($clients as $client) {
            $client = generateClientConfigurationByRequest($request, $client);
            if (!$client) {
                smilify('error', 'خطا در ایجاد کانفیگ');
                return redirect()->back();
            }
            $response = (new \App\Actions\Servers\Servers)->editAccount($server->id, $client, $client['id']);
        }
        smilify('success', 'کانفیگ با موفقیت انجام شد');
        return redirect()->back();
    }

    public function addExtraDomainOrIp($id, Request $request)
    {
        $request->validate([
            'extra_domain_or_ip' => 'required'
        ]);
        $server = Servers::findOrFail($id);
        $jsonData = $server->json_data;
        $jsonData['extra_domain_or_ip'][] = $request->extra_domain_or_ip;
        $server->json_data = $jsonData;
        $server->save();
        smilify('success', 'دامنه/ایپی با موفقیت اضافه شد');
        return redirect()->back();
    }

    public function deleteExtraDomainOrIp($id, $itemId)
    {
        $server = Servers::findOrFail($id);
        $jsonData = $server->json_data;
        $jsonData['extra_domain_or_ip'] = array_values($jsonData['extra_domain_or_ip']);
        unset($jsonData['extra_domain_or_ip'][$itemId]);
        $server->json_data = $jsonData;
        $server->save();
        smilify('success', 'دامنه/ایپی با موفقیت حذف شد');
        return redirect()->back();
    }

    public function botMonitoring()
    {
        $pageTitle = 'تنظیمات روبات مانیتورینگ سرور';
        $servers = Servers::all();
        return view('paper.pages.servers.monitoring-bot.settings', compact('pageTitle', 'servers'));
    }

    public function saveBotMonitoring(Request $request)
    {
        $request->validate([
            'robot_token' => 'required',
            'first_check_host_name' => 'required',
            'admin_telegram_number_id' => 'required',
            'retry_attempts' => 'required|numeric|min:1|max:10',
            'wait_for_check_result' => 'required|numeric|min:10|max:120'
        ]);
        set_static_option('monitorbot_robot_token', $request->robot_token);
        set_static_option('monitorbot_first_check_host_name', $request->first_check_host_name);
        set_static_option('monitorbot_admin_telegram_number_id', $request->admin_telegram_number_id);
        set_static_option('monitorbot_second_check_host_name', (!is_null($request->second_check_host_name) ? $request->second_check_host_name : false));
        set_static_option('monitorbot_bot_status', (isset($request->bot_status) ? true : false));
        set_static_option('monitorbot_retry_attempts', $request->retry_attempts);
        set_static_option('monitorbot_wait_for_check_result', $request->wait_for_check_result);
        if (is_array($request->allowed_servers)) {
            set_static_option('monitorbot_allowed_servers', implode(',', $request->allowed_servers));
        } else {
            set_static_option('monitorbot_allowed_servers', false);
        }
        smilify('success', 'تنظیمات روبات با موفقیت ذخیره شد');
        return redirect()->back();
    }

    public function moreIps()
    {
        $pageTitle = 'تنظیمات ایپی اضافه';
        $servers = Servers::all();
        $additionalIPS = AdditionalIPS::with('server')->paginate(15);
        return view('paper.pages.servers.monitoring-bot.more-ips', compact('pageTitle', 'servers', 'additionalIPS'));
    }

    public function doAddMoreIps(Request $request)
    {
        $request->validate([
            'ip_address' => 'required',
            'provider' => 'required',
            'server_id' => 'required'
        ]);
        AdditionalIPS::create([
            'ip_address' => $request->ip_address,
            'status' => 0,
            'provider' => $request->provider,
            'server_id' => $request->server_id
        ]);
        smilify('success', 'ایپی جدید با موفقیت اضافه شد');
        return redirect()->back();
    }

    public function deleteMoreIps($id)
    {
        AdditionalIPS::where('id', $id)->delete();
        smilify('success', 'ایپی با موفقیت حذف شد');
        return redirect()->back();
    }

    //PriTunl Servers
    public function addPritunlServer()
    {
        $pageTitle = 'افزودن سرور OpenVPN';
        return view('paper.pages.servers.pritunl.add', compact('pageTitle'));
    }

    public function addPritunlServerSecondStep(Request $request)
    {
        $request->validate([
            'server_name' => 'required',
            'server_domain' => 'required',
            'api_token' => 'required',
            'api_secret' => 'required',
            'max_users' => 'required'
        ]);
        $pritunlHandler = new PritunlWebService($request->api_token, $request->api_secret, $request->server_domain);
        $response = $pritunlHandler->getOrganizations();
        $servers = $pritunlHandler->getServers();
        if (!$response) {
            smilify('error', 'خطا در ارتباط با سرور - لطفا مشخصات وارد شده را مجددا بررسی کنید');
            return redirect()->back();
        }
        $serverData = $request;
        $pageTitle = 'مرحله دوم افزودن سرور';
        return view('paper.pages.servers.pritunl.add-second-step', compact('pageTitle', 'response', 'serverData', 'servers'));
    }

    public function addPritunlServerFinalStep(Request $request)
    {
        $request->validate([
            'server_name' => 'required',
            'server_domain' => 'required',
            'api_token' => 'required',
            'api_secret' => 'required',
            'max_users' => 'required',
            'organization_id' => 'required',
            'server_id' => 'required'
        ]);

        Servers::create([
            'hostname' => "null",
            'iran_ip' => "null",
            'panel_port' => "null",
            'panel_cookie' => "null",
            'server_name' => $request->server_name,
            'max_users' => $request->max_users,
            'server_provider_website' => "null",
            'tunnel_ip' => $request->server_domain,
            'eu_ip' => $request->server_domain,
            'tunnel_cookie' => "null",
            'json_data' => [
                'keys' => [
                    'public_key' => "null",
                    'private_key' => "null"
                ],
                'cdn' => false,
                'cloudflare' => false,
                'type' => 'pritunl',
                'pritunl_data' => [
                    'api_token' => $request->api_token,
                    'api_secret' => $request->api_secret,
                    'organization_id' => $request->organization_id,
                    'server_id' => $request->server_id
                ]
            ]
        ]);
        smilify('success', 'سرور جدید ایجاد شد');
        return redirect()->route('pritunl.servers.add');
    }

}
