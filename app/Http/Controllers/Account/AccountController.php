<?php

namespace App\Http\Controllers\Account;

use App\Actions\Api\PritunlWebService;
use App\Actions\Servers\Servers;
use App\Http\Controllers\Controller;
use App\Http\Controllers\TransactionsController;
use App\Models\Accounts;
use App\Models\Package;
use App\Models\Servers as ModelsServers;
use App\Models\Transactions;
use App\Models\User;
use App\Notifications\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Http;

class AccountController extends Controller
{
    public $allowedProtocols = [
        'vmess',
        'vless'
    ];

    public function add()
    {
        $pageTitle = 'ایجاد حساب جدید';
        $packages = Package::where('status', 1)->where('price_level', auth()->user()->price_level)->get();
        return view('paper.pages.accounts.add', compact('pageTitle', 'packages'));
    }

    public function list()
    {
        $pageTitle = 'لیست حساب ها';
        $dbAccounts = Accounts::where('user_id', auth()->user()->id)->with('servers')->orderBy('created_at', 'DESC')->paginate(10);
        $accounts = $dbAccounts;
        return view('paper.pages.accounts.list', compact('pageTitle', 'accounts', 'dbAccounts'));
    }

    public function changeProtocol($id, $isMultiUser = false)
    {
        if (auth()->user()->hasRole('admin')) {
            $account = Accounts::where('id', $id)->first();
        } else {
            $account = Accounts::where('id', $id)->where('user_id', auth()->user()->id)->first();
        }
        $pageTitle = 'ویرایش پروتکل سرویس کاربری';
        if (isset($account->json_data->multi_user)) {
            if ($account->json_data->multi_user) {
                $isMultiUser = true;
            } else {
                $isMultiUser = false;
            }
        } else {
            $isMultiUser = false;
        }
//        if ($isMultiUser) {
//            $accountDetails = getUserAccountDetail($account);
//        } else {
//            $accountDetails = getUserAccountDetail($account);
//        }
//        if (!$accountDetails) {
//            smilify('error', 'پروتکل این اکانت قبلا تغییر کرده');
//            return redirect()->back();
//        }

        return view('paper.pages.accounts.change-protocol', compact('pageTitle', 'account', 'isMultiUser'));
    }

    public function doChangeProtocol($id, $isMultiUser = false, Request $request)
    {
        $request->validate([
            'server_id' => ['required']
        ]);
        if (auth()->user()->hasRole('admin')) {
            $account = Accounts::where('id', $id)->first();
        } else {
            $account = Accounts::where('id', $id)->where('user_id', auth()->user()->id)->first();
        }
//        if ($account->server_id == $request->server_id) {
//            smilify('error', 'سرویس درحال حاظر روی همین سرور است.');
////            return redirect()->back();
//        }
        if (isset($account->json_data->multi_user)) {
            if ($account->json_data->multi_user) {
                $isMultiUser = true;
            } else {
                $isMultiUser = false;
            }
        } else {
            $isMultiUser = false;
        }
        if ($isMultiUser) {
            $accountDetails = multiUserGetAccountDetailsByEmail($account->json_data->multi_user->email, $account->server_id, $account->package_id);
        } else {
            $accountDetails = getUserAccountDetail($account);
        }

        if (!$accountDetails) {
            smilify('error', 'امکان تغییر پروتکل این سرویس وجود ندارد');
//            return redirect()->back();
        }
        $package = Package::where('id', $account->package_id)->first();
        if (is_null($package)) {
            smilify('error', 'امکان تغییر پروتکل این سرویس وجود ندارد');
//            return redirect()->back();
        }

//        } else {
//            $response = $this->changeProtocolToOldServer($account->id, $account->user_id, $request->server_id, $package, $accountDetails);
//        }
        $response = $this->changeProtocolToWmessTcp($account->id, $account->user_id, $request->server_id, $package, $accountDetails);
        dd(getUserAccountDetailsByRemark($account->account_id, 17));
        if (!$response) {
            smilify('error', 'پکیج با سرور انتخاب شده مطابقت ندارد. برای اطلاعات بیشتر با مدیر وبسایت در ارتباط باشید.');
//            return redirect()->back();
        }
        if (!$isMultiUser) {
            $accountDetails['enable'] = false;
            $editAccount = (new Servers)->editAccount($account->server_id, $accountDetails, $accountDetails['id']);
        }
        smilify('success', 'پروتکل سرویس کاربری با موفقیت ویرایش شد');
//        return redirect()->route('accounts.show.edit.results', $account->id);
    }

    public function showResultOfEdit($id)
    {
        if (auth()->user()->hasRole('admin')) {
            $dbAccounts = Accounts::where('id', $id)->paginate(5);
        } else {
            $dbAccounts = Accounts::where('id', $id)->where('user_id', auth()->user()->id)->paginate(10);
        }
        $accounts = $dbAccounts;
        $pageTitle = $accounts[0]->account_name;
        return view('paper.pages.accounts.list', compact('pageTitle', 'accounts', 'dbAccounts'));
    }

    public function search(Request $request)
    {
        $request->validate([
            'term' => 'required'
        ]);
        $term = $request->term;
        if (auth()->user()->hasRole('admin')) {
            $dbAccounts = Accounts::where('account_name', 'LIKE', '%' . $term . '%')->paginate(10);
        } else {
            $dbAccounts = Accounts::where('account_name', 'LIKE', '%' . $term . '%')->where('user_id', auth()->user()->id)->paginate(5);
        }
        $accounts = $dbAccounts;
        $pageTitle = $request->term;
        return view('paper.pages.accounts.list', compact('pageTitle', 'accounts', 'dbAccounts'));
    }

    public function adminList()
    {
        $pageTitle = 'لیست حساب ها';
        $dbAccounts = Accounts::with(['servers', 'user'])->orderBy('created_at', 'DESC')->paginate(10);
        $accounts = $dbAccounts;
        return view('paper.pages.accounts.list', compact('pageTitle', 'accounts', 'dbAccounts'));
    }

    public function adminEdit($id)
    {
        $account = Accounts::with('user', 'servers', 'transaction')->findOrFail($id);
        $servers = ModelsServers::all();
        $pageTitle = 'ویرایش سرویس کاربری';
        return view('paper.pages.accounts.edit', compact('pageTitle', 'account', 'servers'));
    }

    public function adminDoEdit($id, Request $request)
    {
        $account = Accounts::findOrFail($id);
        if ($account->user_id != $request->user_id) {
            $accountDetail = getUserAccountDetail($account);
            if (!$accountDetail) {
                smilify('error', 'سرویس در سرور یافت نشد');
                return redirect()->back();
            }
            $accountDetail['remark'] = 'user,' . $request->user_id . ',' . $account->id;
            $response = (new Servers)->editAccount($account->server_id, $accountDetail, $accountDetail['id']);
            if ($response && $response->success) {
                // $transaction = Transactions::find($account->transactions_id);
                $newOwner = User::find($request->user_id);
                $oldOwner = User::find($account->user_id);
                // $oldOwnerPricePerAccount = ($oldOwner->price_per_account != null ? $oldOwner->price_per_account : get_static_option('default_account_price'));
                // (new TransactionsController)->addToUserWallet($oldOwner->id, $oldOwnerPricePerAccount);
                // Transactions::create([
                //     'user_id' => $oldOwner->id,
                //     'type' => 'income',
                //     'amount' => $oldOwnerPricePerAccount,
                //     'transaction_code' => Hash::make(now()),
                //     'description' => 'بابت انتقال سرویس ' . $account->account_name . ' به حساب ' . $newOwner->name
                // ]);
                // $newOwnerPricePerAccount = ($newOwner->price_per_account != null ? $newOwner->price_per_account : get_static_option('default_account_price'));
                // (new TransactionsController)->takeFromUserWallet($newOwner->id, $newOwnerPricePerAccount);
                // Transactions::create([
                //     'user_id' => $newOwner->id,
                //     'type' => 'outgoing',
                //     'amount' => $newOwnerPricePerAccount,
                //     'transaction_code' => Hash::make(now()),
                //     'description' => 'بابت انتقال سرویس ' . $account->account_name . ' از حساب ' . $oldOwner->name . ' به حساب شما ',
                // ]);
                // $transaction->user_id = $newOwner->id;
                // $transaction->amount = $newOwnerPricePerAccount;
                // $transaction->save();
                $account->user_id = $newOwner->id;
            } else {
                smilify('error', 'خطا در ارتباط با سرور');
                return redirect()->back();
            }
        }
        if ($account->server_id != $request->server_id) {
            $accountDetail = getUserAccountDetail($account);
            if (!$accountDetail) {
                smilify('error', 'سرویس در سرور یافت نشد');
                return redirect()->back();
            }
            $serverAccountId = $accountDetail['id'];
            unset($accountDetail['id']);
            $accountDetail['port'] = rand(10000, 65535);
            $response = (new Servers)->createAccount($request->server_id, $accountDetail);
            if ($response && $response->success) {
                $response = (new Servers)->deleteAccount($account->server_id, $serverAccountId);
                if ($response && $response->success) {
                    $account->server_id = $request->server_id;
                } else {
                    smilify('error', 'خطا در ارتباط با سرور مبدا');
                    return redirect()->back();
                }
            } else {
                smilify('error', 'خطا در ارتباط با سرور مقصد');
                return redirect()->back();
            }
        }
        $account->account_name = $request->account_name;
        $account->save();
        smilify('success', 'سرویس کاربری با موفقیت ویرایش شد');
        return redirect()->route('admin.accounts.list');
    }

    public function adminDoDelete($id, $noHttpReturn = false)
    {
        $account = Accounts::with('servers')->findOrFail($id);
        if (!checkIfAccountIsPritunl($account)) {
            $serverID = $account->server_id;
            if (isset($account->json_data->multi_user)) {
                if ($account->json_data->multi_user) {
                    if ($account->json_data->multi_user) {
                        $account = multiUserGetAccountDetailsByEmail($account->json_data->multi_user->email, $account->server_id, $account->package_id);
                        if (!$account) {
                            Accounts::where('id', $id)->delete();
                            smilify('success', 'سرویس کاربری با موفقیت حذف شد');
                            return redirect()->back();
                        }
                        $users = json_decode($account['master_account']['settings'], true);
                        foreach ($users['clients'] as $key => $user) {
                            if ($user['id'] == $account['config']['id']) {
                                $users['clients'][$key]['expiryTime'] = 10000;
                            }
                        }
                        $account['master_account']['settings'] = json_encode($users);
                        $response = (new Servers())->editAccount($serverID, $account['master_account'], $account['master_account']['id']);
                        Accounts::where('id', $id)->delete();
                        $reseller = User::find($account->user_id);
                        $reseller->notify(new UserNotification(
                            'success',
                            'trash-o',
                            ' سرویس کاربری با نام  ' . $account->account_name . ' حذف شد '
                        ));
                        smilify('success', 'سرویس کاربری با موفقیت حذف شد');
                        return redirect()->back();
                    }
                }
            }
            //Check if account type is multi_user. delete it in another way...
            try {
                $accountDetail = getUserAccountDetail($account);
            } catch (\Exception $e) {
                $accountDetail = false;
            }
            if (!$accountDetail) {
                if (!$noHttpReturn) {
                    smilify('error', 'سرویس در سرور یافت نشد');
                    Accounts::where('id', $id)->delete();
                    smilify('success', 'سرویس کاربری با موفقیت حذف شد');
                    return redirect()->back();
//                return redirect()->back();
                } else {
                    return false;
                }
            }
            $accountDetail['enable'] = false;
            $response = (new Servers)->editAccount($account->server_id, $accountDetail, $accountDetail['id']);
            if ($response && $response->success) {
                if (!$noHttpReturn) {
                    Accounts::where('id', $id)->delete();
                    $reseller = User::find($account->user_id);
                    $reseller->notify(new UserNotification(
                        'success',
                        'trash-o',
                        ' سرویس کاربری با نام  ' . $account->account_name . ' حذف شد '
                    ));
                    smilify('success', 'سرویس کاربری با موفقیت حذف شد');
                    return redirect()->back();
                }
                return true;
            } else {
                if (!$noHttpReturn) {
                    smilify('error', 'خطا در ارتباط با سرور');
                    return redirect()->back();
                }
                return false;
            }
        } else {
            $server = $account->servers;
            $pritunlHandler = new PritunlWebService($server->json_data['pritunl_data']['api_token'], $server->json_data['pritunl_data']['api_secret'], $server->tunnel_ip);
            $response = $pritunlHandler->deleteAccount($server->json_data['pritunl_data']['organization_id'], $account->account_id);
            Accounts::where('id', $id)->delete();
            $reseller = User::find($account->user_id);
            $reseller->notify(new UserNotification(
                'success',
                'trash-o',
                ' سرویس کاربری با نام  ' . $account->account_name . ' حذف شد '
            ));
            smilify('success', 'سرویس کاربری با موفقیت حذف شد');
            return redirect()->back();
        }
    }

    public function doAdd(Request $request)
    {
        $request->validate([
            'server_id' => 'required|numeric',
            'account_name' => 'required|max:1024',
            'package_id' => 'required|numeric',
            'account_gig' => 'required|numeric',
        ]);
        $server = ModelsServers::where('id', $request->server_id)->first();
        $package = Package::where('id', $request->package_id)->where('status', 1)->where('price_level', auth()->user()->price_level)->first();
        if (is_null($server)) {
            smilify('error', 'سرور یافت نشد');
            return redirect()->back();
        }
        if (is_null($package)) {
            smilify('error', 'پکیج یافت نشد');
            return redirect()->back();
        }
        if (!checkIfServerIsPritunl($server)) {
            $countServerClients = countServerClients($server->id);
        } else {
            $countServerClients = countServerClients($server->id, false, 'pritunl', $server);
        }
        if ($countServerClients >= $server->max_users) {
            smilify('error', 'ظرفیت این سرور تکمیل است');
            return redirect()->back();
        }
        if (!checkIfServerIsPritunl($server)) {
            $multiUser = false;
            if (isset($package->protocol['multi_user'])) {
                if ($package->protocol['multi_user']) {
                    $multiUser = checkIfServerIsAllowedForPackageToBeMultiUser($server, $package);
                    if (!$multiUser) {
                        smilify('error', 'پکیج با سرور انتخاب شده مطابقت ندارد. برای اطلاعات بیشتر با مدیر وبسایت در ارتباط باشید.');
                        return redirect()->back();
                    }
                }
            }
        }
        if (checkIfServerIsPritunl($server)) {
            if (!checkIfPackageIsPritunl($package)) {
                smilify('error', 'پکیج با سرور انتخاب شده مطابقت ندارد. برای اطلاعات بیشتر با مدیر وبسایت در ارتباط باشید.');
                return redirect()->back();
            }
        }
        $price = 0;
        $gig = $request->account_gig;
        if ($gig < 100) {
            $price = $gig * 1750;
        } else if ($gig < 200){
            $price = $gig * 1650;
        } else {
            $price = $gig * 1550;
        }

        if ($gig == 0) {
            return response()->json([
                'code' => 403,
                'success' => false,
                'message' => 'ساخت سرویس با حجم نامحدود ممکن نیست',
                'body' => []
            ]);
        }



        if (auth()->user()->wallet < $price) {
            smilify('error', 'اعتبار حساب شما کافی نیست');
            return redirect()->back();
        }
        if (!checkIfServerIsPritunl($server)) {
            $hasCDN = false;
            if (isset($request->make_as_cdn)) {
                if (checkIfServerHasCDNSettings($server)) {
                    $hasCDN = true;
                }
            }
            $account = Accounts::create([
                'user_id' => auth()->user()->id,
                'account_name' => $request->account_name,
                'server_id' => $server->id,
                'package_id' => $package->id,
                'account_id' => 0,
                'json_data' => [
                    'is_cdn' => $hasCDN,
                    'is_multi_user' => (!$multiUser ? false : true)
                ]
            ])->id;


            $response = $this->createAccountOnServer($account, auth()->user()->id, $request->server_id, $package, $server, false, false, false, false, $hasCDN, $multiUser, $request->account_gig);
            if ($response) {
                if ($response->success) {
                    $transactionsId = Transactions::create([
                        'user_id' => auth()->user()->id,
                        'type' => 'outgoing',
                        'amount' => $price,
                        'transaction_code' => Hash::make(now()),
                        'description' => 'خرید حساب کاربری با نام ' . $request->account_name
                    ])->id;
                    Accounts::where('id', $account)->update([
                        'account_id' => 'user,' . auth()->user()->id . ',' . $account,
                        'transactions_id' => $transactionsId,
                    ]);
                    (new TransactionsController)->takeFromUserWallet(auth()->user()->id, $price);

//                    $userAccountServersTake = DB::table("accounts")->select("*")->where("user_id", auth()->user()->id)->dump();
//
//                    foreach ($userAccountServersTake as $time){
//                        dd($time->created_at);
//                    }

                    smilify('success', 'حساب کاربری با موفقیت ایجاد شد');

                    return redirect()->route('accounts.list');
                } else {

                    smilify('error', 'خطا در ایجاد حساب کاربری مجددا امتحان کنیدیییی');
                    return redirect()->back();
                    Accounts::where('id', $account)->delete();
                }
            } else {
                smilify('error', 'خطا در ایجاد حساب کاربری مجددا امتحان کنید');
                Accounts::where('id', $account)->delete();
                return redirect()->back();
            }
        } else {
            $pritunlHandler = new PritunlWebService($server->json_data['pritunl_data']['api_token'], $server->json_data['pritunl_data']['api_secret'], $server->tunnel_ip);
            $response = $pritunlHandler->createAccountUsingBulk([[
                'email' => null,
                'name' => $request->account_name
            ]], $server->json_data['pritunl_data']['organization_id']);
            if ($response == false) {
                smilify('error', 'خطا در ایجاد حساب کاربری مجددا امتحان کنید');
                return redirect()->back();
                Accounts::where('id', $account)->delete();
            }
            $transactionsId = Transactions::create([
                'user_id' => auth()->user()->id,
                'type' => 'outgoing',
                'amount' => $price,
                'transaction_code' => Hash::make(now()),
                'description' => 'خرید حساب کاربری با نام ' . $request->account_name
            ])->id;
            Accounts::create([
                'user_id' => auth()->user()->id,
                'account_name' => $request->account_name,
                'server_id' => $server->id,
                'package_id' => $package->id,
                'account_id' => $response[0]['id'],
                'json_data' => [
                    'is_cdn' => false,
                    'is_multi_user' => false,
                    'type' => 'pritunl',
                    'pritunl_data' => $response[0],
                    'reality_scurity' => false
                ]
            ]);
            (new TransactionsController)->takeFromUserWallet(auth()->user()->id, $price);
            smilify('success', 'حساب کاربری با موفقیت ایجاد شد');
            return redirect()->route('accounts.list');
        }

    }

    public function createAccountOnServer($accountId, $userId, $server_id, $package, $server, $customDetails = false, $customExpireDate = false, $customPort = false, $uuid = false, $hasCDN = false, $multiUser = false, $account_gig = 0)
    {



        $port = ($customPort ? $customPort : rand(10000, 65535));
        $remark = 'user,' . $userId . ',' . $accountId;
        $hasReality = $package->protocol['reality_security'];
        if ($multiUser) {
            $masterAccountDetails = getUserAccountDetailsById($multiUser['master_account'], $server_id);
            if (!$masterAccountDetails) {
                return false;
            }
            $serviceRandomEmail = md5(uniqid(rand(1000, 9999), true)) . '@gmail.com';
            $uuid = md5(now());
            $totalGB = ($account_gig * (1024 * 1024 * 1024));
            $expiryTime = (strtotime(date('Y-m-d', strtotime("+" . $package->expire_at . " days"))) * 1000);

            if ($hasReality) {

                $newClient = [
                    'id' => $uuid,
                    'alterId' => 0,
                    'fingerprint' => 'chrome',
                    'email' => $serviceRandomEmail,
                    'limitIp' => 0,
                    'total' => $totalGB,
                    'expiryTime' => $expiryTime,
                    'flow' => 'xtls-rprx-vision'

                ];
            } else {

                $newClient = [

                    'id' => $uuid,
                    'alterId' => 0,
                    'email' => $serviceRandomEmail,
                    'limitIp' => 0,
                    'totalGB' => $totalGB,
                    'expiryTime' => $expiryTime

                ];
            }
            $clientSettings = json_decode($masterAccountDetails['settings'], true);
            $clientSettings = $clientSettings['clients'];
            $clientSettings[] = $newClient;
            if ($hasReality) {
                $masterAccountDetails['settings'] = json_encode([
                    'clients' => $clientSettings,
                    'decryption' => 'none',
                    'fallbacks' => []
                ]);
            } else {
                $masterAccountDetails['settings'] = json_encode([
                    'clients' => $clientSettings,
                    'disableInsecureEncryption' => false
                ]);
            }
            $localDBAccount = Accounts::where('id', $accountId)->first();
            $accountJsonData = (array)$localDBAccount->json_data;
            $accountJsonData['multi_user'] = $newClient;
            $localDBAccount->json_data = $accountJsonData;
            $localDBAccount->save();
            $response = (new Servers)->editAccount($server_id, $masterAccountDetails, $masterAccountDetails['id']);
            return $response;
        }
        if (!$customDetails) {
            $data = $this->defaultAccountSettings($port, $remark, $package, $server, false, $customExpireDate, $uuid, $hasCDN, $hasReality, $account_gig);
        } else {
            $data = $this->defaultAccountSettings($port, $remark, $package, $server, $customDetails, $customExpireDate, $uuid, $hasCDN, $hasReality, $account_gig);
        }
        $response = (new Servers)->createAccount($server_id, $data);

        return $response;
    }

    public function changeProtocolToOldServer($accountId, $userId, $server_id, $package, $customDetails)
    {
        $server = \App\Models\Servers::where('id', $server_id)->first();
        if (is_null($server)) {
            smilify('error', 'سرور انتخاب شده یافت نشد');
            return redirect()->back();
        }
        $accountOnDB = Accounts::where('id', $accountId)->first();
        if (is_null($accountOnDB)) {
            smilify('error', 'سرویس کاربری انتخاب شده یافت نشد');
            return redirect()->back();
        }
        $account = multiUserGetAccountDetailsByEmail($accountOnDB->json_data->multi_user->email, $accountOnDB->server_id, $accountOnDB->package_id);
        if (!$account) {
            return false;
        }
        $masterAccountClients = json_decode($account['master_account']['settings'], true);
        foreach ($masterAccountClients['clients'] as $key => $item) {
            if ($item['id'] == $accountOnDB->json_data->multi_user->id) {
                $masterAccountClients['clients'][$key]['expiryTime'] = 10000;
            }
        }
        $account['master_account']['settings'] = json_encode($masterAccountClients);
        $response = (new Servers)->editAccount($accountOnDB->server_id, $account['master_account'], $account['master_account']['id']);
        $account['config']['up'] = $account['stats']['up'];
        $account['config']['down'] = $account['stats']['down'];
        $account['config']['total'] = $account['config']['totalGB'];
        $response = $this->createAccountOnServer($accountId, auth()->user()->id, $server_id, $package, $server, $account['config'], Carbon::createFromTimestamp($account['config']['expiryTime']), false, false);
        $accountOnDB->server_id = $server->id;
        $jsonData = $accountOnDB->json_data;
        $jsonData->multi_user = false;
        $jsonData->is_multi_user = false;
        $accountOnDB->json_data = $jsonData;
        $accountOnDB->save();
        return $response;
    }

    public function changeProtocolToNewServer($accountId, $userId, $server_id, $package, $customDetails)
    {
        $server = \App\Models\Servers::where('id', $server_id)->first();
        if (is_null($server)) {
            smilify('error', 'سرور انتخاب شده یافت نشد');
            return redirect()->back();
        }
        $multiUser = false;
        if (!isset($package->protocol['master_account'])) {
            if (!isset($package->protocol['in_sync_package_id'])) {
                return false;
            }
            if ($package->protocol['in_sync_package_id']) {
                $inSyncPackage = Package::where('id', $package->protocol['in_sync_package_id'])->first();
                if (is_null($inSyncPackage)) {
                    return false;
                }
                if ($inSyncPackage->protocol['multi_user']) {
                    $multiUser = checkIfServerIsAllowedForPackageToBeMultiUser($server, $inSyncPackage);
                    if (!$multiUser) {
                        return false;
                    }
                }
            }
        } else {
            if ($package->protocol['multi_user']) {
                $multiUser = checkIfServerIsAllowedForPackageToBeMultiUser($server, $package);
                if (!$multiUser) {
                    return false;
                }
            }
        }

        $masterAccountDetails = getUserAccountDetailsById($multiUser['master_account'], $server_id);
        if (!$masterAccountDetails) {
            return false;
        }
        $serviceRandomEmail = md5(uniqid(rand(), true)) . '@gmail.com';
        $uuid = md5(now());
        $totalGB = ($package->bandwidth * (1024 * 1024 * 1024));
        if ($totalGB != 0) {
            $totalGB = $totalGB - ($customDetails['up'] + $customDetails['down']);
        }
        $expiryTime = (strtotime(date('Y-m-d', strtotime("+" . $package->expire_at . " days"))) * 1000);
        $newClient = [
            'id' => $uuid,
            'alterId' => 0,
            'email' => $serviceRandomEmail,
            'limitIp' => 0,
            'totalGB' => $totalGB,
            'expiryTime' => $customDetails['expiryTime'],
        ];
        $clientSettings = json_decode($masterAccountDetails['settings'], true);
        $clientSettings = $clientSettings['clients'];
        $clientSettings[] = $newClient;
        $masterAccountDetails['settings'] = json_encode([
            'clients' => $clientSettings,
            'disableInsecureEncryption' => false
        ]);
//        dd($newClient,$masterAccountDetails['settings']);
        dd($newClient, $masterAccountDetails['settings']);

        $localDBAccount = Accounts::where('id', $accountId)->first();
        $accountJsonData = (array)$localDBAccount->json_data;
        $accountJsonData['multi_user'] = $newClient;
        $accountJsonData['is_multi_user'] = true;
        $localDBAccount->json_data = $accountJsonData;
        $localDBAccount->server_id = $server_id;
        $localDBAccount->save();
        $response = (new Servers)->editAccount($server_id, $masterAccountDetails, $masterAccountDetails['id']);
//        sleep(3);
//        $masterAccountDetails = getUserAccountDetailsById($multiUser['master_account'], $server_id);
//        $subAccounts = $masterAccountDetails['clientStats'];
//        foreach ($subAccounts as $key => $item) {
//            if ($item['email'] == $newClient['email']) {
//                $masterAccountDetails['clientStats'][$key]['up'] = $customDetails['up'];
//                $masterAccountDetails['clientStats'][$key]['down'] = $customDetails['down'];
//            }
//        }
//        $response2 = (new Servers)->editAccount($server_id, $masterAccountDetails, $masterAccountDetails['id']);
//        dd($response2);
        return $response;
    }

    public function defaultAccountSettings($port, $remark, $package, $server, $customDetails = false, $customExpireDate = false, $customUuid = false, $hasCDN, $hasReality, $account_gig)
    {
        if (!$customExpireDate) {
            $date = date('Y-m-d', strtotime("+" . $package->expire_at . " days"));
        } else {
            $diffInDays = Carbon::parse($customExpireDate->format('Y-m-d'))->diffInDays(date('Y-m-d', time()));
            $date = date('Y-m-d', strtotime("+" . ($package->expire_at - $diffInDays) . " days"));
        }
        $uuid = ($customUuid ? $customUuid : md5(now()));
        $protocolSwitch = $package->protocol['protocol'];
        $totalBand = ($customDetails ? $customDetails['total'] : ($account_gig * (1024 * 1024 * 1024)));
        $expiryTime = ($customDetails ? $customDetails['expiryTime'] : (strtotime($date) * 1000));
        $shortID = '451d';
        $privateKey = md5(rand(1, 5));
        $publicKey = md5(rand(1, 5));
        if ($hasCDN) {
            $protocolSwitch = 'cdn';
        }

        if ($hasReality) {
            $protocolSwitch = 'reality';
        }

        switch ($protocolSwitch) {
            case 'vmess':
                $data = [
                    'up' => ($customDetails ? $customDetails['up'] : 0),
                    'down' => ($customDetails ? $customDetails['down'] : 0),
                    'total' => ($customDetails ? $customDetails['total'] : ($account_gig * (1024 * 1024 * 1024))),
                    'remark' => $remark,
                    'enabled' => false,
                    "expiryTime" => ($customDetails ? $customDetails['expiryTime'] : (strtotime($date) * 1000)),
                    'listen' => "",
                    'port' => $port,
                    'protocol' => 'vmess',
                    'settings' => '{
                        "clients": [
                          {
                            "id": "' . $uuid . '",
                            "alterId": 0
                          }
                        ],
                        "disableInsecureEncryption": false
                      }',
                    'streamSettings' => '{
                        "network": "ws",
                        "security": "none",
                        "wsSettings": {
                          "path": "/",
                          "headers": {}
                        }
                      }',
                    'sniffing' => '{
                        "enabled": true,
                        "destOverride": [
                          "http",
                          "tls"
                        ]
                      }'
                ];
                break;
            case 'vless':
                $data = [
                    'up' => ($customDetails ? $customDetails['up'] : 0),
                    'down' => ($customDetails ? $customDetails['down'] : 0),
                    'total' => ($customDetails ? $customDetails['total'] : ($account_gig * (1024 * 1024 * 1024))),
                    'remark' => $remark,
                    'enabled' => false,
                    "expiryTime" => ($customDetails ? $customDetails['expiryTime'] : (strtotime($date) * 1000)),
                    'listen' => "",
                    'port' => $port,
                    'protocol' => 'vless',
                    'settings' => '{
                            "clients": [
                              {
                                "id": "' . $uuid . '",
                                "flow": "xtls-rprx-direct"
                              }
                            ],
                            "decryption": "none",
                            "fallbacks": []
                          }',
                    'streamSettings' => '{
                            "network": "ws",
                            "security": "none",
                            "wsSettings": {
                              "path": "/",
                              "headers": {}
                            }
                          }',
                    'sniffing' => '{
                            "enabled": true,
                            "destOverride": [
                              "http",
                              "tls"
                            ]
                          }'
                ];

                break;
            case 'reality':
                $data = [
                    'up' => ($customDetails ? $customDetails['up'] : 0),
                    'down' => ($customDetails ? $customDetails['down'] : 0),
                    'total' => ($customDetails ? $customDetails['total'] : ($account_gig * (1024 * 1024 * 1024))),
                    'remark' => $remark,
                    'enabled' => false,
                    "expiryTime" => ($customDetails ? $customDetails['expiryTime'] : (strtotime($date) * 1000)),
                    'listen' => "",
                    'port' => $port,
                    'protocol' => 'vless',
                    'settings' => '{
  "clients": [
    {
      "id": "9c081c70-7c1d-404b-8659-4773277416bd",
      "email": "' . $remark . '.mtnservice@xray.com",
      "flow": "xtls-rprx-vision",
      "fingerprint": "chrome",
      "total": ' . $totalBand . ',
      "expiryTime": ' . $expiryTime . '
    }
  ],
  "decryption": "none",
  "fallbacks": []
}',
                    'streamSettings' => '{
  "network": "tcp",
  "security": "reality",
  "realitySettings": {
    "show": false,
    "dest": "' . get_static_option('reality_server_dest') . ':' . get_static_option('reality_server_port') . '",
    "xver": 0,
    "serverNames": [
      "' . get_static_option('reality_server_names') . '"
    ],
    "privateKey": "' . get_static_option('reality_private_key') . '",
    "publicKey": "' . get_static_option('reality_public_key') . '",
    "minClient": "",
    "maxClient": "",
    "maxTimediff": 0,
    "shortIds": [
      "' . $shortID . '"
    ]
  },
  "tcpSettings": {
    "header": {
      "type": "none"
    },
    "acceptProxyProtocol": false
  }
}',
                    'sniffing' => '{
                        "enabled": true,
                        "destOverride": [
                          "http",
                          "tls"
                        ]
                      }',
                    'ip_alert' => $package->user_limit + 1,
                    'ip_limit' => $package->user_limit
                ];
                break;
            case 'cdn':
                $data = [
                    'up' => ($customDetails ? $customDetails['up'] : 0),
                    'down' => ($customDetails ? $customDetails['down'] : 0),
                    'total' => ($customDetails ? $customDetails['total'] : ($account_gig * (1024 * 1024 * 1024))),
                    'remark' => $remark,
                    'enabled' => false,
                    "expiryTime" => ($customDetails ? $customDetails['expiryTime'] : (strtotime($date) * 1000)),
                    'listen' => "127.0.0.1",
                    'port' => $port,
                    'protocol' => 'vmess',
                    'settings' => '{
                        "clients": [
                          {
                            "id": "' . $uuid . '",
                            "alterId": 0
                          }
                        ],
                        "disableInsecureEncryption": false
                      }',
                    'streamSettings' => '{
                        "network": "ws",
                        "security": "none",
                        "wsSettings": {
                          "path": "/",
                          "headers": {}
                        }
                      }',
                    'sniffing' => '{
                        "enabled": true,
                        "destOverride": [
                          "http",
                          "tls"
                        ]
                      }'
                ];
                break;
        }
        $request = new Request();
        $request->merge(['transmission' => ($package->transmission === 'ws' ? 'websocket' : 'tcp')]);
        $request->merge(['domain' => $server->hostname]);
        $request->merge(['public_key' => $server->json_data['keys']['public_key']]);
        $request->merge(['private_key' => $server->json_data['keys']['private_key']]);
        $request->merge(['protocol' => $package->protocol['protocol']]);
        if ($package->protocol['ssl_protocol'] === 'xtls') {
            $request->merge(['xtls_protocol' => true]);
        } elseif ($package->protocol['ssl_protocol'] === 'tls') {
            $request->merge(['tls_protocol' => true]);
        }
        if ($hasReality) {
            return $data;
        } else {
//            dd($data['total']);
            $data = generateClientConfigurationByRequest($request, $data, $hasCDN, (strtotime($date) * 100), $account_gig);
//            dd($data);
            return $data;
        }
    }

    public function getAccountDetails($accountId)
    {
        $account = Accounts::with('servers')->findOrFail($accountId);
        $package = Package::where('id', $account->package_id)->where('status', 1)->where('price_level', auth()->user()->price_level)->first();
        if (!checkIfAccountIsPritunl($account)) {
            if (isset($account->json_data->multi_user)) {
                if ($account->json_data->multi_user) {
                    $accountData = $account->json_data->multi_user;
                    $response = multiUserGetAccountDetailsByEmail($accountData->email, $account->server_id, $account->package_id);
                    if (!$response) {
                        return response()->json([
                            'code' => 404,
                            'success' => false,
                            'message' => 'Successfully retrieved',
                            'body' => []
                        ]);
                    }
                    $accountDetails = [
                        'total' => $response['stats']['total'],
                        'up' => $response['stats']['up'],
                        'down' => $response['stats']['down'],
                        'expiryTime' => $response['stats']['expiryTime'],
                        'uuid' => $response['config']['id'],
                        'port' => $response['master_account']['port'],
                        'enable' => $response['stats']['enable']
                    ];
                    $hasReality = $package->protocol['reality_security'];
                    $uris = getAccountURI($account, $accountDetails, $response['master_account'], $hasReality);
                } else {
                    $accountDetails = getUserAccountDetail($account);
                    if (!$accountDetails) {
                        return response()->json([
                            'code' => 404,
                            'success' => false,
                            'message' => 'Successfully retrieved',
                            'body' => []
                        ]);
                    }
                    $hasReality = $package->protocol['reality_security'];
                    $uris = getAccountURI($account, $accountDetails, false, $hasReality);
                }
            } else {
                $accountDetails = getUserAccountDetail($account);
                if (!$accountDetails) {
                    return response()->json([
                        'code' => 404,
                        'success' => false,
                        'message' => 'Successfully retrieved',
                        'body' => []
                    ]);
                }
                $uris = getAccountURI($account, $accountDetails, false, true);
            }
            $uri = $uris['uri'];
            $tunnelUri = $uris['tunnel_uri'];
            if ($uris['network'] == 'tcp') {

                $response = [
                    'id' => $account->id,
                    'is_multi_user' => false,
                    'port' => $accountDetails['port'],
                    'traffic' => ' <span class="badge badge-dark" > ' . (($accountDetails['total'] == 0 ? 'نامحدود' : ($accountDetails['total'] / (1024 * 1024 * 1024)) . ' GB')) . ' </span > / <span class="badge badge-dark"> ' . round(($accountDetails['up'] + $accountDetails['down']) / (1024 * 1024 * 1024), 3) . ' GB </span > ',
                    'expires_at' => ($accountDetails['expiryTime'] == 0 ? 'دائمی' : Carbon::parse($accountDetails['expiryTime'] / 1000)->diffForHumans()),
                    'status' => ($accountDetails['enable'] == true ? ' <span class="badge badge-success"> فعال</span> ' : '<span class="badge badge-danger" > غیرفعال</span> '),
                    'address' => ' <input id="account_link_' . $account->id . '" style="width:1px;" value="' . $uri . '"><br><button id="copyClipboard" data-clipboard-target="#account_link_' . $account->id . '" style="color:black !important" data-is_dual_button="1" class="btn btn-warning btn-sm" > همراه اول و مودم</button><br><input style="width:1px;"  id="account_link_tunnel_' . $account->id . '" value="' . $tunnelUri . '"><br><button id="copyClipboard" data-clipboard-target="#account_link_tunnel_' . $account->id . '" class="btn btn-primary btn-sm">ایرانسل و رایتل</button>',
                    'account_data' => [
                        'status' => $accountDetails['enable'],
                        'total_used_traffic' => $accountDetails['up'] + $accountDetails['down'],
                        'total_traffic' => $accountDetails['total'],
                        'expiry_time' => $accountDetails['expiryTime'] / 1000,
                        'current_timestamp' => time()
                    ]
                ];
            } else {
                $response = [
                    'id' => $account->id,
                    'is_multi_user' => true,
                    'traffic' => ' <span class="badge badge-dark" > ' . (($accountDetails['total'] == 0 ? 'نامحدود' : ($accountDetails['total'] / (1024 * 1024 * 1024)) . ' GB')) . ' </span > / <span class="badge badge-dark"> ' . round(($accountDetails['up'] + $accountDetails['down']) / (1024 * 1024 * 1024), 3) . ' GB </span > ',
                    'port' => $accountDetails['port'],
                    'expires_at' => ($accountDetails['expiryTime'] == 0 ? 'دائمی' : Carbon::parse($accountDetails['expiryTime'] / 1000)->diffForHumans()),
                    'status' => ($accountDetails['enable'] == true ? ' <span class="badge badge-success"> فعال</span> ' : '<span class="badge badge-danger" > غیرفعال</span> '),
                    'address' => ' <input id="account_link_' . $account->id . '" style="width:1px;" value="' . $uri . '"><br><button id="copyClipboard" data-clipboard-target="#account_link_' . $account->id . '" data-is_dual_button="0" class="btn btn-primary btn-sm" > ایران</button>',
                    'account_data' => [
                        'status' => $accountDetails['enable'],
                        'total_used_traffic' => $accountDetails['up'] + $accountDetails['down'],
                        'total_traffic' => $accountDetails['total'],
                        'expiry_time' => $accountDetails['expiryTime'] / 1000,
                        'current_timestamp' => time()
                    ]
                ];
            }
        } else {
            $package = Package::where('id', $account->package_id)->first();
            $server = ModelsServers::where('id', $account->server_id)->first();
            $addPackageDaysToCreationTime = \Carbon\Carbon::parse($account->created_at)->addDays($package->expire_at);
            $createdAt = \Carbon\Carbon::parse($account->created_at);
            $diffInDays = $addPackageDaysToCreationTime->diffForHumans();
            $pritunlHandler = new PritunlWebService($server->json_data['pritunl_data']['api_token'], $server->json_data['pritunl_data']['api_secret'], $server->tunnel_ip);
            $accountDetails = $pritunlHandler->getUserDetails($server->json_data['pritunl_data']['organization_id'], $account->account_id);
            $serverLinks = $pritunlHandler->getAccountLinks($server->json_data['pritunl_data']['organization_id'], $account->account_id);
            $uri = 'https://' . $server->tunnel_ip . '/key/' . $serverLinks['id'] . '/' . $server->json_data['pritunl_data']['server_id'] . '.key';
            $response = [
                'id' => $account->id,
                'is_pritunl' => true,
                'is_multi_user' => true,
                'traffic' => 'نامحدود',
                'port' => 'ندارد',
                'expires_at' => $diffInDays,
                'status' => ($accountDetails['status'] == true ? ' <span class="badge badge-success"> آنلاین</span> ' : '<span class="badge badge-danger" > آفلاین</span> '),
                'address' => ' <input id="account_link_' . $account->id . '" style="width:1px;" value="' . $uri . '"><br><button id="copyClipboard" data-clipboard-target="#account_link_' . $account->id . '" class="btn btn-primary btn-sm" >کپی لینک</button>'
            ];
        }

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Successfully retrieved',
            'body' => $response
        ]);
    }

    public function downloadPritunlOVPN($id)
    {
        $account = Accounts::with('servers')->where('account_id', $id)->first();
        $server = $account->servers;
        if (is_null($account)) {
            return abort(404);
        }
        $package = Package::where('id', $account->package_id)->first();
        $pritunlHandler = new PritunlWebService($server->json_data['pritunl_data']['api_token'], $server->json_data['pritunl_data']['api_secret'], $server->tunnel_ip);
        $accountDetails = $pritunlHandler->getUserDetails($server->json_data['pritunl_data']['organization_id'], $account->account_id);
        $serverLinks = $pritunlHandler->getAccountLinks($server->json_data['pritunl_data']['organization_id'], $account->account_id);
//        $uri = 'https://' . $server->tunnel_ip . $serverLinks['view_url'];
        $uri = 'https://' . $server->tunnel_ip . '/key/' . $serverLinks['id'] . '/' . $server->json_data['pritunl_data']['server_id'] . '.key';
        return redirect()->to($uri);
    }

    public function rechargeAccount($id)
    {
        $account = Accounts::where('id', $id)->with('servers')->first();
        if (is_null($account)) {
            return abort(404);
        }

        $pageTitle = 'تمدید سرویس کاربری';
        return view('paper . pages . accounts . buy - bandwidth', compact('pageTitle', 'account'));
    }

    public function orderPackage($id)
    {
        $account = Accounts::where('id', $id)->with('servers')->first();
        if (isset($account->json_data->multi_user)) {
            if ($account->json_data->multi_user) {
                if ($account->json_data->multi_user->email) {
                    smilify('error', 'امکان تمدید این حساب کاربری نیست');
                    return redirect()->back();
                }
                smilify('error', 'امکان تمدید این حساب کاربری نیست');
                return redirect()->back();
            }
        }
        if (is_null($account)) {
            return abort(404);
        }
        $packages = Package::where('status', 1)->where('price_level', auth()->user()->price_level)->get();
        $pageTitle = 'تمدید دوره سرویس کاربری';
        return view('paper.pages.accounts.re-order', compact('pageTitle', 'account', 'packages'));
    }

    public function doOrderPackage($id, Request $request)
    {
        $request->validate([
            'package_id' => 'required'
        ]);
        $regenrateUUID = false;
        if (isset($request->regenerate_uuid)) {
            $regenrateUUID = true;
        }
        if (auth()->user()->hasRole('admin')) {
            $account = Accounts::where('id', $id)->first();
        } else {
            $account = Accounts::where('id', $id)->where('user_id', auth()->user()->id)->first();
        }
        if (isset($account->json_data->multi_user)) {
            if ($account->json_data->multi_user) {
                if ($account->json_data->multi_user->email) {
                    smilify('error', 'امکان تمدید این حساب کاربری نیست');
                    return redirect()->back();
                }
                smilify('error', 'امکان تمدید این حساب کاربری نیست');
                return redirect()->back();
            }
        }
        if (is_null($account)) {
            smilify('error', 'حساب کاربری یافت نشد');
            return redirect()->back();
        }
        $package = Package::where('id', $request->package_id)->where('status', 1)->where('price_level', auth()->user()->price_level)->first();
        if (is_null($package)) {
            smilify('error', 'پکیج یافت نشد');
            return redirect()->back();
        }

        if (auth()->user()->wallet < $package->price) {
            smilify('error', 'اعتبار حساب شما کافی نیست');
            return redirect()->back();
        }

        $accountDetail = getUserAccountDetail($account);
        if (!$accountDetail) {
            smilify('error', 'سرویس در سرور یافت نشد');
            return redirect()->back();
        }
        $accountDetail['up'] = 0;
        $accountDetail['down'] = 0;
        $accountDetail['total'] = ($package->bandwidth * (1024 * 1024 * 1024));
        $accountDetail['expiryTime'] = strtotime(date('Y-m-d', strtotime("+" . $package->expire_at . " days"))) * 1000;
        $accountDetail['enable'] = true;
        if ($regenrateUUID == true) {
            $currentSettings = json_decode($accountDetail['settings'], true);
            $oldUUID = $currentSettings['clients'][0]['id'];
            $accountDetail['settings'] = str_replace($oldUUID, md5(now()), $accountDetail['settings']);
        }
        $response = (new Servers())->editAccount($account->server_id, $accountDetail, $accountDetail['id']);
        if (!$response) {
            smilify('error', 'خطا در ارتباط با سرور');
            return redirect()->back();
        }
        $transactionsId = Transactions::create([
            'user_id' => auth()->user()->id,
            'type' => 'outgoing',
            'amount' => $package->price,
            'transaction_code' => Hash::make(now()),
            'description' => 'تمدید حساب کاربری با نام ' . $account->account_name
        ])->id;
        (new TransactionsController)->takeFromUserWallet(auth()->user()->id, $package->price);
        $account->package_id = $package->id;
        $account->save();
        smilify('success', 'حساب کاربری با موفقیت تمدید شد');
        return redirect()->route('accounts.show.edit.results', $account->id);
    }

    public function switchPower($id)
    {
        if (auth()->user()->hasRole('admin')) {
            $account = Accounts::where('id', $id)->first();
        } else {
            $account = Accounts::where('id', $id)->where('user_id', auth()->user()->id)->first();
        }
        if (is_null($account)) {
            smilify('error', 'سرویس کاربری یافت نشد');
            return redirect()->back();
        }
        $accountDetails = $this->getAccountDetails($id);
        try {
            $accountDetails = json_decode($accountDetails->content());
        } catch (\Exception $e) {
            smilify('error', 'سرویس کاربری یافت نشد');
            return redirect()->back();
        }
        if ($accountDetails->code == 200) {
            if ($accountDetails->body->is_multi_user == false) {
                if (
                    $accountDetails->body->account_data->total_traffic == 0 ||
                    $accountDetails->body->account_data->total_traffic > $accountDetails->body->account_data->total_used_traffic
                ) {
                    if ($accountDetails->body->account_data->expiry_time > time()) {
                        $accData = getUserAccountDetail($account);
                        if ($accData['enable']) {
                            $accData['enable'] = false;
                            $response = (new Servers())->editAccount($account->server_id, $accData, $accData['id']);
                            if (!$response) {
                                smilify('error', 'خطا در ارتباط با سرور');
                                return redirect()->back();
                            }
                            smilify('success', 'حساب کاربری با موفقیت غیرفعال شد');
                            return redirect()->back();
                        } else {
                            $accData['enable'] = true;
                            $response = (new Servers())->editAccount($account->server_id, $accData, $accData['id']);
                            if (!$response) {
                                smilify('error', 'خطا در ارتباط با سرور');
                                return redirect()->back();
                            }
                            smilify('success', 'حساب کاربری با موفقیت فعال شد');
                            return redirect()->back();
                        }
                    }
                }
            }
        }
        smilify('error', 'امکان تغییر در این سرویس کاربری وجود ندارد');
        return redirect()->back();
    }

    private function changeProtocolToWmessTcp($accountId, $user_id, $server_id, $package, $accountDetails)
    {
        $server = \App\Models\Servers::where('id', $server_id)->first();
        if (is_null($server)) {
            smilify('error', 'سرور انتخاب شده یافت نشد');
            return redirect()->back();
        }
        $accountOnDB = Accounts::where('id', $accountId)->first();
        if (is_null($accountOnDB)) {
            smilify('error', 'سرویس کاربری انتخاب شده یافت نشد');
            return redirect()->back();
        }


    }
}
