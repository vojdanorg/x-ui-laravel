<?php

namespace App\Http\Controllers;

use App\Models\Gateway;
use App\Models\Transactions;
use App\Models\User;
use App\Notifications\UserNotification;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class TransactionsController extends Controller
{
    public $gatewayToken = '1';
    public $gatewayURL = "https://gateway.zetasoln.ir/";

    public function list()
    {
        $pageTitle = 'لیست تراکنش ها';
        if (auth()->user()->hasRole('admin')) {
            $transactions = Transactions::orderBy('created_at', 'DESC')->with('user')->paginate(15);
        } else {
            $transactions = Transactions::where('user_id', auth()->user()->id)->orderBy('created_at', 'DESC')->paginate(15);
        }
        return view('paper.pages.transactions.list', compact('pageTitle', 'transactions'));
    }

    public function edit($id)
    {
        $transaction = Transactions::findOrFail($id);
        $pageTitle = 'ویرایش تراکنش';
        return view('paper.pages.transactions.edit', compact('pageTitle', 'transaction'));
    }

    public function add()
    {
        $pageTitle = 'افزایش موجودی';
        return view('paper.pages.transactions.add', compact('pageTitle'));
    }

    public function doAdd(Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric|max:1000000|min:150000'
        ]);
        $gateway = Gateway::create([
            'user_id' => auth()->user()->id,
            'amount' => $request->amount,
            'status' => 0,
            'authority' => null,
            'refid' => null,
            'payments_id' => null,
            'gateway_invoice_id' => null
        ]);
        $response = $this->sendRequestToGateway('api/v1/request-gateway', [
            'call_back_url' => url('verify-payment/' . $gateway->id),
            'amount' => $gateway->amount,
            'email' => auth()->user()->email,
            'mobile' => '09000000000',
            'description' => ' شارژ کیف پول به مبلغ' . number_format($gateway->amount)
        ]);
        $gateway->gateway_invoice_id = $response['body']['id'];
        $gateway->save();
        return redirect()->to($this->gatewayURL . 'api/z-pay-invoice/' . $response['body']['id']);
    }

    public function verifyPayment($id, Request $request)
    {
        $gateway = Gateway::where('status', 0)->where('user_id', auth()->user()->id)->findOrFail($id);
        $gateway->authority = $request->Authority;
        $response = $this->sendRequestToGateway('api/v1/verify-payment/' . $request->id . '?Authority=' . $request->Authority);
        $result = [
            'success' => false,
            'message' => ''
        ];
        if ($response != false || $response != null) {
            if ($response['success'] == true) {
                $gateway->status = 1;
                $gateway->refid = $response['body']['refid'];
                $result = [
                    'success' => true,
                    'message' => 'حساب شما ' . number_format($gateway->amount) . ' تومان شارژ شد'
                ];
                Transactions::create([
                    'user_id' => auth()->user()->id,
                    'type' => 'income',
                    'amount' => $gateway->amount,
                    'transaction_code' => $response['body']['refid'],
                    'description' => 'شارژ حساب کاربری با پرداخت انلاین '
                ]);
                $this->addToUserWallet(auth()->user()->id, $gateway->amount);
            } else {
                $gateway->status = -1;
                $result = [
                    'success' => false,
                    'message' => 'پرداخت انجام نشد'
                ];
            }
        } else {
            $result = [
                'success' => false,
                'message' => 'خطا در ارتباط با درگاه پرداخت. درصورتی که مبلغ از حساب شما کسر شده، حداکثر تا ۷۲ ساعت آینده به حساب شما مستردد میگردد'
            ];
        }
        $gateway->save();
        $pageTitle = 'نتیجه تراکنش';
        return view('paper.pages.transactions.result', compact('pageTitle', 'result'));
    }

    public function telegramVerifyPayment($id, Request $request)
    {
        $gateway = Gateway::where('status', 0)->findOrFail($id);
        $gateway->authority = $request->Authority;
        $response = $this->sendRequestToGateway('api/v1/verify-payment/' . $request->id . '?Authority=' . $request->Authority);
        $result = [
            'success' => false,
            'message' => ''
        ];
        if ($response != false || $response != null) {
            if ($response['success'] == true) {
                $gateway->status = 1;
                $gateway->refid = $response['body']['refid'];
                $result = [
                    'success' => true,
                    'message' => 'پرداخت موفقیت آمیز بود. لطفا به روبات تلگرام برگردید و دکمه تایید خرید را بفشارید',
                    'payment_id' => $gateway->id
                ];
                Transactions::create([
                    'user_id' => auth()->user()->id,
                    'type' => 'income',
                    'amount' => $gateway->amount,
                    'transaction_code' => $response['body']['refid'],
                    'description' => 'خرید با درگاه پرداخت روبات تلگرام'
                ]);
            } else {
                $gateway->status = -1;
                $result = [
                    'success' => false,
                    'message' => 'پرداخت انجام نشد'
                ];
            }
        } else {
            $result = [
                'success' => false,
                'message' => 'خطا در ارتباط با درگاه پرداخت. درصورتی که مبلغ از حساب شما کسر شده، حداکثر تا ۷۲ ساعت آینده به حساب شما مستردد میگردد'
            ];
        }
        $gateway->save();
        $pageTitle = 'نتیجه تراکنش';
        return view('telegrambot.payment.result', compact('pageTitle', 'result'));
    }

    private function sendRequestToGateway($endPoint, $data = [])
    {
        try {
            $response = Http::withHeaders([
                'Token' => $this->gatewayToken,
                'Cache-Control' => 'no-cache'
            ])->post($this->gatewayURL . $endPoint, $data);
            return json_decode($response->body(), 1);
        } catch (Exception $e) {
            return false;
        }
    }

    public function changeSettled($id)
    {
        $transaction = Transactions::where('id', $id)->first();
        $transaction->settled = !$transaction->settled;
        $transaction->save();
        smilify('success', 'وضعیت تسویه حساب با موفقیت تغییر کرد');
        return redirect()->back();
    }

    public function doEdit($id, Request $request)
    {
        $request->validate([
            'type' => 'required',
            'amount' => 'required|numeric',
            'transaction_code' => 'required',
            'description' => 'required'
        ]);
        if ($request->type == 'income') {
            $this->addToUserWallet($request->user_id, $request->amount);
        } else {
            $this->takeFromUserWallet($request->user_id, $request->amount);
        }
        Transactions::where('id', $id)->update([
            'type' => $request->type,
            'amount' => $request->amount,
            'transaction_code' => $request->transaction_code,
            'description' => $request->description
        ]);
        smilify('success', 'تراکنش ویرایش شد');
        return redirect()->back();
    }

    public function addByAdmin()
    {
        $pageTitle = 'افزودن تراکنش';
        return view('paper.pages.transactions.add-by-admin', compact('pageTitle'));
    }

    public function doAddByAdmin(Request $request)
    {
        $request->validate([
            'type' => 'required',
            'amount' => 'required|numeric',
            'transaction_code' => 'required',
            'description' => 'required'
        ]);
        $user = User::findOrFail($request->user_id);
        if ($request->type == 'income') {
            $user->notify(new UserNotification(
                'success',
                'plus-circle',
                ' حساب شما مبلغ ' . number_format($request->amount) . ' تومان شارژ شد '
            ));
            $this->addToUserWallet($request->user_id, $request->amount);
        } else {
            $user->notify(new UserNotification(
                'danger',
                'minus-circle',
                'از حساب شما مبلغ ' . number_format($request->amount) . ' تومان کسر شد '
            ));
            $this->takeFromUserWallet($request->user_id, $request->amount);
        }
        Transactions::create([
            'user_id' => $request->user_id,
            'type' => $request->type,
            'amount' => $request->amount,
            'transaction_code' => $request->transaction_code,
            'description' => $request->description
        ]);
        smilify('success', 'تراکنش اضافه شد');
        return redirect()->back();
    }

    public function takeFromUserWallet($userId, $amount)
    {
        $user = User::where('id', $userId)->first();
        $user->wallet = $user->wallet - $amount;
        $user->save();
    }

    public function addToUserWallet($userId, $amount)
    {
        $user = User::where('id', $userId)->first();
        $user->wallet = $user->wallet + $amount;
        $user->save();
    }
}
