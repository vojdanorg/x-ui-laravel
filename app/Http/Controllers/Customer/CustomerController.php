<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Customer\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class CustomerController extends Controller
{
    public function register(Request $request){
        $validator = Validator::validate($request->toArray(),[
            'reseller_id'=>'required|numeric',
            'fullname'=>'required|max:2048',
            'email'=>'required|max:4096|email|unique:customer,email',
            'mobile'=>'required|max:20|unique:customer,mobile',
            'password'=>['required', 'confirmed',Password::defaults()],
        ]);
        $customer = [
            'user_id'=>$request->reseller_id,
            'fullname'=>$request->fullname,
            'email'=>$request->email,
            'mobile'=>$request->mobile,
            'password'=>Hash::make($request->password)
        ];
        $customer_id = Customer::create($customer)->id;
        $customer['customer_id'] = $customer_id;
        unset($customer['password']);
        unset($customer['user_id']);
        return $customer;
    }
}
