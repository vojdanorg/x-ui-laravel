<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Account\AccountController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Customer\CustomerController;
use App\Http\Controllers\TransactionsController;
use App\Models\Accounts;
use App\Models\Gateway;
use App\Models\Package;
use App\Models\Servers;
use App\Models\Transactions;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;


class ApiController extends Controller
{
    protected $user;
    public $gatewayToken = 'dbfeab503bf79c117b82c37988341ebb';
    public $gatewayURL = "https://gateway.zetasoln.ir/";

    public function __construct()
    {
        $this->user = User::where('api_token', getallheaders()['Token'])->with('accounts', 'transactions')->first();
    }

    public function getUserInfo()
    {
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'success',
            'body' => [
                'name' => $this->user->name,
                'email' => $this->user->email,
                'wallet' => $this->user->wallet,
                'count_accounts' => $this->user->accounts->count(),
                'count_transactions' => $this->user->transactions->count(),
            ]
        ]);
    }

    public function getAvailableServers()
    {
        $servers = Servers::all();
        $availableServers = [];
        foreach ($servers as $server) {
            $countServerClients = countServerClients($server->id, true);
            if ($countServerClients) {
                if (countServerClients($server->id) < $server->max_users) {
                    $availableServers[] = [
                        'server_id' => $server->id,
                        'server_name' => $server->server_name
                    ];
                }
            }
        }
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'success',
            'body' => $availableServers
        ]);
    }

    public function buyAccount(Request $request)
    {
        $request->validate([
            'account_name' => 'required|max:1024',
            'server_id' => 'required|numeric',
            'package_id' => 'required|numeric',
            'protocol' => ['required', Rule::in((new AccountController)->allowedProtocols)],
            'account_gig' => 'required|numeric'
        ]);
        $server = Servers::find($request->server_id);
        if (is_null($server)) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'سرور مورد نظر یافت نشد',
                'body' => []
            ]);
        }
        $package = Package::where('id', $request->package_id)->where('status', 1)->first();
        if (is_null($package)) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'پکیج مورد نظر یافت نشد',
                'body' => []
            ]);
        }
        if (countServerClients($server->id) >= $server->max_users) {
            return response()->json([
                'code' => 403,
                'success' => false,
                'message' => 'ظرفیت سرور تکمیل است',
                'body' => []
            ]);
        }

        $price = 0;
        $gig = $request->account_gig;
        if ($gig < 100) {
            $price = $gig * 1650;
        } else {
            $price = $gig * 1350;
        }

        if ($gig == 0) {
            return response()->json([
                'code' => 403,
                'success' => false,
                'message' => 'ساخت سرویس با حجم نامحدود ممکن نیست',
                'body' => []
            ]);
        }

        if ($this->user->wallet < $price) {
            return response()->json([
                'code' => 403,
                'success' => false,
                'message' => 'اعتبار حساب شما کافی نیست',
                'body' => []
            ]);
        }

        $account = Accounts::create([
            'user_id' => $this->user->id,
            'account_name' => $request->account_name,
            'package_id' => $package->id,
            'server_id' => $server->id,
            'account_id' => 0,
        ])->id;

        $response = (new AccountController)->createAccountOnServer($account, $this->user->id, $server->id, $package, $request->protocol, $request->account_gig);
        if ($response) {
            if ($response->success) {
                $transactionsId = Transactions::create([
                    'user_id' => $this->user->id,
                    'type' => 'outgoing',
                    'amount' => $price,
                    'transaction_code' => Hash::make(now()),
                    'description' => 'خرید حساب کاربری توسط وب سرویس با نام ' . $request->account_name
                ])->id;
                Accounts::where('id', $account)->update([
                    'account_id' => 'user,' . $this->user->id . ',' . $account,
                    'transactions_id' => $transactionsId,
                ]);
                $account = Accounts::where('id', $account)->first();
                $accountData = getUserAccountDetail($account);
                $uris = getAccountURI($account, $accountData);
                $uri = $uris['uri'];
                $tunnelUri = $uris['tunnel_uri'];

                (new TransactionsController)->takeFromUserWallet($this->user->id, $price);
                return response()->json([
                    'code' => 200,
                    'success' => true,
                    'message' => 'حساب کاربری با موفقیت ایجاد شد',
                    'body' => [
                        'id' => $account->id,
                        'account_name' => $account->account_name,
                        'port' => $accountData['port'],
                        'expires_at' => Carbon::parse($accountData['expiryTime'] / 1000)->format('Y-m-d'),
                        'uri' => [
                            'bypass_melli_uri' => $uri,
                            'direct_uri' => $tunnelUri
                        ],
                        'protocol' => $accountData['protocol'],
                    ]
                ]);
            } else {
                return response()->json([
                    'code' => 500,
                    'success' => false,
                    'message' => 'خطا در ایجاد حساب کاربری مجددا امتحان کنید',
                    'body' => []
                ]);
                Accounts::where('id', $account)->delete();
            }
        } else {
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => 'خطا در ایجاد حساب کاربری مجددا امتحان کنید',
                'body' => []
            ]);
            Accounts::where('id', $account)->delete();
        }
    }

    public function createInvoice(Request $request)
    {
        $request->validate([
            'email' => 'required|max:4096',
            'phone_number' => 'required|max:12',
            'package_id' => 'required|numeric'
        ]);
        $package = Package::where('id', $request->package_id)->first();
        if (is_null($package)) {
            return response()->json([
                'code' => 404,
                'message' => 'Package not found',
                'success' => false,
                'body' => []
            ]);
        }
        $sellPrice = $package->price;
        if (is_null($sellPrice)) {
            return response()->json([
                'code' => 403,
                'success' => true,
                'message' => 'قیمت فروش در پنل ادمین تایین نشده',
                'body' => []
            ]);
        }
        $gateway = Gateway::create([
            'user_id' => $this->user->id,
            'amount' => $sellPrice,
            'status' => 0,
            'authority' => null,
            'refid' => null,
            'payments_id' => null,
            'gateway_invoice_id' => null
        ]);
        $response = $this->sendRequestToGateway('api/v1/request-gateway', [
            'call_back_url' => route('telegram.payment.verify', $gateway->id),
            'amount' => $sellPrice,
            'email' => $request->email,
            'mobile' => $request->phone_number,
            'description' => 'پرداخت فاکتور شمار ' . $gateway->id
        ]);
        if (!$response) {
            return response()->json([
                'code' => 500,
                'success' => true,
                'message' => 'خطا در ارتباط با درگاه پرداخت',
                'body' => []
            ]);
            $gateway->delete();
            @$gateway->save();
        }
        $gateway->gateway_invoice_id = $response['body']['id'];
        $gateway->save();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'درگاه پرداخت ایجاد شد',
            'body' => [
                'payment_url' => $this->gatewayURL . 'api/z-pay-invoice/' . $response['body']['id'],
                'payment_id' => $gateway->id
            ]
        ]);
        // return redirect()->to($this->gatewayURL . 'api/z-pay-invoice/' . $response['body']['id']);
    }

    private function sendRequestToGateway($endPoint, $data = [])
    {
        try {
            $response = Http::withHeaders([
                'Token' => $this->gatewayToken,
                'Cache-Control' => 'no-cache'
            ])->post($this->gatewayURL . $endPoint, $data);
            return json_decode($response->body(), 1);
        } catch (Exception $e) {
            return false;
        }
    }

    public function verifyInvoicePayment(Request $request)
    {
        $request->validate([
            'payment_id' => 'required'
        ]);
        $gateway = Gateway::where('id', $request->payment_id)->where('user_id', $this->user->id)->first();
        if (is_null($gateway)) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'پرداختی یافت نشد',
                'body' => []
            ]);
        }
        if ($gateway->status == 1) {
            return response()->json([
                'code' => 200,
                'success' => true,
                'message' => 'پرداخت موفقیت آمیز بود',
                'body' => [
                    'payment_id' => $gateway->id,
                    'amount' => $gateway->amount,
                    'created_at' => $gateway->created_at,
                    'updated_at' => $gateway->updated_at
                ]
            ]);
        } else {
            return response()->json([
                'code' => 403,
                'success' => false,
                'message' => 'پرداخت ناموفق بود',
                'body' => [
                    'payment_id' => $gateway->id,
                    'amount' => $gateway->amount,
                    'created_at' => $gateway->created_at,
                    'updated_at' => $gateway->updated_at
                ]
            ]);
        }
    }

    public function accountStats(Request $request)
    {
        $request->validate([
            'account_id' => 'required'
        ]);
        $account = Accounts::where('id', $request->account_id)->where('user_id', $this->user->id)->with(['servers'])->first();
        if (is_null($account)) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'اکانت یافت نشد',
                'body' => []
            ]);
        }
        $accountStats = getUserAccountDetail($account);
        if (!$accountStats) {
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => 'خطا در ارتباط با سرور اکانت',
                'body' => []
            ]);
        }
        $uris = getAccountURI($account, $accountStats);
        $uri = $uris['uri'];
        $tunnelUri = $uris['tunnel_uri'];

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'ارتباط با سرور اکانت موفقیت آمیز بود',
            'body' => [
                'account_id' => $account->id,
                'total_bandwidth' => ($accountStats['total'] / (1024 * 1024 * 1024)),
                'used_bandwidth' => round(($accountStats['up'] + $accountStats['down']) / (1024 * 1024 * 1024), 3),
                'port' => $accountStats['port'],
                'expires_at' => Carbon::parse($accountStats['expiryTime'] / 1000)->format('Y-m-d'),
                'status' => $accountStats['enable'],
                'address' => [
                    'tunneled' => $uri,
                    'direct' => $tunnelUri,
                ],
                'server' => [
                    'server_id' => $account->servers->id,
                    'server_name' => $account->servers->server_name
                ]
            ]
        ]);
    }

    public function getAvailablePackages(Request $request)
    {
        $packages = Package::where('status', 1)->get();
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'success',
            'body' => $packages
        ]);
    }

    public function registerCustomer(Request $request)
    {
        $request->request->add(['reseller_id' => $this->user->id]);
        $customer = (new CustomerController)->register($request);
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'success',
            'body' => $customer
        ]);
    }

    public function getAccountInfo(Request $request)
    {
        $request->validate([
            'server_addr' => 'required',
            'uuid' => 'required'
        ]);
        $server = findServerByListOfDomain($request->server_addr);
        if (!$server) {
            return response()->json([
                'code' => 404,
                'message' => 'Server not found!',
                'success' => false,
                'body' => []
            ]);
        }
        $isMultiUser = checkIfServerIsAllowedByAnyPackageToBeMultiUser($server, false, true);
        if ($isMultiUser) {
            $listOfAllAccounts = Accounts::all();
            $account = null;
            foreach ($listOfAllAccounts as $item) {
                if (isset($item->json_data->multi_user)) {
                    if ($item->json_data->multi_user) {
                        if ($item->json_data->multi_user->id == $request->uuid) {
                            $account = $item;
                            break;
                        }
                    }
                }
            }
            if (is_null($account)) {
                return response()->json([
                    'code' => 404,
                    'message' => 'Account not found!',
                    'success' => false,
                    'body' => []
                ]);
            }
            $accountDetails = multiUserGetAccountDetailsByEmail($account->json_data->multi_user->email, $server->id, $account->package_id);
            if (!$accountDetails) {
                return response()->json([
                    'code' => 404,
                    'message' => 'Account not found!',
                    'success' => false,
                    'body' => []
                ]);
            }
            $accountOnServerDB = [
                'total' => $accountDetails['stats']['total'],
                'up' => $accountDetails['stats']['up'],
                'down' => $accountDetails['stats']['down'],
                'expiryTime' => $accountDetails['stats']['expiryTime'],
                'uuid' => $accountDetails['config']['id'],
                'port' => $accountDetails['master_account']['port'],
                'enable' => $accountDetails['stats']['enable']
            ];
            $accountDetails['uri'] = getAccountURI($account, $accountOnServerDB, $accountDetails['master_account']);
            unset($accountDetails['master_account']);
            $package = Package::where('id', $account->package_id)->first();
            $reseller = User::where('id', $account->user_id)->first();
            $server = $server->toArray();
            return response()->json([
                'code' => 200,
                'message' => 'success',
                'success' => true,
                'body' => [
                    'is_multi_user' => true,
                    'local_db' => $account,
                    'package' => $package,
                    'reseller' => $reseller,
                    'server_db' => $accountDetails
                ]
            ]);
        } else {
            $clientsOnServer = getClientsOnServer($server->id);
            $accountDetails = null;
            foreach ($clientsOnServer as $item) {
                $itemSettings = json_decode($item['settings'], true);
                if ($itemSettings['clients'][0]['id'] == $request->uuid) {
                    $accountDetails = $item;
                }
            }
            if (is_null($accountDetails)) {
                return response()->json([
                    'code' => 404,
                    'message' => 'Account not found!',
                    'success' => false,
                    'body' => []
                ]);
            }
            $remarkID = explode(',', $accountDetails['remark']);
            $account = true;
            $package = false;
            $reseller = false;
            if (!is_array($remarkID)) {
                $account = false;
            }
            if ($account) {
                $account = Accounts::where('id', $remarkID[2])->first();
                $package = Package::where('id', $account->package_id)->first();
                $reseller = User::where('id', $account->user_id)->first();
            }
            $accountDetails['uri'] = getAccountURI($account, $accountDetails);
            $server = $server->toArray();
            return response()->json([
                'code' => 200,
                'message' => 'success',
                'success' => true,
                'body' => [
                    'is_multi_user' => false,
                    'local_db' => $account,
                    'package' => $package,
                    'reseller' => $reseller,
                    'server_db' => $accountDetails
                ]
            ]);

        }
    }

    public function tempGetAccountInfo(Request $request)
    {
        $request->merge([
            'server_ip' => strtolower($request->server_ip)
        ]);
        $server = Servers::where('hostname', $request->server_ip)
            ->orWhere('tunnel_ip', $request->server_ip)
            ->orWhere('iran_ip', $request->server_ip)
            ->orWhere('eu_ip', $request->server_ip)
            ->first();

        if (is_null($server)) {
            $servers = Servers::all();
            foreach ($servers as $sv) {
                if (!is_null($sv->json_data)) {
                    if (isset($sv->json_data['extra_domain_or_ip'])) {
                        if (is_array($sv->json_data['extra_domain_or_ip'])) {
                            foreach ($sv->json_data['extra_domain_or_ip'] as $dm) {
                                if ($dm == $request->sni) {
                                    $server = $sv;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if (is_null($server)) {
                return response()->json([
                    'code' => 404,
                    'success' => false,
                    'message' => 'Server not found',
                    'body' => []
                ]);
            }
        }
        $account = null;
        if ($request->vless == 1) {
            $serverAccounts = getClientsOnServer($server->id);
            foreach ($serverAccounts as $acc) {
                if ($acc['port'] == $request->account_port) {
                    $account = $acc;
                }
            }
        } else {
            if ($request->net == 'grpc') {
                $server = Servers::where('hostname', $request->sni)
                    ->orWhere('tunnel_ip', $request->sni)
                    ->orWhere('iran_ip', $request->sni)
                    ->orWhere('eu_ip', $request->sni)
                    ->first();
                if (is_null($server)) {
                    $servers = Servers::all();
                    foreach ($servers as $sv) {
                        if (!is_null($sv->json_data)) {
                            if (isset($sv->json_data['extra_domain_or_ip'])) {
                                if (is_array($sv->json_data['extra_domain_or_ip'])) {
                                    foreach ($sv->json_data['extra_domain_or_ip'] as $dm) {
                                        if ($dm == $request->sni) {
                                            $server = $sv;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (is_null($server)) {
                        return response()->json([
                            'code' => 404,
                            'success' => false,
                            'message' => 'Server not found',
                            'body' => []
                        ]);
                    }
                }
                $accountsList = Accounts::all();
                $foundedAccount = null;
                foreach ($accountsList as $item) {
                    if (is_null($item->json_data)) {
                        continue;
                    }
                    $jsonData = $item->json_data;
                    if (!isset($jsonData->multi_user)) {
                        continue;
                    }
                    if ($jsonData->multi_user->id == $request->id) {
                        $foundedAccount = $item;
                        break;
                    }
                }

                $account = multiUserGetAccountDetailsByUUID($request->id, $server->id, $foundedAccount->package_id);
                $account['config']['uuid'] = $request->id;
                $uris = getAccountURI($foundedAccount, $account['config'], $account['master_account']);
                $accountOnDB = $foundedAccount;
                $uri = $uris['uri'];
                $account = $account['stats'];
                $tunnelUri = $uris['tunnel_uri'];
                return response()->json([
                    'code' => 200,
                    'success' => true,
                    'message' => 'Success',
                    'body' => [
                        'account_id' => $accountOnDB->id,
                        'bandwidth' => [
                            'total_bandwidth' => ($account['total'] / (1024 * 1024 * 1024)),
                            'used_bandwidth' => round(($account['up'] + $account['down']) / (1024 * 1024 * 1024), 3),
                            'upload' => ($account['up'] / (1024 * 1024 * 1024)),
                            'download' => ($account['down'] / (1024 * 1024 * 1024)),
                        ],
                        'port' => 0000,
                        'expires_at' => Carbon::parse($account['expiryTime'] / 1000)->format('Y-m-d'),
                        'status' => $account['enable'],
                        'address' => [
                            'tunneled' => $uri,
                            'direct' => $tunnelUri,
                        ],
                        'server' => [
                            'server_id' => $accountOnDB->servers->id,
                            'server_name' => $accountOnDB->servers->server_name
                        ]
                    ]
                ]);
            } else {
                $serverAccounts = getClientsOnServer($server->id);
                foreach ($serverAccounts as $acc) {
                    if ($acc['port'] == $request->account_port) {
                        $account = $acc;
                    }
                }
            }
        }
        if (is_null($account)) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'Account with this port not found',
                'body' => []
            ]);
        }
        $remark = explode(',', $account['remark']);
        $accountOnDB = Accounts::where('id', $remark[2])->with('servers')->first();
        if (is_null($accountOnDB)) {
            return response()->json([
                'code' => 403,
                'success' => false,
                'message' => 'Account is not added to the database',
                'body' => []
            ]);
        }

        $uris = getAccountURI($accountOnDB, $account);
        $uri = $uris['uri'];
        $tunnelUri = $uris['tunnel_uri'];
        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Success',
            'body' => [
                'account_id' => $accountOnDB->id,
                'bandwidth' => [
                    'total_bandwidth' => ($account['total'] / (1024 * 1024 * 1024)),
                    'used_bandwidth' => round(($account['up'] + $account['down']) / (1024 * 1024 * 1024), 3),
                    'upload' => ($account['up'] / (1024 * 1024 * 1024)),
                    'download' => ($account['down'] / (1024 * 1024 * 1024)),
                ],
                'port' => $account['port'],
                'expires_at' => Carbon::parse($account['expiryTime'] / 1000)->format('Y-m-d'),
                'status' => $account['enable'],
                'address' => [
                    'tunneled' => $uri,
                    'direct' => $tunnelUri,
                ],
                'server' => [
                    'server_id' => $accountOnDB->servers->id,
                    'server_name' => $accountOnDB->servers->server_name
                ]
            ]
        ]);
    }

    public function rebuildAccounts(Request $request)
    {
        $request->validate([
            'remark' => 'required|max:2048',
            'uuid' => 'required|max:2048',
            'port' => 'required|numeric|min:1000|max:65535',
            'hostname' => 'required|max:2024',
            'package_replacement_id' => 'required'
        ]);
        $accountOnDB = Accounts::where('account_name', $request->remark)->with('package')->first();
        if (is_null($accountOnDB)) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'account with this remark not found',
                'body' => []
            ]);
        }
        $server = Servers::where('id', $accountOnDB->server_id)->first();
        if (is_null($server)) {
            return response()->json([
                'code' => 404,
                'success' => false,
                'message' => 'Server with this remark not found',
                'body' => []
            ]);
        }
        $account = getUserAccountDetail($accountOnDB);
        if (!$account) {
            $accountController = new AccountController();
            if (is_null($accountOnDB->package)) {
                $package = Package::where('id', $request->package_replacement_id)->first();
            } else {
                $package = $accountOnDB->package;
            }
            $response = $accountController->createAccountOnServer($accountOnDB->id, $accountOnDB->user_id, $server->id, $package, $server, false, $accountOnDB->created_at, (int)$request->port, $request->uuid);
            if (!$response) {
                return response()->json([
                    'code' => 500,
                    'success' => false,
                    'message' => 'Faild to connect to the server',
                    'body' => []
                ]);
            }
            $account = getUserAccountDetail($accountOnDB);
        }
        if ($account) {
            $uris = getAccountURI($accountOnDB, $account);
            $uri = $uris['uri'];
            $tunnelUri = $uris['tunnel_uri'];
            return response()->json([
                'code' => 200,
                'success' => true,
                'message' => 'Success',
                'body' => [
                    'account_id' => $accountOnDB->id,
                    'bandwidth' => [
                        'total_bandwidth' => ($account['total'] / (1024 * 1024 * 1024)),
                        'used_bandwidth' => round(($account['up'] + $account['down']) / (1024 * 1024 * 1024), 3),
                        'upload' => ($account['up'] / (1024 * 1024 * 1024)),
                        'download' => ($account['down'] / (1024 * 1024 * 1024)),
                    ],
                    'port' => $account['port'],
                    'expires_at' => Carbon::parse($account['expiryTime'] / 1000)->format('Y-m-d'),
                    'status' => $account['enable'],
                    'address' => [
                        'tunneled' => $uri,
                        'direct' => $tunnelUri,
                    ],
                    'server' => [
                        'server_id' => $accountOnDB->servers->id,
                        'server_name' => $accountOnDB->servers->server_name
                    ]
                ]
            ]);
        } else {
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => 'Faild to connect to the server - err',
                'body' => []
            ]);
        }
    }

    public function rebuildAccountsByTerm(Request $request)
    {
        $request->validate([
            'term' => 'required',
            'server_id' => 'required',
            'account_gig' => 'required'
        ]);
        $accounts = Accounts::where('account_name', 'LIKE', '%' . $request->term . '%')->where('server_id', $request->server_id)->with('package')->get();
        $accountController = new AccountController();
        foreach ($accounts as $accountOnDB) {
            $account = getUserAccountDetail($accountOnDB);
            if (!$account) {
                $server = Servers::where('id', $accountOnDB->server_id)->first();
                if (is_null($accountOnDB->package)) {
                    $package = Package::where('id', $request->package_replacement_id)->first();
                } else {
                    $package = $accountOnDB->package;
                }
                $response = $accountController->createAccountOnServer($accountOnDB->id, $accountOnDB->user_id, $server->id, $package, $server, false, $accountOnDB->created_at, $request->account_gig);
            }
        }
        return response()->json([
            'done' => true
        ]);
    }
}
