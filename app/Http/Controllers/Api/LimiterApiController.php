<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Accounts;
use App\Models\Servers;
use Illuminate\Http\Request;

class LimiterApiController extends Controller
{
    private $token = '!@3iojoijqd(AWUd09j12';

    public function getListOfAccountOnServer(Request $request)
    {
        if (!$this->checkForApiToken()) {
            return $this->return403Error();
        }
        $request->validate([
            'eu_server_ip' => 'required'
        ]);

        $server = Servers::where('tunnel_ip', $request->eu_server_ip)->first();
        if (is_null($server)) {
            return $this->returnServerNotFound();
        }
        $serverClients = getClientsOnServer($server->id);
        if(!$serverClients){
            return response()->json([
                'code'=>500,
                'success'=>false,
                'message'=>'error while connecting to the server',
                'body'=>[]
            ]);
        }
        return response()->json([
            'code'=>200,
            'success'=>true,
            'message'=>'retrieved',
            'body'=>$serverClients
        ]);
    }

    public function addListOfConnectedIPsToAccount(Request $request){
        if (!$this->checkForApiToken()) {
            return $this->return403Error();
        }
        $request->validate([
            'eu_server_ip' => 'required',
            'ips'=>'required',
            'account_remark'=>'required'
        ]);
        $server = Servers::where('tunnel_ip', $request->eu_server_ip)->first();
        if (is_null($server)) {
            return $this->returnServerNotFound();
        }
        $account = Accounts::where('account_id',$request->account_remark)->where('server_id',$server->id)->update([
            'ips'=>$request->ips
        ]);
        return response()->json([
            'code'=>200,
            'success'=>true,
            'message'=>'updated',
            'body'=>[]
        ]);

    }

    private function checkForApiToken()
    {
        if (!isset(getallheaders()['Token'])) {
            return false;
        }
        if (getallheaders()['Token'] != $this->token) {
            return false;
        }
        return true;
    }

    private function return403Error()
    {
        return response()->json([
            'code' => 403,
            'success' => false,
            'message' => 'forbidden',
            'body' => []
        ]);
    }

    private function returnServerNotFound()
    {
        return response()->json([
            'code' => 404,
            'success' => false,
            'message' => 'Server not found!',
            'body' => []
        ]);
    }
}
