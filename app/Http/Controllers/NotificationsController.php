<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    public function readById($id)
    {
        $notification = auth()->user()->notifications()->findOrFail($id);
        $notification->markAsRead();
        return redirect()->back();
    }

    public function readAll()
    {
        $user = auth()->user();
        $notifications = $user->unreadNotifications()->get();
        foreach ($notifications as $notification) {
            $notification->markAsRead();
        }
        return redirect()->back();
    }

    public function list()
    {
        $pageTitle = 'اطلاعیه ها';
        $user = User::findOrFail(auth()->user()->id);
        $notifications = Auth::user()->notifications()->paginate(20);

        return view('paper.pages.user.notifications', compact('pageTitle', 'notifications'));
    }
}
