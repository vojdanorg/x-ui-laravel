<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TransactionsController;
use App\Models\Accounts;
use App\Models\Transactions;
use App\Notifications\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;

class DashboardController extends Controller
{
    public function index()
    {
        $pageTitle = 'داشبورد';
        $accounts = Accounts::where('user_id', auth()->user()->id)->with('servers')->orderBy('created_at', 'DESC')->limit(4)->get();
        $accountsCount = Accounts::where('user_id', auth()->user()->id)->get()->count();
        $unSettledTransactions = Transactions::where('user_id', auth()->user()->id)->UnSettleds();
        $transactions = Transactions::where('user_id', auth()->user()->id)->get();
        $incomeChart = [];
        $chart_options = [
            'chart_title' => 'تعداد تراکنش در روز',
            'report_type' => 'group_by_date',
            'model' => 'App\Models\Transactions',
            'conditions' => [
                ['name' => 'income', 'condition' => 'type = "outgoing" AND user_id = ' . auth()->user()->id, 'color' => 'black', 'fill' => false]
            ],
            'group_by_field' => 'created_at',
            'group_by_period' => 'day',
            'filter_field' => 'created_at',
            'filter_period' => 'week',
            'filter_days' => 30,
            'chart_type' => 'line'
        ];

        $incomeChart = new LaravelChart($chart_options);


        $todayLeaderboard = DB::table('transactions')
            ->select('users.name', 'users.id', 'last_gift_date', DB::raw('count(transactions.id) as total_transactions'))
            ->where('type', 'outgoing')
            ->join('users', 'transactions.user_id', '=', 'users.id')
            ->whereDate('transactions.created_at', Carbon::today('Asia/Tehran'))
            ->groupBy('users.id')
            ->orderByDesc('total_transactions')
            ->get();

        foreach ($todayLeaderboard as $checkReceiveCode) {
            $todayCheckUserGetGift = DB::table('users')->where('id', $checkReceiveCode->id);
            $dateTodayGiftCheck = Carbon::parse($checkReceiveCode->last_gift_date);
            foreach ($todayCheckUserGetGift as $check) {
                if (!$dateTodayGiftCheck->isToday()) {
                    $todayUpdateUserReceiveStatus = DB::table('users')->where('id', $checkReceiveCode->id)->update([
                        'today_gift_receive' => 0,
                        'last_gift_date' => Carbon::today()
                    ]);
                }
            }

        }

        $todayGift = DB::table('transactions')
            ->select('users.name', 'users.id', 'last_gift_date', 'today_gift_receive', 'last_gift_amount', DB::raw('count(transactions.id) as total_transactions'))
            ->where('type', 'outgoing')
            ->where('today_gift_receive', '0')
            ->join('users', 'transactions.user_id', '=', 'users.id')
            ->whereDate('transactions.created_at', Carbon::today('Asia/Tehran'))
            ->groupBy('users.id')
            ->orderByDesc('total_transactions')
            ->get();

        foreach ($todayGift as $gift) {
            if ($gift->total_transactions > 19) {
                $date = Carbon::parse($gift->last_gift_date);
                if ($date->isToday()) {
                    if ($gift->today_gift_receive == 0) {
                        $this->setGiftForUser($gift->id, $gift->last_gift_amount);
                    }
                }
            }
        }
        return view('dashboard', compact('pageTitle', 'accounts', 'accountsCount', 'unSettledTransactions', 'transactions', 'incomeChart', 'todayLeaderboard'));
    }

    private function setGiftForUser($user_id, $last_gift_amount)
    {
        $select_user_transactions = DB::table('transactions')->select(DB::raw('SUM(amount) as amount'))->whereDate('created_at', Carbon::today())->where('type', 'outgoing')->where('user_id', $user_id)->get();

        $gift_price = $select_user_transactions[0]->amount * 10 / 100;

        $final_gift_price = $gift_price - $last_gift_amount;
//        $lastUserWallet = DB::table('users')->select('wallet')->where('id', $user_id)->first();
        $updateUserWallet = DB::table('users')->where('id', $user_id)->update([
            'today_gift_receive' => 1,
            'last_gift_date' => Carbon::today(),
            'last_gift_amount' => $gift_price
        ]);

        $transactionsId = Transactions::create([
            'user_id' => $user_id,
            'type' => 'income',
            'amount' => $final_gift_price,
            'transaction_code' => Hash::make(now()),
            'description' => 'هدیه سیستمی روزانه به مبلغ ' . $final_gift_price . ' تاریخ ' . Carbon::today() . ' با موفقیت واریز شد!'
        ])->id;
        (new TransactionsController)->addToUserWallet($user_id, $final_gift_price);
        smilify('success', 'هدیه روزانه شما به مبلغ ' . $final_gift_price . ' با موفقیت به حساب شما واریز شد');

    }
}
