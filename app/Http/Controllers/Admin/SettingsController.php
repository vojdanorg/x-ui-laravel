<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function general()
    {
        $pageTitle = 'تنظیمات اصلی';
        return view('paper.pages.admin.settings.general', compact('pageTitle'));
    }

    public function saveGeneral(Request $request)
    {
        if (isset($request->default_uri_hosts)) {
            set_static_option('default_uri_hosts', $request->default_uri_hosts);
        } else {
            set_static_option('default_uri_hosts', " ");
        }

        if (isset($request->reality_server_names)){
            set_static_option('reality_server_names', $request->reality_server_names);
        } else {
            set_static_option('reality_server_names', "reality_server_names");
        }

        if (isset($request->reality_public_key)){
            set_static_option('reality_public_key', $request->reality_public_key);
        } else {
            set_static_option('reality_public_key', "reality_public_key");
        }

        if (isset($request->reality_private_key)){
            set_static_option('reality_private_key', $request->reality_private_key);
        } else {
            set_static_option('reality_private_key', "reality_private_key");
        }


        if (isset($request->reality_server_names)){
            set_static_option('reality_server_dest', $request->reality_server_names);
        } else {
            set_static_option('reality_server_dest', "reality_server_dest");
        }


        if (isset($request->reality_server_names)){
            set_static_option('reality_server_port', $request->reality_server_port);
        } else {
            set_static_option('reality_server_port', "reality_server_port");
        }

        if (isset($request->multi_user_services_addr)) {
            if (strlen($request->multi_user_services_addr) > 3) {
                set_static_option('multi_user_services_addr', $request->multi_user_services_addr);
            }
        }

        if (isset($request->tunnel_ip_for_server_connection)) {
            set_static_option('tunnel_ip_for_server_connection', 1);
        }
        if (isset($request->stop_users_from_buying_accounts)) {
            set_static_option('stop_users_from_buying_accounts', 1);
        } else {
            set_static_option('stop_users_from_buying_accounts', 0);
        }
        if (isset($request->count_disabled_services_as_server_capacity)) {
            set_static_option('count_disabled_services_as_server_capacity', 1);
        } else {
            set_static_option('count_disabled_services_as_server_capacity', 0);
        }
        if (isset($request->allowed_user_register)) {
            set_static_option('allowed_user_register', 1);
        } else {
            set_static_option('allowed_user_register', 0);
        }
        if (isset($request->auto_delete_deactivated_clients_on_servers)) {
            set_static_option('auto_delete_deactivated_clients_on_servers', 1);
            set_static_option('auto_delete_deactivated_clients_on_servers_wait_before_delete', $request->auto_delete_deactivated_clients_on_servers_wait_before_delete ?? 1);
        } else {
            set_static_option('auto_delete_deactivated_clients_on_servers', 0);
        }
        if (isset($request->show_resellers_leaderboard_publicly)) {
            set_static_option('show_resellers_leaderboard_publicly', 1);
        } else {
            set_static_option('show_resellers_leaderboard_publicly', 0);
        }
        smilify('success', 'تغییرات ذخیره شد');
        return redirect()->back();
    }

}
