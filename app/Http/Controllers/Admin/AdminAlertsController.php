<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminAlerts;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AdminAlertsController extends Controller
{
    public function index()
    {
        $pageTitle = 'لیست اطلاعیه ها';
        $alerts = AdminAlerts::all();
        return view('paper.pages.admin.alerts.list', compact('pageTitle', 'alerts'));
    }

    public function add()
    {
        $pageTitle = 'افزودن اطلاعیه جدید';
        return view('paper.pages.admin.alerts.add', compact('pageTitle'));
    }

    public function doAdd(Request $request)
    {
        $request->validate([
            'message' => 'required',
            'class' => ['required', Rule::in(['danger', 'info', 'warning', 'success'])]
        ]);
        AdminAlerts::create([
            'class' => $request->class,
            'message' => $request->message
        ]);
        smilify('success', 'اطلاعیه جدید اضافه شد');
        return redirect()->back();
    }

    function delete($id)
    {
        AdminAlerts::where('id', $id)->delete();
        smilify('success', 'اطلاعیه حذف شد');
        return redirect()->back();
    }
}
