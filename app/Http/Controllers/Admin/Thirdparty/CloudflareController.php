<?php

namespace App\Http\Controllers\Admin\Thirdparty;

use App\Http\Controllers\Controller;
use App\Models\Cloudflare;
use Illuminate\Http\Request;

class CloudflareController extends Controller
{
    public function index()
    {
        $pageTitle = 'تنظیمات کلودفلیر';
        $cloudflares = Cloudflare::all();
        return view('paper.pages.admin.third-party.cloudflare.list', compact('pageTitle', 'cloudflares'));
    }

    public function add()
    {
        $pageTitle = 'افزودن کلودفلیر';
        return view('paper.pages.admin.third-party.cloudflare.add', compact('pageTitle'));
    }

    public function doAdd(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'api_token' => 'required',
            'zone_id' => 'required',
            'global_key' => 'required'
        ]);

        Cloudflare::create([
            'name' => $request->name,
            'email' => $request->email,
            'api_token' => $request->api_token,
            'zone_id' => $request->zone_id,
            'global_key' => $request->global_key
        ]);
        smilify('success', 'کلود فلیر جدید با موفقیت اضافه شد');
        return redirect()->back();
    }

    public function edit($id)
    {
        $cloudflare = Cloudflare::findOrFail($id);
        $pageTitle = 'ویرایش کلودفلیر';
        return view('paper.pages.admin.third-party.cloudflare.edit', compact('pageTitle', 'cloudflare'));
    }

    public function doEdit($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'api_token' => 'required',
            'zone_id' => 'required',
            'global_key' => 'required'
        ]);
        Cloudflare::where('id', $id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'api_token' => $request->api_token,
            'zone_id' => $request->zone_id,
            'global_key' => $request->global_key
        ]);
        smilify('success', 'کلود فلیر با موفقیت ویرایش شد');
        return redirect()->back();
    }
}
