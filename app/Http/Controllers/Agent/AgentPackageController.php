<?php

namespace App\Http\Controllers\Agent;

use App\Actions\Servers\Servers;
use App\Http\Controllers\Controller;
use App\Models\Accounts;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use function PHPUnit\Framework\containsEqual;
use function Symfony\Component\String\u;

class AgentPackageController extends Controller
{
    //

    function index()
    {
        $pageTitle = 'لیست پکیج ها';
        $packages = Package::all();
        return view('paper.pages.agent.package.list', compact('pageTitle', 'packages'));
    }


    function changeProtocol()
    {
        $pageTitle = 'تغییر پروتکل';
        return view('changeProtocol', compact('pageTitle'));
    }

    function forward_page()
    {
        $pageTitle = 'مدیریت حساب ها';
        return view('forward_page', compact('pageTitle'));
    }

    function doChangeProtocol(Request $request)
    {
        $pageTitle = 'تغییر پروتکل';

        if (is_null($request->link)) {
            return view('changeProtocol', compact('pageTitle'));
        } else {
            if (str_contains($request->link, "vless://")) {
                $string = $request->link;
                //                $pattern = '/^vless:\/\/([\w-]+)@(.*):(\d+)\?.*sni=([\w.-]+).*#(.*) (\d+\.\d+)/';
                $pattern = '/^vless:\/\/(.*)@(.*):(\d{1,5})?(.*)\?type=(.*)\&security=(.*)&flow(.*)#(.*)/';
                if (preg_match($pattern, $string, $matches)) {
                    $uuid = $matches[1];
                    $hostname = $matches[2];
                    $port = $matches[3];
                    $name = $matches[8];
                    if ($hostname == "pro-max.mtnservice.space") {
                        $list = (new \App\Actions\Servers\Servers())->getServerClients(20);
                        foreach ($list->obj as $item) {
                            if ($item['port'] == $port) {
                                $account = $item;
                                $clients = json_decode($account['settings'], true);
                                $clientID = $clients['clients'][0]['id'];

                                $data = $this->defaultAccountSettings($port, $name, $hostname, $account, $clientID);
                                if (!$data) {
                                    return redirect()->back();
                                }

                                $stream = json_decode($account['streamSettings'], true);


                                $client = json_decode($data['settings'], true);
                                $createAccount = (new \App\Actions\Servers\Servers())->createAccount(22, $data);
                                (new \App\Actions\Servers\Servers())->editAccount(20, $data, $account['id']);
                                if (!$createAccount) {
                                    (new \App\Actions\Servers\Servers())->editAccount(20, $data, $account['id']);
                                    $createAccount = (new \App\Actions\Servers\Servers())->createAccount(22, $data);
                                    if ($createAccount->success) {
                                        $lists = (new \App\Actions\Servers\Servers())->getServerClients(20);
                                        foreach ($lists->obj as $items) {
                                            if ($items['port'] == (int)$port) {
                                                $account = $items;

                                                $stream = json_decode($account['streamSettings'], true);


                                                $client = json_decode($data['settings'], true);

                                                $clientID = $client['clients'][0]['id'];
                                                $network = $stream['network'];
                                                $urlAccounts = 'vless://' . $client['clients'][0]['id'] . "@" . "mtn-ge3.mtnservice.space" . ':' . $account['port'] . '?type=tcp&security=' . $stream['security'] . ($stream['network'] === 'ws' ? '&path=' . urlencode('/') : '&flow=' . $client['clients'][0]['flow'] . '&sni=' . $stream['realitySettings']['serverNames'][0] . '&pbk=' . $stream['realitySettings']['publicKey'] . '&flow=' . $client['clients'][0]['flow'] . '&fp=' . $client['clients'][0]['fingerprint'] . '&sid=' . $stream['realitySettings']['shortIds'][0] . '#' . $data['remark']);

                                                $hash = Str::random(15, 'alnum');
                                                DB::table('connection_details')->insert([
                                                    'remark' => $data['remark'],
                                                    'port' => (int)$port,
                                                    'download' => $account['down'],
                                                    'upload' => $account['up'],
                                                    'total' => $account['total'],
                                                    'connection' => $urlAccount,
                                                    'expire_at' => $account['expiryTime'],
                                                    'expire_link_time' => Carbon::now(),
                                                    'hash_link' => $hash,
                                                ]);
                                                return redirect()->route('account.information', $hash);
                                            }
                                        }
                                    }
                                } else {
                                    $lists = (new \App\Actions\Servers\Servers())->getServerClients(20);
                                    (new \App\Actions\Servers\Servers())->editAccount(20, $data, $account['id']);
                                    $urlAccount = 'vless://' . $client['clients'][0]['id'] . "@" . "mtn-ge3.mtnservice.space" . ':' . $account['port'] . '?type=tcp&security=' . $stream['security'] . ($stream['network'] === 'ws' ? '&path=' . urlencode('/') : '&flow=' . $client['clients'][0]['flow'] . '&sni=' . $stream['realitySettings']['serverNames'][0] . '&pbk=' . $stream['realitySettings']['publicKey'] . '&flow=' . $client['clients'][0]['flow'] . '&fp=' . $client['clients'][0]['fingerprint'] . '&sid=' . $stream['realitySettings']['shortIds'][0] . '#' . $data['remark']);

//                                    dd($uri);
                                    foreach ($lists->obj as $items) {
                                        if ($items['port'] == (int)$port) {
                                            $account = $items;

                                            $hash = Str::random(15, 'alnum');
                                            DB::table('connection_details')->insert([
                                                'remark' => $data['remark'],
                                                'port' => (int)$port,
                                                'download' => $account['down'],
                                                'upload' => $account['up'],
                                                'total' => $account['total'],
                                                'connection' => $urlAccount,
                                                'expire_at' => $account['expiryTime'],
                                                'expire_link_time' => Carbon::now(),
                                                'hash_link' => $hash,
                                            ]);
                                            return redirect()->route('account.information', $hash);
                                        }
                                    }
                                    //                                    return view('view_account_details', compact('account', 'pageTitle', 'clientID', 'network'));
                                }

                            }
                        }


                    } else {
                        $pageTitle = 'no account found';
                        return view('no_account_reached', compact('pageTitle'));

                    }
                }
            }
        }
        return redirect()->back();
    }

    public function defaultAccountSettings($port, $remark, $hostname, $oldAccountDetails, $uuid)
    {
        if (!$oldAccountDetails || !$port || !$remark || !$hostname) {
            dd($oldAccountDetails);
        }

        $totalBand = ($oldAccountDetails['total']);
        $expiryTime = ($oldAccountDetails['expiryTime']);

        $data = [
            'up' => ($oldAccountDetails['up']),
            'down' => ($oldAccountDetails['down']),
            'total' => ($oldAccountDetails['total']),
            'remark' => $remark,
            'enabled' => false,
            "expiryTime" => ($oldAccountDetails['expiryTime']),
            'listen' => "",
            'port' => (int)$port,
            'protocol' => 'vless',
            'settings' => '{
  "clients": [
    {
      "id": "' . $uuid . '",
      "email": "' . $oldAccountDetails['remark'] . '| ' . rand(1000, 6555) . 'mtnservice@xray.com",
      "flow": "xtls-rprx-vision",
      "fingerprint": "chrome",
      "total": ' . $oldAccountDetails['total'] . ',
      "expiryTime": ' . $oldAccountDetails['expiryTime'] . '
    }
  ],
  "decryption": "none",
  "fallbacks": []
}',
            'streamSettings' => '{
  "network": "tcp",
  "security": "reality",
  "realitySettings": {
    "show": false,
    "dest": "' . get_static_option('reality_server_dest') . ':' . get_static_option('reality_server_port') . '",
    "xver": 0,
    "serverNames": [
      "' . get_static_option('reality_server_names') . '"
    ],
    "privateKey": "' . get_static_option('reality_private_key') . '",
    "publicKey": "' . get_static_option('reality_public_key') . '",
    "minClient": "",
    "maxClient": "",
    "maxTimediff": 0,
    "shortIds": [
      "451d"
    ]
  },
  "tcpSettings": {
    "header": {
      "type": "none"
    },
    "acceptProxyProtocol": false
  }
}',
            'sniffing' => '{
                        "enabled": true,
                        "destOverride": [
                          "http",
                          "tls"
                        ]
                      }'
        ];

        return $data;
    }

    private function getDetailsToShow($port)
    {

        return false;
    }

    function account_information($hash)
    {
        $account = DB::table('connection_details')->select('*')->where('hash_link', $hash)->first();

        if ($account) {

            if (Carbon::now()->lt($account->expire_link_time)) {
                $removeDetails = DB::table('connection_details')->where('hash_link', $hash)->delete();
                if ($removeDetails) {
                    redirect()->back();
                }
                redirect()->back();
            }

            $pageTitle = 'اطلاعات حساب';
            $ir_link = "";
            if (str_contains($account->connection, 'pro-max.mtnservice.space')){
                $ir_link = str_replace('pro-max.mtnservice.space', 'mci.pro-max.mtnservice.space', $account->connection);
            } else if (str_contains($account->connection, 'pro.mtnservice.space')){
                $ir_link = str_replace('pro.mtnservice.space', 'mci-pro.mtnservice.space', $account->connection);
            } else if (str_contains($account->connection, 'mtn-ge3.mtnservice.space')){
                $ir_link = str_replace('mtn-ge3.mtnservice.space', 'mci-ge3.mtnservice.space', $account->connection);
            }
            return view('view_account_details', compact('pageTitle', 'account', 'ir_link'));

        }

        return redirect()->back();
    }


    function accountDetail()
    {
        $pageTitle = 'نمایش اطلاعات کاربری';
        return view('account_detail', compact('pageTitle'));
    }


    function doAccountDetail(Request $request)
    {
        $pageTitle = 'نمایش اطلاعات حساب کاربری';

        if (is_null($request->link)) {
            return view('changeProtocol', compact('pageTitle'));
        } else {
            if (str_contains($request->link, "vless://")) {
                $string = $request->link;
                // $pattern = '/^vless:\/\/([\w-]+)@(.*):(\d+)\?.*sni=([\w.-]+).*#(.*) (\d+\.\d+)/';
                $pattern = '/^vless:\/\/(.*)@(.*):(\d{1,5})?(.*)\?type=(.*)\&security=(.*)&flow(.*)#(.*)/';
                if (preg_match($pattern, $string, $matches)) {
                    $uuid = $matches[1];
                    $hostname = $matches[2];
                    $port = $matches[3];
                    $name = $matches[8];
                    $hash = Str::random(15, 'alnum');
                    if ($hostname == "pro-max.mtnservice.space" || $hostname == 'mci.mtnservice.space') {
                        $list = (new \App\Actions\Servers\Servers())->getServerClients(20);
                        foreach ($list->obj as $item) {
                            if ($item['port'] == $port) {
                                $account = $item;
                                $client = json_decode($account['settings'], true);
                                $clientID = $client['clients'][0]['id'];
                                $stream = json_decode($account['streamSettings'], true);

                                $urlAccounts = 'vless://' . $client['clients'][0]['id'] . "@" . "pro-max.mtnservice.space" . ':' . $account['port'] . '?type=tcp&security=' . $stream['security'] . ($stream['network'] === 'ws' ? '&path=' . urlencode('/') : '&flow=' . $client['clients'][0]['flow'] . '&sni=' . $stream['realitySettings']['serverNames'][0] . '&pbk=' . $stream['realitySettings']['publicKey'] . '&flow=' . $client['clients'][0]['flow'] . '&fp=' . $client['clients'][0]['fingerprint'] . '&sid=' . $stream['realitySettings']['shortIds'][0] . '#' . $account['remark']);
                                DB::table('connection_details')->insert([
                                    'remark' => $account['remark'],
                                    'port' => (int)$port,
                                    'download' => $account['down'],
                                    'upload' => $account['up'],
                                    'total' => $account['total'],
                                    'connection' => $urlAccounts,
                                    'expire_at' => $account['expiryTime'],
                                    'expire_link_time' => Carbon::now(),
                                    'hash_link' => $hash,
                                ]);

                                return redirect()->route('account.information', $hash);
                            }
                        }
                    } else if ($hostname == 'pro.mtnservice.space') {
                        $list = (new \App\Actions\Servers\Servers())->getServerClients(19);
                        foreach ($list->obj as $item) {
                            if ($item['port'] == $port) {
                                $account = $item;
                                $client = json_decode($account['settings'], true);
                                $clientID = $client['clients'][0]['id'];
                                $stream = json_decode($account['streamSettings'], true);

                                $urlAccounts = 'vless://' . $client['clients'][0]['id'] . "@" . "pro.mtnservice.space" . ':' . $account['port'] . '?type=tcp&security=' . $stream['security'] . ($stream['network'] === 'ws' ? '&path=' . urlencode('/') : '&flow=' . $client['clients'][0]['flow'] . '&sni=' . $stream['realitySettings']['serverNames'][0] . '&pbk=' . $stream['realitySettings']['publicKey'] . '&flow=' . $client['clients'][0]['flow'] . '&fp=' . $client['clients'][0]['fingerprint'] . '&sid=' . $stream['realitySettings']['shortIds'][0] . '#' . $account['remark']);
                                DB::table('connection_details')->insert([
                                    'remark' => $account['remark'],
                                    'port' => (int)$port,
                                    'download' => $account['down'],
                                    'upload' => $account['up'],
                                    'total' => $account['total'],
                                    'connection' => $urlAccounts,
                                    'expire_at' => $account['expiryTime'],
                                    'expire_link_time' => Carbon::now(),
                                    'hash_link' => $hash,
                                ]);

                                return redirect()->route('account.information', $hash);
                            }
                        }
                    } else if ($hostname == 'mtn.mtnservice.space' || $hostname == 'mci-ge3.mtnservice.space' || $hostname == 'mtn-ge3.mtnservice.space') {
                        $list = (new \App\Actions\Servers\Servers())->getServerClients(22);
                        foreach ($list->obj as $item) {
                            if ($item['port'] == $port) {
                                $account = $item;
                                $client = json_decode($account['settings'], true);
                                $clientID = $client['clients'][0]['id'];
                                $stream = json_decode($account['streamSettings'], true);

                                $urlAccounts = 'vless://' . $client['clients'][0]['id'] . "@" . "mtn-ge3.mtnservice.space" . ':' . $account['port'] . '?type=tcp&security=' . $stream['security'] . ($stream['network'] === 'ws' ? '&path=' . urlencode('/') : '&flow=' . $client['clients'][0]['flow'] . '&sni=' . $stream['realitySettings']['serverNames'][0] . '&pbk=' . $stream['realitySettings']['publicKey'] . '&flow=' . $client['clients'][0]['flow'] . '&fp=' . $client['clients'][0]['fingerprint'] . '&sid=' . $stream['realitySettings']['shortIds'][0] . '#' . $account['remark']);
                                DB::table('connection_details')->insert([
                                    'remark' => $account['remark'],
                                    'port' => (int)$port,
                                    'download' => $account['down'],
                                    'upload' => $account['up'],
                                    'total' => $account['total'],
                                    'connection' => $urlAccounts,
                                    'expire_at' => $account['expiryTime'],
                                    'expire_link_time' => Carbon::now(),
                                    'hash_link' => $hash,
                                ]);

                                return redirect()->route('account.information', $hash);
                            }
                        }
                    }
                }
            }
        }
    }

    private function accountDetailFromServer(int $serverID, int $port)
    {
        $list = (new \App\Actions\Servers\Servers())->getServerClients($serverID);
        $hash = Str::random(15, 'alnum');
        foreach ($list->obj as $item) {
            if ($item['port'] == $port) {
                $account = $item;
                $client = json_decode($account['settings'], true);
                $clientID = $client['clients'][0]['id'];
                $stream = json_decode($account['streamSettings'], true);

                $urlAccounts = 'vless://' . $client['clients'][0]['id'] . "@" . "mtn-ge3.mtnservice.space" . ':' . $account['port'] . '?type=tcp&security=' . $stream['security'] . ($stream['network'] === 'ws' ? '&path=' . urlencode('/') : '&flow=' . $client['clients'][0]['flow'] . '&sni=' . $stream['realitySettings']['serverNames'][0] . '&pbk=' . $stream['realitySettings']['publicKey'] . '&flow=' . $client['clients'][0]['flow'] . '&fp=' . $client['clients'][0]['fingerprint'] . '&sid=' . $stream['realitySettings']['shortIds'][0] . '#' . $account['remark']);
                DB::table('connection_details')->insert([
                    'remark' => $account['remark'],
                    'port' => (int)$port,
                    'download' => $account['down'],
                    'upload' => $account['up'],
                    'total' => $account['total'],
                    'connection' => $urlAccounts,
                    'expire_at' => $account['expiryTime'],
                    'expire_link_time' => Carbon::now(),
                    'hash_link' => $hash,
                ]);

            }
        }

        return redirect()->route('account.information', $hash);
    }
}
