<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Models\Customer\Customer;
use App\Models\User;
use App\Notifications\UserNotification;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Auth\LoginRequest;

class AgentController extends Controller
{
    //

    function register()
    {
        $pageTitle = 'ایجاد کاربر جدید';
        return view('paper.pages.agent.agent_register', compact('pageTitle'));
    }

    function login()
    {
        $pageTitle = 'ورود به حساب کاربری مشتریان';
        return view('auth.agent_login', compact('pageTitle'));
    }

    function loginAsMember(LoginRequest $request)
    {

        $user = DB::table('users')->where('email', $request->email)->first();
        if (is_null($user)) {
            dump($user);
            smilify('success', ' اطلاعات اشتباست ');
        } else {
            if (Hash::check($request->agent_code, $user->agent_code)
                &&
                Hash::check($request->password, $user->password)
                &&
                Hash::check($request->agent_accept_code, $user->agent_accept_code)) {
                $request->authenticateAsMember();
                $request->session()->regenerate();
                auth()->user()->notify(new UserNotification(
                    'success',
                    'unlock',
                    ' ایپی ' . $request->ip() . ' وارد حساب شد '
                ));
                smilify('success', auth()->user()->name . ' خوش آمدید ');
                return redirect()->intended(RouteServiceProvider::HOME);
            } else {
                return redirect()->back();
            }
        }
    }

    function do_register(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required'],
            'agent_code' => ['required'],
            'agent_accept_code' => ['required', 'unique:users'],
        ]);

        $agentUser = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'wallet' => 0,
            'password' => Hash::make($request->password),
            'agent_code' => Hash::make($request->agent_code),
            'agent_accept_code' => Hash::make($request->agent_accept_code),
            'admin_id' => auth()->user()->id
        ]);

        $agentUser->attachRole('agent');
        event(new Registered($agentUser));
        smilify('success', auth()->user()->name . ' حساب کاربری با موفقیت ایجاد شد ');

        return redirect()->back();
    }
}
