<?php

namespace App\Http\Controllers\AppController;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //
    function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'reseller_id' => 'required',
            'confirm_code' => 'required'
        ]);
        $checkUserExist = DB::table('users')->where('email', $request->email)
            ->where('reseller_id', $request->reseller_id)->where('confirm_code', $request->confirm_code)->first();
        if (!$checkUserExist) {
            return response()->json([
                'status' => false,
                'message' => 'کاربر یافت نشد',
                'code' => 500,
                'body' => []
            ]);
        }

        if (!Hash::check($request->password, $checkUserExist->password)) {
            return response()->json([
                'status' => false,
                'message' => 'کاربر یافت نشد',
                'code' => 500,
                'body' => []
            ]);
        } else {
            return response()->json([
                'status' => true,
                'message' => 'با موفقیت وارد شدید',
                'code' => 200,
                'body' => [
                    'name' => $checkUserExist->name,
                    'email' => $checkUserExist->email,
                    'wallet' => $checkUserExist->wallet
                ]
            ]);
        }
    }

    function loginApp(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $checkUserExist = DB::table('users')->where('email', $request->email)->first();
        if (!$checkUserExist) {
            return response()->json([
                'status' => false,
                'message' => 'کاربر یافت نشد',
                'code' => 500,
                'body' => []
            ]);
        }

        if (!Hash::check($request->password, $checkUserExist->password)) {
            return response()->json([
                'status' => false,
                'message' => 'کاربر یافت نشد',
                'code' => 500,
                'body' => []
            ]);
        } else {
            return response()->json([
                'status' => true,
                'message' => 'با موفقیت وارد شدید',
                'code' => 200,
                'body' => [
                    'name' => $checkUserExist->name,
                    'email' => $checkUserExist->email,
                    'wallet' => $checkUserExist->wallet,
                    'id' => $checkUserExist->id,
                    'auth' => $checkUserExist->id,
                ]
            ]);
        }
    }


    function increaseWallet(Request $request)
    {
        $find_user = DB::table('users')->where('id', $request->id)->where('email', $request->email)->first();

        if ($find_user) {
            $walletAmount = is_numeric($find_user->wallet) ? (float) $find_user->wallet : 0.0;
            $requestAmount = is_numeric($request->amount) ? (float) $request->amount : 0.0;

            $newAmount = $walletAmount + $requestAmount;

            $update_wallet = DB::table('users')
                ->where('id', $request->id)
                ->where('email', $request->email)
                ->update(['wallet' => $newAmount]);

            if ($update_wallet) {
                return response()->json([
                    'status' => true,
                    'message' => 'عملیات افزایش موجودی با موفقیت انجام شد',
                    'code' => 200,
                    'body' => ['wallet' => $newAmount]
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'عملیات افزایش موجودی با موفقیت انجام نشد',
                    'code' => 500,
                    'body' => []
                ]);
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'کاربر یافت نشد',
                'code' => 500,
                'body' => []
            ]);
        }
    }
}
