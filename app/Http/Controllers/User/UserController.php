<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Accounts;
use App\Models\Transactions;
use App\Models\User;
use App\Notifications\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;

class UserController extends Controller
{
    public function profile()
    {
        $pageTitle = 'پروفایل کاربری';
        return view('paper.pages.user.profile', compact('pageTitle'));
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'sell_price' => 'numeric'
        ]);
        User::where('id', auth()->user()->id)->update([
            'sell_price' => $request->sell_price
        ]);

        smilify('success', 'پروفایل با موفقیت بروز شد');
        return redirect()->back();
    }

    public function changePassword()
    {
        $pageTitle = 'تغییر رمزعبور';
        return view('paper.pages.user.change_password', compact('pageTitle'));
    }

    public function doChangePassword(Request $request)
    {
        $validate = $request->validate([
            'password' => ['required', 'confirmed', Password::defaults()],
        ]);
        User::where('id', auth()->user()->id)->update([
            'password' => Hash::make($request->password),
        ]);
        smilify('success', 'رمز عبور با موفقیت تغییر کرد');
        return redirect()->back();
    }


    public function list()
    {
//        dd(checkIfUserIsBlocked(auth()->user()->id));
        $pageTitle = 'لیست کاربران';
        $users = User::with('accounts')->orderBy('created_at', 'DESC')->get();
        return view('paper.pages.user.list', compact('pageTitle', 'users'));
    }

    public function edit($id)
    {
        $pageTitle = 'ویرایش کاربر';
        $user = User::findOrFail($id);
        return view('paper.pages.user.edit', compact('pageTitle', 'user'));
    }

    public function doEdit($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required'
        ]);
        User::where('id', $id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'price_per_account' => $request->price_per_account
        ]);
        smilify('success', 'با موفقیت تغییر کرد');
        return redirect()->back();
    }

    public function delete($id)
    {
        User::where('id', $id)->delete();
        smilify('success', 'با موفقیت حذف شد ');
        return redirect()->route('user.list');
    }

    public function detail($id)
    {
        $user = User::with(['accounts', 'transactions'])->findOrFail($id);
        $dbAccounts = Accounts::where('user_id', $user->id)->with('servers')->orderBy('created_at', 'DESC')->paginate(5);
        $accounts = $dbAccounts;
        $unSettledTransactions = Transactions::where('user_id', $id)->UnSettleds();
        $transactions = Transactions::where('user_id', $user->id)->orderBy('created_at', 'DESC')->paginate(10);
        $pageTitle = ' جزئیات ' . $user->name;
        $incomeChart = [];
        $chart_options = [
            'chart_title' => 'تعداد تراکنش در روز',
            'report_type' => 'group_by_date',
            'model' => 'App\Models\Transactions',
            'conditions' => [
                ['name' => 'income', 'condition' => 'type = "outgoing" AND user_id = ' . $user->id, 'color' => 'black', 'fill' => false]
            ],
            'group_by_field' => 'created_at',
            'group_by_period' => 'day',
            'filter_field' => 'created_at',
            'filter_period' => 'week',
            'filter_days' => 30,
            'chart_type' => 'line'
        ];
        $incomeChart = new LaravelChart($chart_options);

        return view('paper.pages.user.detail', compact('pageTitle', 'user', 'unSettledTransactions', 'accounts', 'dbAccounts', 'transactions', 'incomeChart'));
    }

    public function autoSettleBasedOnAmount($id)
    {
        $user = User::findOrFail($id);
        $pageTitle = ' تسویه حساب با ' . $user->name;
        $unSettledTransactions = Transactions::where('user_id', $id)->UnSettleds();
        return view('paper.pages.user.auto_settle', compact('pageTitle', 'user', 'unSettledTransactions'));
//        dd(getListOfTransactionsForSpecificUserAndAmount($id, 500000));
    }

    public function doAutoSettleBasedOnAmount($id, Request $request)
    {
        $user = User::findOrFail($id);
        $unSettledTransactions = Transactions::where('user_id', $id)->UnSettleds();
        $request->validate([
            'amount' => 'numeric|min:0|max:' . $unSettledTransactions->sum('amount')
        ]);
        $filtredList = getListOfTransactionsForSpecificUserAndAmount($id, $request->amount);
        foreach ($filtredList['filtered_transactions'] as $transaction) {
            Transactions::where('id', $transaction->id)->update(['settled' => 1]);
        }
        if ($filtredList['difference_between_sum_and_amount']) {
            Transactions::create([
                'user_id' => $id,
                'type' => 'outgoing',
                'amount' => $filtredList['difference_between_sum_and_amount'],
                'transaction_code' => Hash::make(now()),
                'description' => 'مابه التفاوت تسویه حساب به مبلغ: ' . number_format($request->amount) . ' تومان.'
            ]);
        }
        $user->notify(new UserNotification(
            'success',
            'check',
            ' از حساب شما مبلغ ' . number_format($request->amount) . ' تومان تسویه شد '
        ));
        smilify('success', 'تسویه حساب با موفقیت انجام شد');
        return redirect()->route('user.detail', $id);
    }

    public function changeStatus($id)
    {
        $user = User::findOrFail($id);
        $userJsonData = (is_null($user->json_data) ? [] : (array)$user->json_data);
        $isBlocekd = checkIfUserIsBlocked($id);
        $userJsonData['is_blocked'] = !$isBlocekd;
//        dd($userJsonData, $isBlocekd);
        User::where('id', $id)->update([
            'json_data' => $userJsonData
        ]);
        smilify('success', 'مسدود سازی کاربر با موفقیت انجام شد');
        return redirect()->back();
    }
}
