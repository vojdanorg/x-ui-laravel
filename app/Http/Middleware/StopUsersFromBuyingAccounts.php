<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class StopUsersFromBuyingAccounts
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!auth()->user()->hasRole('admin')) {
            if (get_static_option('stop_users_from_buying_accounts')) {
                smilify('error', 'درحال حاظر امکان خرید سرویس کاربری غیرممکن میباشد. برای دریافت اطلاعات بیشتر با مدیر وبسایت در ارتباط باشید.');
                return redirect()->back();
            }
        }
        return $next($request);
    }
}
