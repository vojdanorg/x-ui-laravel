<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckIfUserIsBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check()) {
            if (checkIfUserIsBlocked(auth()->user()->id)) {
                return abort(403, 'حساب کاربری شما مسدود شده است. جهت کسب اطلاعات بیشتر با مدیریت در ارتباط باشید.');
            }
        }

        return $next($request);
    }
}
