<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class checkApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(!isset(getallheaders()['Token'])){
            return response()->json([
                'code' => 403,
                'success' => false,
                'message' => 'forbidden',
                'body' => []
            ]);
        }
        $user = User::select('api_token')->where('api_token',getallheaders()['Token'])->first();
        if(is_null($user)){
            return response()->json([
                'code' => 403,
                'success' => false,
                'message' => 'forbidden',
                'body' => []
            ]);
        }
        return $next($request);
    }
}