<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminAlerts extends Model
{
    use HasFactory;

    public $table = 'admin_alerts';
    protected $fillable = [
        'class',
        'message'
    ];
}
