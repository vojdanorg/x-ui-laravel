<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Accounts extends Model
{
    use HasFactory;

    public $table = 'accounts';
    protected $fillable = [
        'user_id',
        'account_name',
        'server_id',
        'account_id',
        'ips',
        'package_id',
        'block_reason', // 0=> not blocked, 1=>bandwidth limit reached, 2=>expire time reached, 3=>too much ips connected
        'json_data'
    ];
    protected $casts = [
        'json_data' => 'object'
    ];
    public function servers()
    {
        return $this->BelongsTo(Servers::class, 'server_id', 'id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function transaction()
    {
        return $this->hasOne(Transactions::class, 'id', 'transactions_id');
    }

    public function package()
    {
        return $this->hasOne(Package::class, 'id', 'package_id');
    }
}
