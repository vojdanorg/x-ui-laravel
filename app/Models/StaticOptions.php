<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaticOptions extends Model
{
    use HasFactory;
    public $table = 'static_options';
    protected $fillable = [
        'key',
        'value'
    ];
}
