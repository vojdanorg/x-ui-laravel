<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cloudflare extends Model
{
    use HasFactory;

    public $table = 'cloudflare';
    protected $fillable = [
        'name',
        'email',
        'api_token',
        'zone_id',
        'global_key',
        'json_data',
   ];
    protected $casts = [
      'json_data'=>'object'
    ];
}
