<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    public $table = 'packages';
    protected $fillable = [
        'price',
        'bandwidth',
        'expire_at',
        'status',
        'name',
        'user_limit',
        'protocol',
        'description',
        'price_leve'
    ];
    protected $casts = [
      'protocol'=>'array'
    ];
    public function accounts()
    {
        return $this->hasMany(Accounts::class, 'package_id', 'id');
    }
}
