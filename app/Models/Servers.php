<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servers extends Model
{
    use HasFactory;
    public $table = 'servers';
    protected $fillable = [
        'hostname',
        'iran_ip',
        'panel_port',
        'panel_cookie',
        'server_name',
        'max_users',
        'server_provider_website',
        'tunnel_ip',
        'eu_ip',
        'tunnel_cookie',
        'json_data'
    ];
    protected $hidden = [
        'panel_cookie',
        'tunnel_cookie'
    ];
    protected $casts = [
      'json_data'=>'array'
    ];
}
