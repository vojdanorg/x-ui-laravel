<?php

namespace App\Models\Customer;

use App\Models\Accounts;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    public $table = 'customer';
    protected $fillable = [
        'fullname',
        'email',
        'mobile',
        'password',
        'wallet',
        'user_id',
    ];

    public function reseller()
    {
        return $this->BelongsTo(User::class, 'user_id', 'id');
    }

    public function accounts(){
        return $this->hasMany(Accounts::class,'id','customer_id');
    }
}
