<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    use HasFactory;
    public $table = 'transactions';
    protected $fillable = [
        'user_id',
        'type',
        'transaction_code',
        'description',
        'amount',
        'settled'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function scopeUnSettleds($q)
    {
        $q->where('settled', '=', 0)->where('type','outgoing');
    }
}
