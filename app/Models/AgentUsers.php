<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Laravel\Sanctum\HasApiTokens;

class AgentUsers extends Model
{
    use LaratrustUserTrait;
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'agent_code',
        'agent_accept_code'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'wallet'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */

    public function accounts()
    {
        return $this->hasMany(Accounts::class, 'user_id', 'id');
    }

    public function transactions()
    {
        return $this->hasMany(Transactions::class, 'user_id', 'id');
    }
}
