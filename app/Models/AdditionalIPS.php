<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdditionalIPS extends Model
{
    use HasFactory;

    public $table = 'additional_ips';
    protected $fillable = [
        'ip_address',
        'status',
        'provider',
        'server_id'
    ];

    public function server()
    {
        return $this->hasOne(Servers::class, 'id', 'server_id');
    }
}
