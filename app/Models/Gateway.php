<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{
    use HasFactory;
    public $table = 'gateway';
    protected $fillable = [
        'user_id',
        'amount',
        'status',
        'authority',
        'refid',
        'payments_id',
        'gateway_invoice_id'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    
    public function payment(){
        return $this->belongsTo(Payments::class,'payments_id','id');
    }
}
