<?php

use App\Actions\Servers\Servers;
use App\Models\Transactions;
use App\Models\User;
use App\Models\Servers as ModelsServers;
use App\Actions\Api\PritunlWebService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

function getServerStatus($id, $forView = false, $serverType = 'v2ray', $serverData = false)
{
    $status = false;
    if ($serverType == 'v2ray') {
        $status = (new Servers)->getServerStatus($id);
        if ($forView == true) {
            if ($status == true) {
                return '<span class="badge badge-success">فعال</span>';
            } else {
                return '<span class="badge badge-danger">غیرفعال</span>';
            }
        }
    } elseif ($serverType == 'pritunl') {
        $pritunlHandler = new PritunlWebService($serverData->json_data['pritunl_data']['api_token'], $serverData->json_data['pritunl_data']['api_secret'], $serverData->tunnel_ip);
        $status = $pritunlHandler->getOrganizations();
        if (!$status) {
            return '<span class="badge badge-danger">غیرفعال</span>';
        } else {
            return '<span class="badge badge-success">فعال</span>';
        }
    }
    return $status;
}

function countServerClients($id, $returnBoolForNoData = false, $serverType = 'v2ray', $serverData = false)
{
    if ($serverType == 'v2ray') {
        $list = (new Servers)->getServerClients($id);
        if ($list == false) {
            return ($returnBoolForNoData ? false : '-');
        }
        $subUsersCount = 0;
        foreach ($list->obj as $item) {
            if (isset($item['clientStats'])) {
                $subAccount = json_decode($item['settings'], true)['clients'];
                foreach ($subAccount as $key => $SettingsItems) {
                    if (get_static_option('count_disabled_services_as_server_capacity')) {
                        $subUsersCount++;
                    } else {
                        if (is_int($SettingsItems['expiryTime'])) {
                            if (($SettingsItems['expiryTime'] / 1000) >= time()) {
                                $subUsersCount++;
                            }
                        }
                    }
                }
            }
        }
        if (get_static_option('count_disabled_services_as_server_capacity')) {
            foreach ($list->obj as $item) {
                $subUsersCount++;
            }
        } else {
            foreach ($list->obj as $item) {
                if ($item['enable']) {
                    $subUsersCount++;
                }
            }
        }
        return $subUsersCount;
    } elseif ($serverType == 'pritunl') {
        $subUsersCount = 0;
        $pritunlHandler = new PritunlWebService($serverData->json_data['pritunl_data']['api_token'], $serverData->json_data['pritunl_data']['api_secret'], $serverData->tunnel_ip);
        $oUsers = $pritunlHandler->getOrganizationUsersByID($serverData->json_data['pritunl_data']['organization_id']);
        if ($oUsers == false) {
            return ($returnBoolForNoData ? false : '-');
        }
        if (get_static_option('count_disabled_services_as_server_capacity')) {
            foreach ($oUsers as $item) {
                $subUsersCount++;
            }
        } else {
            foreach ($oUsers as $item) {
                if (!$item['disabled']) {
                    $subUsersCount++;
                }
            }
        }
        return $subUsersCount;
    }

}

function set_static_option($key, $value)
{
    \App\Models\StaticOptions::where('key', $key)->delete();
    $status = \App\Models\StaticOptions::updateOrCreate([
        'key' => $key,
        'value' => $value
    ])->id;
    if ($status) {
        return true;
    } else {
        return false;
    }
}

function get_static_option($key)
{
    $response = \App\Models\StaticOptions::where('key', $key)->first();
    if (!is_null($response)) {
        return $response->value;
    }
}

function getTransactionType($type)
{
    switch ($type) {
        case 'income':
            return '<span class="badge badge-success">واریز</span>';
            break;
        case 'outgoing':
            return '<span class="badge badge-danger">برداشت</span>';
            break;
    }
}

function getAllUsers()
{
    return User::all();
}

function getAllServers()
{
    return ModelsServers::all();
}


function getUserAccountsDetails($accounts, $admin = false)
{
    if ($accounts->count() <= 0) {
        return [];
    }
    $serverAccounts = [];
    $userAccounts = [];
    foreach ($accounts as $account) {
        if (!in_array($account->servers->id, $serverAccounts)) {
            $list = (new Servers)->getServerClients($account->servers->id);
            if (!is_bool($list)) {
                $serverAccounts[$account->servers->id][] = $list->obj;
                $list = $list->obj;
            } else {
                $list = [];
            }
        } else {
            $list = $serverAccounts[$account->servers->id];
        }
        $list = $list;
        foreach ($list as $acc) {
            $id = explode(',', $acc['remark']);
            if (isset($id[1])) {
                if ($id[1] == auth()->user()->id || $admin == true) {
                    if ($id[2] == $account->id) {
                        $userAccounts[$account->id] = [
                            'local' => $account,
                            'server' => $acc
                        ];
                    }
                }
            }
        }
    }
    return $userAccounts;
}

function getUserAccountDetail($account)
{
    $details = false;
    try {

        $list = (new Servers)->getServerClients($account->server_id);
    } catch (\Exception $e) {
        $list = false;
    }
    if (!is_bool($list)) {
        $list = $list->obj;
    } else {
        $list = [];
    }
    foreach ($list as $acc) {
        $id = explode(',', $acc['remark']);
        if (isset($id[1])) {
            if ($id[1] == $account->user_id) {
                if ($id[2] == $account->id) {
                    $details = $acc;
                }
            }
        }
    }
    return $details;
}

function getUserAccountDetailById($id, $serverId)
{
    $details = false;
    $list = (new Servers)->getServerClients($serverId);
    if (!is_bool($list)) {
        $list = $list->obj;
    } else {
        $list = [];
    }
    foreach ($list as $acc) {
        if ($acc['id'] == $id) {
            $details = $acc;
        }
    }
    return $details;
}

function getTransactionSettleStatus($status)
{
    switch ($status) {
        case 0:
            return '<span class="badge badge-warning text-black">تسویه نشده</span>';
            break;
        case 1:
            return '<span class="badge badge-success">تسویه شده</span>';
            break;
    }
}

function getClientsOnServer($id)
{
    $list = (new Servers)->getServerClients($id);
    if ($list == false) {
        return false;
    }
    return $list->obj;
}

function checkIfAccountIsCDN($account)
{
    $hasCDN = false;
    if (isset($account->json_data->is_cdn)) {
        if ($account->json_data->is_cdn) {
            $hasCDN = true;
        }
    }
    return $hasCDN;
}

function getAccountURI($account, $accountDetails, $masterAccount = false, $has_reality = false)
{
    if ($masterAccount) {
        $steamSettings = json_decode($masterAccount['streamSettings'], true);
        $uri = 'vmess://' . base64_encode(json_encode([
                "v" => '2',
                'ps' => $account->account_name,
                'add' => get_static_option('multi_user_services_addr') ?? 'www.ilovepdf.com',
                'port' => (int)$masterAccount['port'],
                'id' => $accountDetails['uuid'],
                'aid' => 0,
                'net' => $steamSettings['network'],
                'type' => 'gun',
                'host' => "",
                'path' => "",
                'tls' => 'tls',
                'sni' => $account->servers->json_data['cdn']['domain']
            ]));
        return [
            'uri' => $uri,
            'tunnel_uri' => null,
            'network' => false
        ];
    }
    $extras = json_decode($accountDetails['settings'], true);
    $steamSettings = json_decode($accountDetails['streamSettings'], true);
    if ($accountDetails['protocol'] == 'vmess' || checkIfAccountIsCDN($account)) {
        $path = '/';
        $port = (int)$accountDetails['port'];
        $address = $account->servers->hostname;
        $tls = 'none';
        if (isset($account->package->protocol['ssl_protocol'])) {
            if ($account->package->protocol['ssl_protocol']) {
                $tls = $account->package->protocol['ssl_protocol'];
            }
        }
        $host = (!is_null(get_static_option('default_uri_hosts')) ? get_static_option('default_uri_hosts') : '');
        if (checkIfAccountIsCDN($account)) {
            $path = '/' . $account->servers->json_data['cdn']['path'] . '/' . (int)$accountDetails['port'];
            $port = $account->servers->json_data['cdn']['https_port'];
            $address = $account->servers->json_data['cdn']['domain'];
            $tls = 'tls';
            $host = $account->servers->json_data['cdn']['domain'];
        }
        $uri = 'vmess://' . base64_encode(json_encode([
                "v" => '2',
                'ps' => $account->account_name,
                'add' => $address,
                'port' => (int)$port,
                'id' => $extras['clients'][0]['id'],
                'aid' => 0,
                'net' => $steamSettings['network'],
                'type' => (checkIfAccountIsCDN($account) ? 'none' : 'http'),
                'host' => $host,
                'path' => $path,
                'tls' => $tls,
            ]));
        $tunnelUri = 'vmess://' . base64_encode(json_encode([
                "v" => '2',
                'ps' => '[MCI]' . $account->account_name,
                'add' => $address,
                'port' => (int)$port,
                'id' => $extras['clients'][0]['id'],
                'aid' => 0,
                'net' => $steamSettings['network'],
                'type' => (checkIfAccountIsCDN($account) ? 'none' : 'http'),
                'host' => 'bsi.ir',
                'path' => $path,
                'tls' => $tls,
            ]));

    } elseif ($accountDetails['protocol'] == 'vless') {
        if ($has_reality) {
            $uri = 'vless://' . $extras['clients'][0]['id'] . "@" . $account->servers->hostname . ':' . $accountDetails['port'] . '?type=tcp&security=' . $steamSettings['security'] . ($steamSettings['network'] === 'ws' ? '&path=' . urlencode('/') : '&flow=' . $extras['clients'][0]['flow'] . '&sni=' . $steamSettings['realitySettings']['serverNames'][0] . '&pbk=' . $steamSettings['realitySettings']['publicKey'] . '&flow=' . $extras['clients'][0]['flow'] . '&fp=' . $extras['clients'][0]['fingerprint'] . '&sid=' . $steamSettings['realitySettings']['shortIds'][0] . '#' . $account->account_name);
            $tunnelUri = 'vless://' . $extras['clients'][0]['id'] . "@" . $account->servers->tunnel_ip . ':' . $accountDetails['port'] . '?type=' . $steamSettings['network'] . '&security=' . $steamSettings['security'] . ($steamSettings['network'] === 'ws' ? '&path=' . urlencode('/') : '&flow=' . $extras['clients'][0]['flow'] . '&sni=' . $account->servers->tunnel_ip) . '#' . urlencode('[NO TUNNEL]' . $account->account_name);
        } else {
            $uri = 'vless://' . $extras['clients'][0]['id'] . "@" . $account->servers->hostname . ':' . $accountDetails['port'] . '?type=' . $steamSettings['network'] . '&security=' . $steamSettings['security'] . ($steamSettings['network'] === 'ws' ? '&path=' . urlencode('/') : '&flow=' . $extras['clients'][0]['flow'] . '&sni=' . $account->servers->hostname) . '#' . urlencode($account->account_name);
            $tunnelUri = 'vless://' . $extras['clients'][0]['id'] . "@" . $account->servers->tunnel_ip . ':' . $accountDetails['port'] . '?type=' . $steamSettings['network'] . '&security=' . $steamSettings['security'] . ($steamSettings['network'] === 'ws' ? '&path=' . urlencode('/') : '&flow=' . $extras['clients'][0]['flow'] . '&sni=' . $account->servers->tunnel_ip) . '#' . urlencode('[NO TUNNEL]' . $account->account_name);
        }
    }
    return [
        'uri' => $uri,
        'tunnel_uri' => $tunnelUri,
        'network' => $steamSettings['network']
    ];
}

function generateClientConfigurationByRequest($request, $client, $hasCDN, $expiryTime)
{
    if ($hasCDN) {
        return $client;
    }
    $total = json_decode($client['total'], true);
    $expiryTime = ($client ? $client['expiryTime'] : (strtotime($expiryTime) * 1000));

    $client['protocol'] = $request->protocol;
    $extras = json_decode($client['settings'], true);
    if ($request->protocol === 'vless' && $request->transmission === 'tcp') {
        $client['settings'] = '{
                                          "clients": [
                                            {
                                              "id": "' . $extras['clients'][0]['id'] . '",
                                              "flow": "xtls-rprx-direct"
                                            }
                                          ],
                                          "decryption": "none",
                                          "fallbacks": []
                                        }';
        if (!isset($request->tls_protocol) && !isset($request->xtls_protocol)) {
            $client['streamSettings'] = '{
                                              "network": "tcp",
                                              "security": "none",
                                              "tcpSettings": {
                                                "header": {
                                                  "type": "none"
                                                }
                                              }
                                            }';
        } else {
            $client['streamSettings'] = '{
                                              "network": "tcp",
                                              "security": "' . (isset($request->xtls_protocol) ? 'xtls' : 'tls') . '",
                                              "' . (isset($request->xtls_protocol) ? 'xtls' : 'tls') . 'Settings": {
                                                "serverName": "' . $request->domain . '",
                                                "certificates": [
                                                  {
                                                    "certificateFile": "' . $request->public_key . '",
                                                    "keyFile": "' . $request->private_key . '"
                                                  }
                                                ]
                                              },
                                              "tcpSettings": {
                                                "header": {
                                                  "type": "none"
                                                }
                                              }
                                            }';
        }

    } elseif ($request->protocol === 'vless' && $request->transmission === 'websocket') {
        $client['settings'] = '{
                                          "clients": [
                                            {
                                              "id": "' . $extras['clients'][0]['id'] . '",
                                              "flow": "xtls-rprx-direct"
                                            }
                                          ],
                                          "decryption": "none",
                                          "fallbacks": []
                                        }';
        if (!isset($request->tls_protocol)) {
            $client['streamSettings'] = '{
                                              "network": "ws",
                                              "security": "none",
                                              "wsSettings": {
                                                "header": {
                                                  "type": "none"
                                                }
                                              }
                                            }';
        } else {
            $client['streamSettings'] = '{
                                              "network": "ws",
                                              "security": "tls",
                                              "tlsSettings": {
                                                "serverName": "' . $request->domain . '",
                                                "certificates": [
                                                  {
                                                    "certificateFile": "' . $request->public_key . '",
                                                    "keyFile": "' . $request->private_key . '"
                                                  }
                                                ]
                                              },
                                              "wsSettings": {
                                                "path":"/"
                                                "header": {}
                                              }
                                            }';
        }
    } elseif ($request->protocol === 'vmess' && $request->transmission === 'tcp') {

        $client['settings'] = '{
        "clients": [
        {
      "id": "' . $extras['clients'][0]['id'] . '",
      "email": "user' . rand(1000, 6999) . '@mtnservice.com",
      "flow": "",
      "fingerprint": "chrome",
      "total": "' . $total . '",
      "expiry": "' . $expiryTime . '"

        }
        ],
          "decryption": "none",
          "fallbacks": []

        }';

        if (!isset($request->tls_protocol)) {
            $client['streamSettings'] = '{
  "network": "tcp",
  "security": "none",
  "tcpSettings": {
    "header": {
      "type": "http",
      "request": {
        "method": "GET",
        "path": [
          "/"
        ],
        "headers": {
          "Host": [
            "' . get_static_option('default_uri_hosts') . '"
          ]
        }
      },
      "response": {
        "version": "1.1",
        "status": "200",
        "reason": "OK",
        "headers": {}
      }
    },
    "acceptProxyProtocol": false
  }
}';
        } else {
            $client['streamSettings'] = '{
                                              "network": "tcp",
                                              "security": "tls",
                                              "tlsSettings": {
                                                "serverName": "' . $request->domain . '",
                                                "certificates": [
                                                  {
                                                    "certificateFile": "' . $request->public_key . '",
                                                    "keyFile": "' . $request->private_key . '"
                                                  }
                                                ]
                                              },
                                              "tcpSettings": {
                                                "header": {
                                                  "type": "none"
                                                }
                                              }
                                            }';
        }
    } elseif ($request->protocol === 'vmess' && $request->transmission === 'websocket') {
        $client['settings'] = '{
                                          "clients": [
                                            {
                                              "id": "' . $extras['clients'][0]['id'] . '",
                                              "alterId": 1
                                            }
                                          ],
                                          "disableInsecureEncryption": false,
                                        }';
        if (!isset($request->tls_protocol)) {
            $client['streamSettings'] = '{
                                              "network": "ws",
                                              "security": "none",
                                              "wsSettings": {
                                                "header": {
                                                  "type": "none"
                                                }
                                              }
                                            }';
        } else {
            $client['streamSettings'] = '{
                                              "network": "ws",
                                              "security": "tls",
                                              "tlsSettings": {
                                                "serverName": "' . $request->domain . '",
                                                "certificates": [
                                                  {
                                                    "certificateFile": "' . $request->public_key . '",
                                                    "keyFile": "' . $request->private_key . '"
                                                  }
                                                ]
                                              },
                                              "wsSettings": {
                                                "path": "/",
                                                "headers": {}
                                              }
                                            }';
        }
    } else {
        return false;
    }
    return $client;
}

function generateClientConfigurationByBackUp($client, $expiryTime)
{
    $total = json_decode($client['total'], true);
    $expiryTime = ($client ? $client['expiryTime'] : (strtotime($expiryTime) * 1000));

    $extras = json_decode($client['settings'], true);

    $client['settings'] = '{
        "clients": [
        {
      "id": "' . $extras['clients'][0]['id'] . '",
      "email": "user' . rand(1000, 6999) . '@mtnservice.com",
      "flow": "",
      "fingerprint": "chrome",
      "total": "' . $total . '",
      "expiry": "' . $expiryTime . '"

        }
        ],
          "decryption": "none",
          "fallbacks": []

        }';

    $client['streamSettings'] = '{
  "network": "tcp",
  "security": "none",
  "tcpSettings": {
    "header": {
      "type": "http",
      "request": {
        "method": "GET",
        "path": [
          "/"
        ],
        "headers": {
          "Host": [
            "' . get_static_option('default_uri_hosts') . '"
          ]
        }
      },
      "response": {
        "version": "1.1",
        "status": "200",
        "reason": "OK",
        "headers": {}
      }
    },
    "acceptProxyProtocol": false
  }
}';

    return $client;
}


function generateExtraConfigForBuyAccount($server, $account)
{

}

function getAllAdminAlerts()
{
    return \App\Models\AdminAlerts::all();
}

function checkIfServerHasCDNSettings($server)
{
    $hasCDN = false;
    if (isset($server->json_data['cdn'])) {
        if ($server->json_data['cdn'] != false) {
            $hasCDN = true;
        }
    }
    return $hasCDN;
}

function checkIfServerIsAllowedForPackageToBeMultiUser($server, $package)
{
    $result = false;
    if (isset($package->protocol['multi_user'])) {
        if (is_array($package->protocol['multi_user'])) {
            foreach ($package->protocol['multi_user'] as $multiuser) {
                if ($multiuser['server'] == $server->id) {
                    return $multiuser;
                }
            }
        }
    }
}

function checkIfServerIsAllowedByAnyPackageToBeMultiUser($server, $returnMultiPackage = false, $ignoreStatus = false)
{
    if (!$ignoreStatus) {
        $packages = \App\Models\Package::where('status', 1)->get();
    } else {
        $packages = \App\Models\Package::all();
    }
    $allowedPackages = [];
    foreach ($packages as $package) {
        if (checkIfServerIsAllowedForPackageToBeMultiUser($server, $package)) {
            if ($returnMultiPackage) {
                $allowedPackages[] = $package->id;
            } else {
                return $package;
            }
        } else {
            continue;
        }
    }
    if ($returnMultiPackage) {
        return $allowedPackages;
    } else {
        return false;
    }
}

function getUserAccountDetailsById($serverAccountId, $server_id)
{
    $list = (new \App\Actions\Servers\Servers())->getServerClients($server_id);
    foreach ($list->obj as $item) {
        if ($item['id'] == $serverAccountId) {
            echo($item['id']);
            return $item;
        }
    }
    return false;
}

function getUserAccountDetailsByRemark($serverAccountRemark, $server_id)
{
    $list = (new \App\Actions\Servers\Servers())->getServerClients($server_id);
    foreach ($list->obj as $item) {
        if ($item['remark'] == $serverAccountRemark) {
            echo($item['remark']);
            return $item;
        }
    }
    return false;
}

function multiUserGetAccountDetailsByEmail($accountEmail, $server_id, $package_id)
{
    $package = \App\Models\Package::where('id', $package_id)->first();
    $server = \App\Models\Servers::where('id', $server_id)->first();
    if (!isset($package->protocol['master_account'])) {
        $inSyncPackage = \App\Models\Package::where('id', $package->protocol['in_sync_package_id'])->first();
        $multiUser = checkIfServerIsAllowedForPackageToBeMultiUser($server, $inSyncPackage);
    } else {
        $multiUser = checkIfServerIsAllowedForPackageToBeMultiUser($server, $package);
    }
    $masterAccount = getUserAccountDetailsById($multiUser['master_account'], $server->id);

    $subAccounts = $masterAccount['clientStats'];
    $accountData = false;
    foreach ($subAccounts as $item) {
        if ($item['email'] == $accountEmail) {
            $accountData = [];
            $accountData['stats'] = $item;
        }
    }
    foreach (json_decode($masterAccount['settings'], true)['clients'] as $item) {
        if ($item['email'] == $accountEmail) {
            $accountData['config'] = $item;
        }
    }
    $accountData['master_account'] = $masterAccount;
    return $accountData;
}

function multiUserGetAccountDetailsByUUID($uuid, $server_id, $package_id)
{
    $package = \App\Models\Package::where('id', $package_id)->first();
    $server = \App\Models\Servers::where('id', $server_id)->first();
    $multiUser = checkIfServerIsAllowedForPackageToBeMultiUser($server, $package);
    $masterAccount = getUserAccountDetailsById($multiUser['master_account'], $server->id);
    $subAccounts = $masterAccount['clientStats'];
    $accountData = null;
    foreach (json_decode($masterAccount['settings'], true)['clients'] as $key => $item) {
        if ($item['id'] == $uuid) {
            $accountData = [];
            $accountData['config'] = $item;
            $accountData['config']['account_id'] = $key;
            break;
        }
    }
    foreach ($subAccounts as $item) {
        if ($item['id'] == $accountData['config']['account_id']) {
            $accountData['stats'] = $item;
        }
    }
    $accountData['master_account'] = $masterAccount;
    return $accountData;
}

function getAccountDetailsByINBOUNDID($account_id, $server_id, $package_id)
{
    $package = \App\Models\Package::where('id', $package_id)->first();
    $server = \App\Models\Servers::where('id', $server_id)->first();
    $masterAccount = getUserAccountDetailsByRemark($account_id, $server->id);
    $subAccounts = $masterAccount['clientInfo'];
    $accountData = null;
    foreach (json_decode($masterAccount['settings'], true)['clientInfo'] as $key => $item) {
        $accountData = [];
        $accountData['config'] = $item;
        $accountData['config']['account_id'] = $key;
    }
    foreach ($subAccounts as $item) {
        $accountData['stats'] = $item;
    }
    $accountData['master_account'] = $masterAccount;
    return $accountData;
}

function getRandomWord($len = 10)
{
    $word = array_merge(range('a', 'z'), range('A', 'Z'));
    shuffle($word);
    return substr(implode($word), 0, $len);
}

function findServerByListOfDomain($server_addr)
{
    $server = \App\Models\Servers::where('hostname', $server_addr)
        ->orWhere('tunnel_ip', $server_addr)
        ->orWhere('iran_ip', $server_addr)
        ->orWhere('eu_ip', $server_addr)
        ->first();
    if (is_null($server)) {
        $servers = \App\Models\Servers::all();
        foreach ($servers as $sv) {
            if (!is_null($sv->json_data)) {
                if (isset($sv->json_data['extra_domain_or_ip'])) {
                    if (is_array($sv->json_data['extra_domain_or_ip'])) {
                        foreach ($sv->json_data['extra_domain_or_ip'] as $dm) {
                            if ($dm == $server_addr) {
                                $server = $sv;
                                break;
                            }
                        }
                    }
                }
            }
        }
        if (is_null($server)) {
            return false;
        }
    }
    return $server;
}

function checkIfServerIsAllowedToBeMonitored($server_id)
{
    $allowedList = get_static_option('monitorbot_allowed_servers');
    if (is_null($allowedList)) {
        return false;
    }
    if (!$allowedList) {
        return false;
    }
    if (checkIfStringContainsComma($allowedList)) {
        $allowedList = explode(',', $allowedList);
        if (in_array($server_id, $allowedList)) {
            return true;
        }
        return false;
    }
    if ($allowedList == $server_id) {
        return true;
    }
    return false;
}

function checkIfStringContainsComma($string)
{
    if (strpos($string, ',')) {
        return true;
    }
    return false;
}

function getV2RayServers()
{
    $servers = ModelsServers::all();
    $v2rayServers = [];
    foreach ($servers as $server) {
        if (!isset($server->json_data['type'])) {
            $v2rayServers[] = $server;
        }
    }
    return $v2rayServers;
}

function getPritunlServers()
{
    $servers = ModelsServers::all();
    $pritunlServers = [];
    foreach ($servers as $server) {
        if (isset($server->json_data['type'])) {
            if ($server->json_data['type'] == 'pritunl') {
                $pritunlServers[] = $server;
            }
        }
    }
    return $pritunlServers;
}

function checkIfServerIsPritunl($server)
{
    $result = false;
    if (isset($server->json_data['type'])) {
        if ($server->json_data['type'] == 'pritunl') {
            $result = true;
        }
    }
    return $result;
}

function checkIfPackageIsPritunl($package)
{
    $result = false;
    if (isset($package->protocol['type'])) {
        if ($package->protocol['type'] == 'pritunl') {
            $result = true;
        }
    }
    return $result;
}

function checkIfAccountIsPritunl($account)
{
    $result = false;
    if (isset($account->json_data->type)) {
        if ($account->json_data->type == 'pritunl') {
            $result = true;
        }
    }
    return $result;
}

function getListOfTransactionsForSpecificUserAndAmount($userId, $amount)
{
    $user_id = $userId;
    $targetSum = $amount;

    $transactions = DB::table('transactions')
        ->select('id', 'amount')
        ->where('user_id', $user_id)
        ->where('settled', false)
        ->where('type', 'outgoing')
        ->orderBy('created_at')
        ->get();
    $sum = 0;
    $filteredTransactions = [];


    foreach ($transactions as $transaction) {
        $sum += $transaction->amount;
        $filteredTransactions[] = $transaction;

        if ($sum >= $targetSum) {
            break;
        }
    }
    return [
        'filtered_transactions' => $filteredTransactions,
        'difference_between_sum_and_amount' => $sum - $targetSum
    ];

}

function checkIfUserByIdIsOnline($id)
{
    if (Cache::get('user-is-online-' . $id)) {
        return true;
    } else {
        return false;
    }
}

function getUnSetteledByUserId($id)
{
    $unSettledTransactions = Transactions::where('user_id', $id)->UnSettleds();
    return $unSettledTransactions->sum('amount');
}

function checkIfUserIsBlocked($userId)
{
    $user = User::where('id', $userId)->first();
    if (is_null($user)) {
        return false;
    }
    if (!isset($user->json_data->is_blocked)) {
        return false;
    }
    if ($user->json_data->is_blocked) {
        return true;
    }
    return false;
}
